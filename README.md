#framework-server

> 一个极简封装的Java平台快速开发框架

## 系统结构
- 标准的SSM(Spring, SpringMVC, MyBatis)架构 
- 自带一个代码生成器：[http://git.oschina.net/backflow/framework-generator](http://git.oschina.net/backflow/framework-generator), 可生成从`mapper文件 > java实体 > dao > server > controller > view` 一条龙的代码, 生成器入口见`src/test`下相关文件
- 极简的封装（依赖于几个基类:`BaseEntity`, `BaseDao`, `BaseMybatisDao`, `BaseService`, `BaseController`）, 生成后不需写一行代码即可拥有最基本的CRUD功能
- 简化后的RBAC模型 ([framework-admin](http://git.oschina.net/backflow/framework-admin)中有说明) 对权限控制不需要依赖`apache-shiro`之类的框架, 通过一个注解与拦截器便可实现对权限的控制. 见(`PermissionInterceptor`类 与 `Permissions`注解


前端系统=> [http://git.oschina.net/backflow/framework-admin](http://git.oschina.net/backflow/framework-admin)

运行步骤:
- clone项目, 以maven项目导入, 等待依赖下载完成
- 执行`src/main/resources/schema`下的sql文件初始化数据库 (需本地安装MySQL, 并创建名为`admin`的数据库, 当然也可以为其它名字)
- 修改`src/main/webapp/META-INF`下的`context.xml`(`tomcat`内置应用级别的数据源配置文件), 指向对应的数据库地址, 若数据源名称有改动, 还需要修改`web.xml`下的`<resource-ref>`指向正确的数据源

管理员帐号/密码:`admin/admin`

项目刚刚发布完成度不高, 但后续会持续跟进, 也欢迎大家参与进来, 你的任何想法与建议我都很想知道! 快给我留言吧!!
