/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : admin

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-06-06 19:52:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_category
-- ----------------------------
DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '类别名称',
  `cover` varchar(255) DEFAULT NULL COMMENT '类别封面',
  `description` varchar(255) DEFAULT NULL COMMENT '类别描述',
  `parent_id` int(11) DEFAULT NULL COMMENT '父类别ID',
  `parent_name` varchar(64) DEFAULT NULL,
  `ancestors` varchar(255) DEFAULT NULL COMMENT '别路径',
  `ancestor_names` varchar(255) DEFAULT NULL COMMENT '父类别名称路径',
  `level` tinyint(1) DEFAULT NULL COMMENT '层级',
  `site_id` int(11) DEFAULT NULL COMMENT '站点Id',
  `priority` tinyint(5) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除标识',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='文章类别';

-- ----------------------------
-- Records of t_category
-- ----------------------------
INSERT INTO `t_category` VALUES ('1', '开源项目', '', '', null, '', '1', '开源项目', '0', null, null, '0', '2016-01-06 21:24:59', '2016-03-07 15:54:18');
INSERT INTO `t_category` VALUES ('2', '技术问答', '', '', '3', '问答', 'null,2', 'null,技术问答', '1', null, null, '1', '2016-01-08 00:15:14', '2016-02-28 01:12:14');
INSERT INTO `t_category` VALUES ('3', '问答', '', '向广大热心程序员寻求答案', null, null, null, null, null, null, null, '0', '2016-01-08 00:20:19', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('4', '技术分享', '', '', '3', null, null, null, null, null, null, '0', '2016-01-08 00:25:30', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('5', 'Java', '', '', '1', null, null, null, null, null, null, '0', '2016-01-08 00:26:18', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('6', 'C#', '', '', '1', null, null, null, null, null, null, '0', '2016-01-08 00:26:34', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('7', 'PHP', '', '', '1', null, null, null, null, null, null, '0', '2016-01-08 00:26:41', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('8', 'C/C++', '', '', '1', null, null, null, null, null, null, '0', '2016-01-08 00:26:50', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('9', 'Ruby', '', '', '1', null, null, null, null, null, null, '0', '2016-01-08 00:26:59', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('10', 'Python', '', '', '1', null, null, null, null, null, null, '0', '2016-01-08 00:27:10', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('11', 'Go', '', '', '1', null, null, null, null, null, null, '0', '2016-01-08 00:27:15', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('12', 'Java Script', '', '', '1', null, null, null, null, null, null, '0', '2016-01-08 00:27:30', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('13', '职业生涯', '', '', '3', '问答', 'null,13', 'null,职业生涯', '1', null, null, '0', '2016-01-08 00:28:57', '2016-01-14 00:42:08');
INSERT INTO `t_category` VALUES ('14', '你真逗', '', '', '2', null, null, null, null, null, null, '0', '2016-01-09 22:58:23', '2016-01-14 00:42:08');

-- ----------------------------
-- Table structure for t_department
-- ----------------------------
DROP TABLE IF EXISTS `t_department`;
CREATE TABLE `t_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `name` varchar(32) DEFAULT NULL COMMENT '部门名称',
  `leader` varchar(32) DEFAULT NULL COMMENT '负责人',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `tel` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `parent` int(11) DEFAULT NULL COMMENT '上级部门ID',
  `ancestors` varchar(255) DEFAULT NULL COMMENT '祖先ID路径',
  `level` tinyint(1) DEFAULT NULL COMMENT '等级(深度)',
  `priority` tinyint(5) DEFAULT NULL COMMENT '优先级',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `updated` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='部门';

-- ----------------------------
-- Records of t_department
-- ----------------------------
INSERT INTO `t_department` VALUES ('1', '益策（中国）', '1', '', null, null, '1', '0', null, '2016-06-27 17:29:34', '2017-05-16 16:12:05');
INSERT INTO `t_department` VALUES ('10', '广州', '32', '益策总部', '020-84120887', '1', '1,10', '1', '1', '2016-06-27 17:29:34', '2017-05-16 17:39:21');
INSERT INTO `t_department` VALUES ('11', '深圳', '', '', '0755-82077814', '1', '1,11', '1', null, '2016-09-19 13:47:15', null);
INSERT INTO `t_department` VALUES ('14', '成都', '', '', '028-61114433', '1', '1,14', '1', null, '2016-09-19 13:47:39', null);
INSERT INTO `t_department` VALUES ('17', '武汉', '', '', '027-87110487', '1', '1,17', '1', null, '2016-09-19 13:48:00', null);
INSERT INTO `t_department` VALUES ('18', '重庆', '', '', '023-68077434', '1', '1,18', '1', null, '2016-09-19 13:48:07', null);
INSERT INTO `t_department` VALUES ('19', '嘿嘿嘿', '21', null, null, null, '19', '0', null, '2017-05-19 14:55:56', null);

-- ----------------------------
-- Table structure for t_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL COMMENT '编码',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `key` varchar(32) NOT NULL COMMENT '键',
  `value` varchar(128) NOT NULL COMMENT '值',
  `comment` varchar(255) DEFAULT NULL,
  `priority` tinyint(5) DEFAULT NULL COMMENT '优先级',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态: 1-正常 0-禁用',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `t_dict_code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=846 DEFAULT CHARSET=utf8 COMMENT='字典信息';

-- ----------------------------
-- Records of t_dict
-- ----------------------------
INSERT INTO `t_dict` VALUES ('19', 'user_type', '用户类型', '1', '用户', null, '1', '0', '2016-01-15 20:42:24', '2016-01-15 20:42:24');
INSERT INTO `t_dict` VALUES ('20', 'user_type', '用户类型', '-1', '管理员', null, '2', '0', '2016-01-15 20:42:24', '2016-01-15 20:42:24');
INSERT INTO `t_dict` VALUES ('51', 'email_login_url', '邮箱域名地址映射', 'qq.com', 'https://mail.qq.com', null, '1', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('52', 'email_login_url', '邮箱域名地址映射', 'hotmail.com', 'https://login.live.com/login.srf?cbcxt=mai', null, '2', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('53', 'email_login_url', '邮箱域名地址映射', 'sina.cn', 'https://mail.sina.com.cn', null, '9', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('54', 'email_login_url', '邮箱域名地址映射', 'live.cn', 'https://login.live.com/login.srf?cbcxt=mai', null, '4', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('55', 'email_login_url', '邮箱域名地址映射', 'sohu.com', 'http://mail.sohu.com', null, '3', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('56', 'email_login_url', '邮箱域名地址映射', 'gmail.com', 'https://mail.google.com', null, '5', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('57', 'email_login_url', '邮箱域名地址映射', 'sina.com', 'https://mail.sina.com.cn', null, '6', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('58', 'email_login_url', '邮箱域名地址映射', 'yahoo.com', 'https://login.yahoo.com', null, '7', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('59', 'email_login_url', '邮箱域名地址映射', '163.com', 'http://mail.163.com', null, '8', '0', '2016-02-28 17:33:24', '2016-02-28 17:33:24');
INSERT INTO `t_dict` VALUES ('179', 'app_course_tag', '课程标签', 'recommend', '推荐', null, '2', '1', '2016-05-23 18:38:30', '2016-05-23 18:38:30');
INSERT INTO `t_dict` VALUES ('180', 'app_course_tag', '课程标签', 'hot', '热卖', null, '1', '1', '2016-05-23 18:38:30', '2016-05-23 18:38:30');
INSERT INTO `t_dict` VALUES ('181', 'app_course_tag', '课程标签', 'free', '免费', null, '4', '1', '2016-05-23 18:38:30', '2016-05-23 18:38:30');
INSERT INTO `t_dict` VALUES ('182', 'app_course_tag', '课程标签', 'newest', '最新', null, '3', '1', '2016-05-23 18:38:30', '2016-05-23 18:38:30');
INSERT INTO `t_dict` VALUES ('183', 'app_course_tag', '课程标签', 'trinity', '三位一体', null, '5', '1', '2016-05-23 18:38:30', '2016-05-23 18:38:30');
INSERT INTO `t_dict` VALUES ('204', 'status', '禁用状态', '0', '禁用', null, '1', '1', '2016-05-27 10:59:19', '2016-05-27 10:59:19');
INSERT INTO `t_dict` VALUES ('205', 'status', '正常状态', '1', '正常', null, '2', '1', '2016-05-27 10:59:19', '2016-05-27 10:59:19');
INSERT INTO `t_dict` VALUES ('228', 'room_topic_type', '直播主题类别', '3', '跨界观', '', '1', '1', '2016-07-22 11:29:47', '2016-07-22 11:29:47');
INSERT INTO `t_dict` VALUES ('229', 'room_topic_type', '直播主题类别', '2', '讲书院', '', '2', '1', '2016-07-22 11:29:47', '2016-07-22 11:29:47');
INSERT INTO `t_dict` VALUES ('230', 'room_topic_type', '直播主题类别', '1', '校长谈', '', '3', '1', '2016-07-22 11:29:47', '2016-07-22 11:29:47');
INSERT INTO `t_dict` VALUES ('235', 'app_msg', '错误码定义', 'invalid.encode', '-6', '', '6', '1', '2016-07-22 11:30:44', '2016-07-22 11:30:44');
INSERT INTO `t_dict` VALUES ('236', 'app_msg', '错误码定义', 'invalid.version', '-4', '', '4', '1', '2016-07-22 11:30:44', '2016-07-22 11:30:44');
INSERT INTO `t_dict` VALUES ('237', 'app_msg', '错误码定义', 'network.error', '-1', '', '2', '1', '2016-07-22 11:30:44', '2016-07-22 11:30:44');
INSERT INTO `t_dict` VALUES ('238', 'app_msg', '错误码定义', 'invalid.sign', '-5', '', '5', '1', '2016-07-22 11:30:44', '2016-07-22 11:30:44');
INSERT INTO `t_dict` VALUES ('239', 'app_msg', '错误码定义', 'params.required', '-3', '', '3', '1', '2016-07-22 11:30:44', '2016-07-22 11:30:44');
INSERT INTO `t_dict` VALUES ('240', 'app_msg', '错误码定义', 'phone.validateCode.sent', '-8', '', '8', '1', '2016-07-22 11:30:44', '2016-07-22 11:30:44');
INSERT INTO `t_dict` VALUES ('241', 'app_msg', '错误码定义', 'invalid.appid', '-2', '', '0', '1', '2016-07-22 11:30:44', '2016-07-22 11:30:44');
INSERT INTO `t_dict` VALUES ('242', 'app_msg', '错误码定义', 'login.expired', '-7', '', '7', '1', '2016-07-22 11:30:44', '2016-07-22 11:30:44');
INSERT INTO `t_dict` VALUES ('243', 'server_config_', '', 'weixin.appid', 'wx002cf1fb2bfc8a42', '2', '5', '1', '2016-08-05 17:33:09', '2017-03-13 17:33:09');
INSERT INTO `t_dict` VALUES ('244', 'server_config_', '', 'weixin.apiurl.https', 'https://api.weixin.qq.com/cgi-bin', '', '0', '1', '2016-08-05 17:33:09', '2016-08-05 17:33:09');
INSERT INTO `t_dict` VALUES ('245', 'server_config_', '', 'weixin.granttype', 'client_credential', '3', '4', '1', '2016-08-05 17:33:09', '2016-08-25 10:59:14');
INSERT INTO `t_dict` VALUES ('246', 'server_config_', '', 'weixin.secret', '0922317d27aeed55a4af4df7520534ef', '4', '2', '1', '2016-08-05 17:33:09', '2017-03-13 17:33:14');
INSERT INTO `t_dict` VALUES ('386', 'reward_mark', '打赏的金额对应的描述', '20', '润喉片', '<#FFFF00> {receiver} <#FF0000>讲了这么久，嗓子都哑了，<#FFFF00> {payer} <#FF0000>递给一盒润喉片', '40', '1', '2016-09-10 12:01:23', '2016-10-18 14:53:20');
INSERT INTO `t_dict` VALUES ('387', 'reward_mark', '打赏的金额对应的描述', '10', '敬茶', '<#FFFF00> {payer} <#FF0000>请<#FFFF00> {receiver} <#FF0000>喝杯茶解解渴', '50', '1', '2016-09-10 12:01:23', '2016-10-18 14:53:25');
INSERT INTO `t_dict` VALUES ('388', 'reward_mark', '打赏的金额对应的描述', '0', '棒棒糖', '<#FFFF00> {payer} <#FF0000>听得很开心，送给<#FFFF00> {receiver} <#FF0000>一根棒棒糖', '2', '1', '2016-09-10 12:01:23', '2016-10-18 14:53:29');
INSERT INTO `t_dict` VALUES ('389', 'reward_mark', '打赏的金额对应的描述', '100', '金话筒', '<#FFFF00> {payer} <#FF0000>送出一个金话筒，感谢<#FFFF00> {receiver} <#FF0000>精心的付出', '20', '1', '2016-09-10 12:01:23', '2016-10-18 14:53:33');
INSERT INTO `t_dict` VALUES ('390', 'reward_mark', '打赏的金额对应的描述', '-1', '其他', '7', '3', '1', '2016-09-10 12:01:23', '2016-09-10 12:01:23');
INSERT INTO `t_dict` VALUES ('391', 'reward_mark', '打赏的金额对应的描述', '5', '干货', '<#FFFF00> {receiver} <#FF0000>的内容满满都是干货，<#FFFF00> {payer} <#FF0000>听得津津有味', '60', '1', '2016-09-10 12:01:23', '2016-10-18 14:53:37');
INSERT INTO `t_dict` VALUES ('392', 'reward_mark', '打赏的金额对应的描述', '50', '32个赞', '<#FFFF00> {payer} <#FF0000>听得眉飞色舞，给<#FFFF00> {receiver} <#FF0000>32个赞', '30', '1', '2016-09-10 12:01:23', '2016-10-18 14:53:38');
INSERT INTO `t_dict` VALUES ('393', 'room_guest_type', '直播嘉宾类别', '0', '学员', '学员', '1', '1', null, '2016-10-25 14:30:02');
INSERT INTO `t_dict` VALUES ('394', 'room_guest_type', '直播嘉宾类别', '139', '分享嘉宾', '', '5', '1', null, '2016-10-25 14:30:02');
INSERT INTO `t_dict` VALUES ('395', 'room_guest_type', '直播嘉宾类别', '140', '对话嘉宾', '', '4', '1', null, '2016-10-25 14:30:02');
INSERT INTO `t_dict` VALUES ('396', 'room_guest_type', '直播嘉宾类别', '142', '助教', '', '2', '1', null, '2016-10-25 14:30:02');
INSERT INTO `t_dict` VALUES ('397', 'room_guest_type', '直播嘉宾类别', '141', '主持人', '', '3', '1', null, '2016-10-25 14:30:02');
INSERT INTO `t_dict` VALUES ('398', 'hinine', '', 'hshs', 'hdhsh', '', '0', '0', null, '2016-11-04 18:33:27');
INSERT INTO `t_dict` VALUES ('399', 'hinine', '', 'gsgsh', 'dhhd', '', '2', '0', null, '2016-11-04 18:33:27');
INSERT INTO `t_dict` VALUES ('435', 'app_index_guest_tag', '嘉宾标签', '3', '人力资源', '', '3', '0', null, '2016-12-15 18:10:30');
INSERT INTO `t_dict` VALUES ('436', 'app_index_guest_tag', '嘉宾标签', '2', '战略布局', '', '2', '0', null, '2016-12-15 18:10:30');
INSERT INTO `t_dict` VALUES ('437', 'app_index_guest_tag', '嘉宾标签', '1', '互联网思维', '', '0', '0', null, '2016-12-15 18:10:30');
INSERT INTO `t_dict` VALUES ('438', 'app_index_guest_tag', '嘉宾标签', '4', '财税领域', '', '4', '0', null, '2016-12-15 18:10:30');
INSERT INTO `t_dict` VALUES ('673', 'app_index_hot_keyword', '首页热门关键字', '3', '张学友', '', '3', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('674', 'app_index_hot_keyword', '首页热门关键字', '2', '学道', '', '2', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('675', 'app_index_hot_keyword', '首页热门关键字', '1', '财税', '', '1', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('676', 'app_index_hot_keyword', '首页热门关键字', '10', '周二', '', '10', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('677', 'app_index_hot_keyword', '首页热门关键字', '7', '周五', '', '7', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('678', 'app_index_hot_keyword', '首页热门关键字', '6', '周六', '', '6', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('679', 'app_index_hot_keyword', '首页热门关键字', '5', '周日', '', '5', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('680', 'app_index_hot_keyword', '首页热门关键字', '4', '测试新加字段的--文章链接', '', '4', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('681', 'app_index_hot_keyword', '首页热门关键字', '9', '周三', '', '9', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('682', 'app_index_hot_keyword', '首页热门关键字', '8', '周四', '', '8', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('683', 'app_index_hot_keyword', '首页热门关键字', '11', '周一', '', '11', '1', null, '2016-12-19 11:42:58');
INSERT INTO `t_dict` VALUES ('693', 'app_index_focus_lable', '标签', '15', '测试三', '', '15', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('694', 'app_index_focus_lable', '标签', '16', '测试四', '', '16', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('695', 'app_index_focus_lable', '标签', '13', '测试一', '', '13', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('696', 'app_index_focus_lable', '标签', '14', '测试二', '', '14', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('697', 'app_index_focus_lable', '标签', '11', '这个标签无解', 'none', '11', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('698', 'app_index_focus_lable', '标签', '12', '这有五个字', '', '12', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('699', 'app_index_focus_lable', '标签', '3', '通用管理大神', 'new', '3', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('700', 'app_index_focus_lable', '标签', '2', '职场提升大神', 'new', '2', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('701', 'app_index_focus_lable', '标签', '1', '财税金融大神', 'hot', '1', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('702', 'app_index_focus_lable', '标签', '10', '大厨食神', 'meals', '10', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('703', 'app_index_focus_lable', '标签', '7', '测试标签', 'hot', '7', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('704', 'app_index_focus_lable', '标签', '6', '情商管理', 'new', '6', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('705', 'app_index_focus_lable', '标签', '5', '办公技能', 'new', '5', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('706', 'app_index_focus_lable', '标签', '4', '职业素养', 'new', '4', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('707', 'app_index_focus_lable', '标签', '9', '宠物家园', 'animals', '9', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('708', 'app_index_focus_lable', '标签', '8', '益策王牌', '', '8', '1', null, '2016-12-22 17:10:10');
INSERT INTO `t_dict` VALUES ('742', 'server_config_', '', 'server.open.url', 'https://test-s.xuehu365.com', '2', '5', '1', '2016-08-05 17:33:09', '2017-03-09 11:33:37');
INSERT INTO `t_dict` VALUES ('743', 'server_config_', '', 'weixin.web.appid', 'wx3118b182a52b3971', '2', '5', '1', '2016-08-05 17:33:09', '2017-03-10 14:18:59');
INSERT INTO `t_dict` VALUES ('744', 'server_config_', '', 'weixin.web.secret', 'cf268e73d5a1760249e7b944b2090891', '4', '2', '1', '2016-08-05 17:33:09', '2017-03-10 14:19:02');
INSERT INTO `t_dict` VALUES ('804', 'testdict', 'testdict', 'testdict', 'testdict', 'testdict', '0', '0', null, '2017-05-15 04:17:25');
INSERT INTO `t_dict` VALUES ('805', '11111111', '', '11111111', '11111111', '', '0', '1', null, '2017-05-15 04:19:01');
INSERT INTO `t_dict` VALUES ('836', 'app_index_module', 'APP首页课程分类', '3', '直播预告', '', '3', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('837', 'app_index_module', 'APP首页课程分类', '2', '推荐分类', '', '2', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('838', 'app_index_module', 'APP首页课程分类', '1', '轮播图', '', '1', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('839', 'app_index_module', 'APP首页课程分类', '10', '首页开关', '1 ', '10', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('840', 'app_index_module', 'APP首页课程分类', '7', '首页嘉宾', '', '7', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('841', 'app_index_module', 'APP首页课程分类', '6', '课程分类', '', '6', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('842', 'app_index_module', 'APP首页课程分类', '5', '财道家塾', '', '5', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('843', 'app_index_module', 'APP首页课程分类', '4', '学道家塾', '', '4', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('844', 'app_index_module', 'APP首页课程分类', '9', '课程优惠分类', '', '9', '1', null, '2017-05-15 15:33:13');
INSERT INTO `t_dict` VALUES ('845', 'app_index_module', 'APP首页课程分类', '8', '广告页', '', '8', '1', null, '2017-05-15 15:33:13');

-- ----------------------------
-- Table structure for t_file_mapping
-- ----------------------------
DROP TABLE IF EXISTS `t_file_mapping`;
CREATE TABLE `t_file_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(64) NOT NULL COMMENT '文件key',
  `url` varchar(255) NOT NULL COMMENT '文件地址',
  `hash` varchar(64) DEFAULT NULL COMMENT '文件HASH',
  `size` int(11) NOT NULL DEFAULT '0' COMMENT '文件大小',
  `folder` varchar(64) DEFAULT NULL COMMENT ' 所属目录',
  `filename` varchar(255) NOT NULL COMMENT '文件名',
  `extention` varchar(16) DEFAULT NULL COMMENT '扩展名',
  `mimetype` varchar(128) DEFAULT NULL COMMENT 'MIME类型',
  `upload_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '上传时间',
  `download_num` int(5) NOT NULL DEFAULT '0' COMMENT '下载量',
  `duration` int(11) DEFAULT '0' COMMENT '时长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=742 DEFAULT CHARSET=utf8 COMMENT='文件映射';

-- ----------------------------
-- Records of t_file_mapping
-- ----------------------------
INSERT INTO `t_file_mapping` VALUES ('11', 'admin/image/2Q8ZUulU.jpg', 'http://static.xuehu365.com/admin/image/2Q8ZUulU.jpg', 'FmdHqTTI0EZU_y0VCFgiZLUlml8a', '75458', 'admin/image/', 'buddy.jpg', 'jpg', 'image/jpeg', '2016-08-25 15:44:20', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('33', 'admin/audio/2Q8ZHh7T.mp3', 'http://static.xuehu365.com/admin/audio/2Q8ZHh7T.mp3', 'FgomFPp-zvDGnvN0_4eyUuiZ6YB4', '13365', 'admin/audio/', '1 (1).mp3', 'mp3', 'audio/mp3', '2016-08-25 16:36:51', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('34', 'admin/audio/2Q8ZHh6n.mp3', 'http://static.xuehu365.com/admin/audio/2Q8ZHh6n.mp3', 'Fprv3HtoDrY16p8QY4E0GVSUrj4d', '70965', 'admin/audio/', '1 (2).mp3', 'mp3', 'audio/mp3', '2016-08-25 16:36:51', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('36', 'admin/audio/2Q8ZGT19.mp3', 'http://static.xuehu365.com/admin/audio/2Q8ZGT19.mp3', 'FtyutOnX-DEHPXZS2hXTTTVaE7ow', '238965', 'admin/audio/', '1 (11).mp3', 'mp3', 'audio/mp3', '2016-08-25 16:41:43', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('37', 'admin/audio/2Q8ZFn5k.mp3', 'http://static.xuehu365.com/admin/audio/2Q8ZFn5k.mp3', 'FrXaXwN4eNNdlRwqKGjcM7Kz6_12', '236085', 'admin/audio/', '1 (15).mp3', 'mp3', 'audio/mp3', '2016-08-25 16:44:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('38', 'admin/image/2Q8ZEuQL.jpg', 'http://static.xuehu365.com/admin/image/2Q8ZEuQL.jpg', 'Fhl3FiUz9OXqUlaHjvIVFlq8zuiK', '40324', 'admin/image/', '217160091193710182.jpg', 'jpg', 'image/jpeg', '2016-08-25 16:47:54', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('39', 'admin/image/2Q8ZEpcq.gif', 'http://static.xuehu365.com/admin/image/2Q8ZEpcq.gif', 'FrYReXM8I6-eOG_XBlGauUla281l', '308021', 'admin/image/', 'b58f8c5494eef01ffb8aff7ae2fe9925bd317df7.gif', 'gif', 'image/gif', '2016-08-25 16:48:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('40', 'admin/image/2Q8ZEnba.png', 'http://static.xuehu365.com/admin/image/2Q8ZEnba.png', 'FmnbVpij5jzUEXQhwDaUqS6Q2fRA', '59283', 'admin/image/', '1697.tm.png', 'png', 'image/png', '2016-08-25 16:48:21', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('41', 'admin/audio/2Q8ZAqwI.mp3', 'http://static.xuehu365.com/admin/audio/2Q8ZAqwI.mp3', 'Ft-IwUqq_7W3xXiNIKCMjw8jhUHj', '92277', 'admin/audio/', '1 (10).mp3', 'mp3', 'audio/mp3', '2016-08-25 17:04:01', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('42', 'admin/audio/2Q8Z3i72.mp3', 'http://static.xuehu365.com/admin/audio/2Q8Z3i72.mp3', 'Fv48RIyCPIEQob8d9yGdbag2nZoW', '186645', 'admin/audio/', '1 (14).mp3', 'mp3', 'audio/mp3', '2016-08-25 17:32:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('43', 'admin/file/2Q8TgPTl.sql', 'http://static.xuehu365.com/admin/file/2Q8TgPTl.sql', 'FvySIDa1g3XNiMxBstiPTYYFyawv', '4398', 'admin/file/', 't_article.sql', 'sql', 'application/octet-stream', '2016-08-26 15:36:18', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('44', 'admin/file/2Q8Tg3Zd.pdf', 'http://static.xuehu365.com/admin/file/2Q8Tg3Zd.pdf', 'FgatjGPfy-IFzemN9cnGys4NdjzQ', '3354776', 'admin/file/', '人类简史：从动物到上帝.pdf', 'pdf', 'application/pdf', '2016-08-26 15:37:42', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('45', 'admin/image/2Q8T6RFD.jpg', 'http://static.xuehu365.com/admin/image/2Q8T6RFD.jpg', 'FvioLxH5wLw6PcapsH6ifc67AoXp', '59110', 'admin/image/', 'angela.jpg', 'jpg', 'image/jpeg', '2016-08-26 17:59:11', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('46', 'admin/image/2Q8T6Pzr.jpg', 'http://static.xuehu365.com/admin/image/2Q8T6Pzr.jpg', 'FtFPyLK0hvnNTNi1DqmFiZIV_NfX', '212225', 'admin/image/', 'wallpaper-jessica.jpg', 'jpg', 'image/jpeg', '2016-08-26 17:59:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('47', 'admin/image/2Q8T6P7F.jpg', 'http://static.xuehu365.com/admin/image/2Q8T6P7F.jpg', 'Fhi7ifOoi_KdPBXjmI60O4anzNM5', '200506', 'admin/image/', 'wallpaper-tiffany.jpg', 'jpg', 'image/jpeg', '2016-08-26 17:59:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('48', 'admin/image/2Q7vqwOc.jpg', 'http://static.xuehu365.com/admin/image/2Q7vqwOc.jpg', 'Fi1VaP0dh9V4-if_NNtsXsLyUs44', '13656', 'admin/image/', '20160620155358c416e3ee-5085-4e81-a0a0-199e30afd393.jpg', 'jpg', 'image/jpeg', '2016-09-01 10:27:44', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('49', 'admin/image/2Q7vqfg8.jpg', 'http://static.xuehu365.com/admin/image/2Q7vqfg8.jpg', 'FuR3lrTw497d9zK-wO3jLtscKaRi', '79390', 'admin/image/', '2sY5o9vd261fxOHhkRnU_big.jpg', 'jpg', 'image/jpeg', '2016-09-01 10:28:48', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('50', 'admin/image/2Q7vq9aU.jpeg', 'http://static.xuehu365.com/admin/image/2Q7vq9aU.jpeg', 'FrKbS3sYDxtzubJF_U63DXohfP_H', '8143', 'admin/image/', 'QQ图片20160422145845.jpeg', 'jpeg', 'image/jpeg', '2016-09-01 10:30:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('51', 'admin/image/2Q7vq3c4.jpg', 'http://static.xuehu365.com/admin/image/2Q7vq3c4.jpg', 'FlSXZEf4QubOfZvLSX5U3H5PAoBD', '11803', 'admin/image/', '2QBCAzXD_188250.jpg', 'jpg', 'image/jpeg', '2016-09-01 10:31:14', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('52', 'admin/image/2Q7vpuhr.jpg', 'http://static.xuehu365.com/admin/image/2Q7vpuhr.jpg', 'FrOsZxBBvoPgGqhotMLEDBJi0_hz', '13761', 'admin/image/', '2QBCDMb5_188250.jpg', 'jpg', 'image/jpeg', '2016-09-01 10:31:49', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('53', 'admin/image/2Q7vY9mo.jpg', 'http://static.xuehu365.com/admin/image/2Q7vY9mo.jpg', 'FnJ6Z7UkR1rOi9_MbExQMX_g32nP', '44461', 'admin/image/', 'QQ图片20160901114153.jpg', 'jpg', 'image/jpeg', '2016-09-01 11:42:21', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('54', 'admin/image/2Q7p2KNa.jpg', 'http://static.xuehu365.com/admin/image/2Q7p2KNa.jpg', 'FuE_4dCpasfAF22NUpbjPUowW1TJ', '17019', 'admin/image/', '16090115459052270.jpg', 'jpg', 'image/jpeg', '2016-09-02 14:26:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('55', 'admin/image/2Q7oMKga.jpg', 'http://static.xuehu365.com/admin/image/2Q7oMKga.jpg', 'Flwf4b1d6mxMzJHPy0lresy3_IXh', '22381', 'admin/image/', '16090105056088017.jpg', 'jpg', 'image/jpeg', '2016-09-02 17:13:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('56', 'admin/image/2Q7oJas8.jpg', 'http://static.xuehu365.com/admin/image/2Q7oJas8.jpg', 'FnmBh6CvakHOCAbckiLxZQiYZT2Z', '194671', 'admin/image/', '16090145113427545.jpg', 'jpg', 'image/jpeg', '2016-09-02 17:24:06', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('57', 'admin/audio/2Q7YFAXs.mp3', 'http://static.xuehu365.com/admin/audio/2Q7YFAXs.mp3', 'FmEPtrSuD0DLtKumnI1u1yNKIQvO', '477165', 'admin/audio/', '1111.mp3', 'mp3', 'audio/mp3', '2016-09-05 11:22:02', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('58', 'admin/audio/2Q7Scdaw.mp3', 'http://static.xuehu365.com/admin/audio/2Q7Scdaw.mp3', 'lht1fDgQBpjo1J2PJbz8OGIn4zgZ', '7002920', 'admin/audio/', 'Faded - Alan Walker.mp3', 'mp3', 'audio/mp3', '2016-09-06 10:26:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('59', 'admin/audio/2Q7KaGYp.mp3', 'http://static.xuehu365.com/admin/audio/2Q7KaGYp.mp3', 'FoNJttGw_HBxGA9Q64ltiW0S1-TS', '47554', 'admin/audio/', '微信网页版_webwxgetvoice(1).mp3', 'mp3', 'audio/mp3', '2016-09-07 19:26:02', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('60', 'admin/audio/2Q7KZoZH.mp3', 'http://static.xuehu365.com/admin/audio/2Q7KZoZH.mp3', 'Fnm66iH2MxPRRIQjrn2BFPHsde1j', '59866', 'admin/audio/', '微信网页版_webwxgetvoice(11).mp3', 'mp3', 'audio/mp3', '2016-09-07 19:27:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('61', 'admin/audio/2Q7KYzoB.mp3', 'http://static.xuehu365.com/admin/audio/2Q7KYzoB.mp3', 'Fj4_dMxPnhN-avOLiuX3UsMJdrfA', '928749', 'admin/audio/', '2Q7MR7C0.mp3', 'mp3', 'audio/mp3', '2016-09-07 19:31:05', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('62', 'admin/audio/2Q7KVPZV.wav', 'http://static.xuehu365.com/admin/audio/2Q7KVPZV.wav', 'FiGepdWwSmFDbgdgvQUFOK9XBTcT', '2689004', 'admin/audio/', '2Q7u1QfJ.wav', 'wav', 'audio/wav', '2016-09-07 19:45:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('63', 'admin/audio/2Q7KVCRU.m4a', 'http://static.xuehu365.com/admin/audio/2Q7KVCRU.m4a', 'Fij3PYxOSGQIWzodjVY5CySc1ckC', '562119', 'admin/audio/', '2Q7KYzoB.m4a', 'm4a', 'audio/x-m4a', '2016-09-07 19:46:10', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('64', 'admin/image/2Q7FUOaO.jpg', 'http://static.xuehu365.com/admin/image/2Q7FUOaO.jpg', 'FoKhvYZvThB_ZjVC3OpQptdmBQz3', '123231', 'admin/image/', '50a72609cae53.jpg', 'jpg', 'image/jpeg', '2016-09-08 16:20:43', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('65', 'admin/file/2Q7FU7b8.pdf', 'http://static.xuehu365.com/admin/file/2Q7FU7b8.pdf', 'FvI_HIMSy2XYZiFEH8amMIbA0EQ6', '391189', 'admin/file/', '2015年新员工入职指引.pdf', 'pdf', 'application/pdf', '2017-01-22 10:57:57', '22', '0');
INSERT INTO `t_file_mapping` VALUES ('66', 'admin/audio/2Q7EwsHq.mp3', 'http://static.xuehu365.com/admin/audio/2Q7EwsHq.mp3', 'FoNJttGw_HBxGA9Q64ltiW0S1-TS', '47554', 'admin/audio/', '微信网页版_webwxgetvoice(1).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:33:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('67', 'admin/audio/2Q7Ewmhf.mp3', 'http://static.xuehu365.com/admin/audio/2Q7Ewmhf.mp3', 'Fje-bVcIwOF-SFnzm8i1gJzd3fCG', '48418', 'admin/audio/', '微信网页版_webwxgetvoice(2).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:34:14', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('68', 'admin/audio/2Q7EwOCh.mp3', 'http://static.xuehu365.com/admin/audio/2Q7EwOCh.mp3', 'FunEHhEHxQQiV5-K5D4k6uoYTSHO', '49786', 'admin/audio/', '微信网页版_webwxgetvoice(3).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:35:48', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('69', 'admin/audio/2Q7Ew4Zg.mp3', 'http://static.xuehu365.com/admin/audio/2Q7Ew4Zg.mp3', 'Fufl8UubRLuaeO8jE8aI0RoARsYj', '57058', 'admin/audio/', '微信网页版_webwxgetvoice(4).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:37:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('70', 'admin/audio/2Q7Ew1bI.mp3', 'http://static.xuehu365.com/admin/audio/2Q7Ew1bI.mp3', 'FslD6PRlmOvnBs3lueVMLmHhvnXK', '56194', 'admin/audio/', '微信网页版_webwxgetvoice(8).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:37:15', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('71', 'admin/audio/2Q7EvxOc.mp3', 'http://static.xuehu365.com/admin/audio/2Q7EvxOc.mp3', 'FloaJZYJJHfrWUZSmUVRR_xeR2FA', '44890', 'admin/audio/', '微信网页版_webwxgetvoice(6).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:37:31', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('72', 'admin/audio/2Q7Evtmg.mp3', 'http://static.xuehu365.com/admin/audio/2Q7Evtmg.mp3', 'FuvxoknVRyNz-Fq8FY1Tds2RZ-IJ', '59074', 'admin/audio/', '微信网页版_webwxgetvoice(7).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:37:45', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('73', 'admin/audio/2Q7Evpsi.mp3', 'http://static.xuehu365.com/admin/audio/2Q7Evpsi.mp3', 'FmFgn4WESrLyPOjcBbYO4qjkqSGT', '59434', 'admin/audio/', '微信网页版_webwxgetvoice(9).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:38:00', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('74', 'admin/audio/2Q7EvZC0.mp3', 'http://static.xuehu365.com/admin/audio/2Q7EvZC0.mp3', 'FrhNQJHjcbsJ5DvmJfu0ET5_uxeF', '57994', 'admin/audio/', '微信网页版_webwxgetvoice(16).mp3', 'mp3', 'audio/mp3', '2016-09-08 18:39:04', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('75', 'admin/audio/2Q7EvVUo.wav', 'http://static.xuehu365.com/admin/audio/2Q7EvVUo.wav', 'FiGepdWwSmFDbgdgvQUFOK9XBTcT', '2689004', 'admin/audio/', '2Q7u1QfJ.wav', 'wav', 'audio/wav', '2016-09-08 18:39:18', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('76', 'admin/audio/2Q7EvQqc.m4a', 'http://static.xuehu365.com/admin/audio/2Q7EvQqc.m4a', 'Fij3PYxOSGQIWzodjVY5CySc1ckC', '562119', 'admin/audio/', '2Q7KYzoB.m4a', 'm4a', 'audio/x-m4a', '2016-09-08 18:39:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('77', 'admin/audio/2Q7EvJZV.mp3', 'http://static.xuehu365.com/admin/audio/2Q7EvJZV.mp3', 'Fj4_dMxPnhN-avOLiuX3UsMJdrfA', '928749', 'admin/audio/', '2Q7MR7C0.mp3', 'mp3', 'audio/mp3', '2016-09-08 18:40:04', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('78', 'admin/audio/2Q6CMFXA.mp3', 'http://static.xuehu365.com/admin/audio/2Q6CMFXA.mp3', 'FkK6_4jzoOfaHFYRxluvZ2CmCXkK', '58858', 'admin/audio/', '微信网页版_webwxgetvoice(18).mp3', 'mp3', 'audio/mpeg', '2016-09-19 19:40:46', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('79', 'admin/image/2Q624ZxF.png', 'http://static.xuehu365.com/admin/image/2Q624ZxF.png', 'Fi3kWT37fg71zgjwIih5w4gYRxNP', '165274', 'admin/image/', 'office.png', 'png', 'image/png', '2016-09-21 13:53:41', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('80', 'admin/image/2Q5qAz3A.png', 'http://static.xuehu365.com/admin/image/2Q5qAz3A.png', 'FsWab5wusIlBbS8MKAlPcPYB-FiC', '112775', 'admin/image/', '营销.png', 'png', 'image/png', '2016-09-23 14:43:31', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('81', 'admin/image/2Q5q8kfb.png', 'http://static.xuehu365.com/admin/image/2Q5q8kfb.png', 'Fqu2kJyodMU90wLvmQaXesVxqoTd', '137501', 'admin/image/', '让孩子成为一个独立自主的人.png', 'png', 'image/png', '2016-09-23 14:52:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('82', 'admin/image/2Q5q891D.png', 'http://static.xuehu365.com/admin/image/2Q5q891D.png', 'FjvBq-qQl2UL83MHInNe8FxG3EYA', '206148', 'admin/image/', '04.png', 'png', 'image/png', '2016-09-23 14:54:47', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('83', 'admin/image/2Q5YppKz.png', 'http://static.xuehu365.com/admin/image/2Q5YppKz.png', 'FiI7art4WmY0_ecEVJ0riraAiz_D', '101710', 'admin/image/', '共同体管理.png', 'png', 'image/png', '2016-09-26 13:54:11', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('84', 'admin/image/2Q5YBmAq.png', 'http://static.xuehu365.com/admin/image/2Q5YBmAq.png', 'FjvBq-qQl2UL83MHInNe8FxG3EYA', '206148', 'admin/image/', '04.png', 'png', 'image/png', '2016-09-26 16:33:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('85', 'admin/image/2Q5YAWcV.png', 'http://static.xuehu365.com/admin/image/2Q5YAWcV.png', 'Fl1PFLrwVoo3INaI8fXd6v8JLaHJ', '180011', 'admin/image/', '02.png', 'png', 'image/png', '2016-09-26 16:38:14', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('86', 'admin/image/2Q5Sz1nG.png', 'http://static.xuehu365.com/admin/image/2Q5Sz1nG.png', 'FpelY0borLGrSDOwVNp7cSe9-EwP', '141744', 'admin/image/', '成就正向积极的孩子.png', 'png', 'image/png', '2016-09-27 13:55:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('87', 'admin/image/2Q5SxyEu.png', 'http://static.xuehu365.com/admin/image/2Q5SxyEu.png', 'Fs3spqBuxxVUoLkJPLrnAb47v2bo', '49287', 'admin/image/', '乐视.png', 'png', 'image/png', '2016-09-27 13:59:28', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('88', 'admin/image/2Q5SuSdq.png', 'http://static.xuehu365.com/admin/image/2Q5SuSdq.png', 'Fiz8u71rwwuBh4SoVYREyheO5kz9', '157137', 'admin/image/', '03.png', 'png', 'image/png', '2016-09-27 14:13:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('89', 'admin/image/2Q5Slwdl.png', 'http://static.xuehu365.com/admin/image/2Q5Slwdl.png', 'Fl1PFLrwVoo3INaI8fXd6v8JLaHJ', '180011', 'admin/image/', '02.png', 'png', 'image/png', '2016-09-27 14:47:14', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('90', 'admin/image/2Q5NwTgE.jpg', 'http://static.xuehu365.com/admin/image/2Q5NwTgE.jpg', 'FpL3XH84RVu1iaZITh1QuDqMRpJY', '52814', 'admin/image/', '103569885.jpg', 'jpg', 'image/jpeg', '2016-09-28 10:36:45', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('91', 'admin/image/2Q5Nvoas.jpg', 'http://static.xuehu365.com/admin/image/2Q5Nvoas.jpg', 'FjKcMrfj0GIQ_mGVf-5Rqu9rg810', '75799', 'admin/image/', '635597559352343750.jpg', 'jpg', 'image/jpeg', '2016-09-28 10:39:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('92', 'admin/image/2Q5NBzIx.jpg', 'http://static.xuehu365.com/admin/image/2Q5NBzIx.jpg', 'Fjo3k7jw4XbpmtBan5Kfv5DUtCUd', '96567', 'admin/image/', '6741626376552006973.jpg', 'jpg', 'image/jpeg', '2016-09-28 13:41:25', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('93', 'admin/image/2Q5N05zy.jpg', 'http://static.xuehu365.com/admin/image/2Q5N05zy.jpg', 'FtGeJa7U-YG0KO2rLIldQGcW8jdN', '110883', 'admin/image/', 'michael.jpg', 'jpg', 'image/jpeg', '2016-09-28 14:28:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('94', 'admin/image/2Q5MAxtE.jpg', 'http://static.xuehu365.com/admin/image/2Q5MAxtE.jpg', 'FiQHNpvVryhljDr9jXEG2y0hL9Sq', '82475', 'admin/image/', '14_5.jpg', 'jpg', 'image/jpeg', '2016-09-28 17:51:45', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('95', 'admin/image/2Q5I99pc.jpg', 'http://static.xuehu365.com/admin/image/2Q5I99pc.jpg', 'Fp0CdP-i-vR7cT65A2DBkWAlQqrv', '23399', 'admin/image/', '安又琪4.jpg', 'jpg', 'image/jpeg', '2016-09-29 10:24:01', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('96', 'admin/image/2Q5GTqaO.jpg', 'http://static.xuehu365.com/admin/image/2Q5GTqaO.jpg', 'FnYE_uzoOpnM5uRfigCTYmwynRm_', '22795', 'admin/image/', 'u=3348368455,1867438270&fm=11&gp=0.jpg', 'jpg', 'image/jpeg', '2016-09-29 17:14:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('97', 'admin/image/2Q5GOHCR.jpg', 'http://static.xuehu365.com/admin/image/2Q5GOHCR.jpg', 'FlO36Su0b2Qo3tx_EspfWy7QSRur', '40634', 'admin/image/', '1436426400236055108.jpg', 'jpg', 'image/jpeg', '2016-09-29 17:36:31', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('98', 'admin/image/2Q5AyzZ0.jpg', 'http://static.xuehu365.com/admin/image/2Q5AyzZ0.jpg', 'FqQBcFCoc72lJG0CwWDApXzvbyqY', '29358', 'admin/image/', '安又琪1.jpg', 'jpg', 'image/jpeg', '2016-09-30 15:48:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('99', 'admin/image/2Q5Ayjqc.jpg', 'http://static.xuehu365.com/admin/image/2Q5Ayjqc.jpg', 'FvApWapm_oui0OVzpaf6bCRJk4cF', '36731', 'admin/image/', '安又琪3.jpg', 'jpg', 'image/jpeg', '2016-09-30 15:49:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('100', 'admin/image/2Q5AxoSm.jpg', 'http://static.xuehu365.com/admin/image/2Q5AxoSm.jpg', 'FqTnoB32pNkQYdGpNq4cb8V57dH-', '26986', 'admin/image/', '小特.jpg', 'jpg', 'image/jpeg', '2016-09-30 15:53:00', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('101', 'admin/image/2Q5AxZQh.jpg', 'http://static.xuehu365.com/admin/image/2Q5AxZQh.jpg', 'FlO36Su0b2Qo3tx_EspfWy7QSRur', '40634', 'admin/image/', '1436426400236055108.jpg', 'jpg', 'image/jpeg', '2016-09-30 15:53:57', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('102', 'admin/image/2Q3PN3O0.png', 'http://static.xuehu365.com/admin/image/2Q3PN3O0.png', 'Fiz8u71rwwuBh4SoVYREyheO5kz9', '157137', 'admin/image/', '03.png', 'png', 'image/png', '2016-10-19 09:42:41', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('103', 'admin/image/2Q3PIuXA.jpg', 'http://static.xuehu365.com/admin/image/2Q3PIuXA.jpg', 'FidsteXbVXUvjrn5tWbcBmGVk1sU', '27458', 'admin/image/', '周星驰.jpg', 'jpg', 'image/jpeg', '2016-10-19 09:59:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('104', 'admin/image/2Q3NrkbT.jpg', 'http://static.xuehu365.com/admin/image/2Q3NrkbT.jpg', 'FmArQtpjHIAr_sZnrNZn4ozLgIbK', '265996', 'admin/image/', 'Img5925745_n.jpg', 'jpg', 'image/jpeg', '2016-10-19 15:53:18', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('105', 'admin/image/2Q3NqpH3.jpg', 'http://static.xuehu365.com/admin/image/2Q3NqpH3.jpg', 'FliKDhg0D-LkRcAZgWNE9oqZNU9U', '298185', 'admin/image/', 'R-4.jpg', 'jpg', 'image/jpeg', '2016-10-19 15:56:58', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('106', 'admin/image/2Q1PDlTp.jpg', 'http://static.xuehu365.com/admin/image/2Q1PDlTp.jpg', 'FsJVSZVKpRH52NBbIeOXmGA4AgTU', '331944', 'admin/image/', '广告页模板-2.jpg', 'jpg', 'image/jpeg', '2016-11-09 15:17:20', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('107', 'admin/image/2Q1KWhXe.jpg', 'http://static.xuehu365.com/admin/image/2Q1KWhXe.jpg', 'FjYy7kuQNxxkV0-88AuHHHedTizY', '11574', 'admin/image/', '火箭.jpg', 'jpg', 'image/jpeg', '2016-11-10 10:33:29', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('108', 'admin/image/2Q1JAsuP.jpg', 'http://static.xuehu365.com/admin/image/2Q1JAsuP.jpg', 'FlcjYpJj7VJWNQ6qh8Ze_gB5dfce', '24239', 'admin/image/', '充值成功提示问题.jpg', 'jpg', 'image/jpeg', '2016-11-10 16:06:25', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('109', 'admin/image/2Q1EelZG.jpg', 'http://static.xuehu365.com/admin/image/2Q1EelZG.jpg', 'FviQUNH-G-N0nbW81c6Cb3lhWuJl', '16242', 'admin/image/', 'u=1077517299,914628865&fm=11&gp=0.jpg', 'jpg', 'image/jpeg', '2016-11-11 10:39:05', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('110', 'admin/image/2Q1EV189.jpg', 'http://static.xuehu365.com/admin/image/2Q1EV189.jpg', 'FjHkwara94L1_JR8pXJQXw6GPcm1', '91040', 'admin/image/', '5254.jpg', 'jpg', 'image/jpeg', '2016-11-11 11:17:48', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('111', 'admin/image/2Q1ENMmI.jpeg', 'http://static.xuehu365.com/admin/image/2Q1ENMmI.jpeg', 'FjEyCEFLQkYbzus1RqKvS2cdoAk8', '111533', 'admin/image/', 'mp18693383_1434153981026_53.jpeg', 'jpeg', 'image/jpeg', '2016-11-11 11:48:12', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('112', 'admin/image/2Q1DlPUH.jpg', 'http://static.xuehu365.com/admin/image/2Q1DlPUH.jpg', 'Fo_zTJ5yCXqr27Xad09Wxg9N05cR', '334853', 'admin/image/', 'fenhong_chongwu_zhu-016.jpg', 'jpg', 'image/jpeg', '2016-11-11 14:18:58', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('113', 'admin/file/2Q0ee9rh.ppt', 'http://static.xuehu365.com/admin/file/2Q0ee9rh.ppt', 'FjUsu5-wQT_mzeGilm5_-lawu4dq', '2177536', 'admin/file/', '益策月度述职报告模板20161031.ppt', 'ppt', 'application/vnd.ms-powerpoint', '2016-11-17 14:27:18', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('114', 'admin/audio/2Q0ZuaNl.mp3', 'http://static.xuehu365.com/admin/audio/2Q0ZuaNl.mp3', 'FtjwYFWspE62EP64g1YpmmZ9trMT', '59722', 'admin/audio/', '微信网页版_webwxgetvoice(61).mp3', 'mp3', 'audio/mp3', '2016-11-18 09:53:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('116', 'admin/image/2Q00VJgt.jpg', 'http://static.xuehu365.com/admin/image/2Q00VJgt.jpg', 'Fsh90KJjkCjQRhvdkxWsN3NuLL95', '11158', 'admin/image/', 'testin.jpg', 'jpg', 'image/jpeg', '2016-11-24 11:13:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('117', 'admin/audio2Pzz4404.mp3', 'http://static.xuehu365.com/admin/audio2Pzz4404.mp3', 'lmD8g-rY9gxrv1L4hU-k1kMFWkeS', '7409487', 'admin/audio', 'Starving - Hailee Steinfeld,Grey,Zedd.mp3', 'mp3', 'audio/mp3', '2016-11-24 17:07:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('118', 'admin/*/2PztxxxZ.exe', 'http://static.xuehu365.com/admin/*/2PztxxxZ.exe', 'Fh_1nb11i1zNQ6-T9LrgTWM4la2r', '1556480', 'admin/*/', 'Moo0ColorPicker.exe', 'exe', 'application/x-msdownload', '2016-11-25 14:03:25', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('122', 'admin/image/2PybBRrf.jpg', 'http://static.xuehu365.com/admin/image/2PybBRrf.jpg', 'FkRGUIHa-KnVwkO0P6xds4LrYhWg', '5983', 'admin/image/', 'ae705c3eb5220af16979cd1e694a7e87.jpg', 'jpg', 'image/jpeg', '2016-12-09 09:37:55', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('123', 'admin/video/2PyZVKgf.mov', 'http://static.xuehu365.com/admin/video/2PyZVKgf.mov', 'FtdpaWy6Giiw7xBQNVcpNIOmAowb', '2991545', 'admin/video/', '12345.mov', 'mov', 'video/quicktime', '2016-12-09 16:31:29', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('124', 'admin/image/2PyIUO4k.jpg', 'http://static.xuehu365.com/admin/image/2PyIUO4k.jpg', 'FuF45kq65MAocGu-XPYyYJ75iyCd', '79162', 'admin/image/', '033.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('125', 'admin/image/2PyIUO3p.jpg', 'http://static.xuehu365.com/admin/image/2PyIUO3p.jpg', 'FpRittTPnOh2Zz9ddEYxnro4tBUT', '157860', 'admin/image/', '073.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('126', 'admin/image/2PyIUO39.jpg', 'http://static.xuehu365.com/admin/image/2PyIUO39.jpg', 'FpIJsbr7QURyunNXbihz2727bW51', '234391', 'admin/image/', '99.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('127', 'admin/image/2PyIUNzz.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNzz.jpg', 'FvvcfigPPWsCEWVMb2rIk7a9DYUm', '191147', 'admin/image/', '277dd69f1222f2ee712006b0dd1b6992.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('128', 'admin/image/2PyIUNzQ.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNzQ.jpg', 'Fusm5vxNh1RcRE8mgi7VLInjJigi', '455445', 'admin/image/', '9172105_1435759779698_800x600.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('129', 'admin/image/2PyIUNrS.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNrS.jpg', 'FjvZOT-5QNYfZBOxRcyJDNTqkWYJ', '189219', 'admin/image/', '10058002_1437488290160_800x600.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('130', 'admin/image/2PyIUNqD.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNqD.jpg', 'Frjjg1QrcNmD2kwG4zO3B4N2ADRH', '262343', 'admin/image/', '10689934_1438699529228_800x600.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('131', 'admin/image/2PyIUNio.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNio.jpg', 'FmziwaVSAkKBCdB_Ej2HpP-6EUUE', '106858', 'admin/image/', '38066387_1409410859658_800x600.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('132', 'admin/image/2PyIUNhx.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNhx.jpg', 'FmjN-CUtfjslqXYtPfbiVJ1XIjTh', '354358', 'admin/image/', '2016011207.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('133', 'admin/image/2PyIUNXS.jpeg', 'http://static.xuehu365.com/admin/image/2PyIUNXS.jpeg', 'FvxysVA9dJta-Z3BlvMyQzVmkVOQ', '46067', 'admin/image/', '20150515015506_5cxFy.thumb.700_0.jpeg', 'jpeg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('134', 'admin/image/2PyIUNWS.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNWS.jpg', 'FvAlkzxUBGFx5iiIqU0PffxL78sY', '73041', 'admin/image/', '11449941865510539317.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('135', 'admin/image/2PyIUNVQ.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNVQ.jpg', 'Fmw63lQt0XfwcVq7GLg3oOAyDo4A', '122173', 'admin/image/', 'd87bbe3c494356cbbd8959d7cc3c569e.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('136', 'admin/image/2PyIUNU7.jpg', 'http://static.xuehu365.com/admin/image/2PyIUNU7.jpg', 'FiAD1YZqzlNEqcLfFlO4V4xJoo6G', '40507', 'admin/image/', 'view.jpg', 'jpg', 'image/jpeg', '2016-12-12 14:21:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('137', 'admin/video/2Py61aoR.mp4', 'http://static.xuehu365.com/admin/video/2Py61aoR.mp4', 'FjhQqGHdvs8KNAumTxitfxIm9HwA', '1837815', 'admin/video/', 'ping20s[2].mp4', 'mp4', 'video/mp4', '2016-12-14 17:31:30', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('138', 'admin/image/2PxLpw6N.gif', 'http://static.xuehu365.com/admin/image/2PxLpw6N.gif', 'FhzJi79pYPlhhR-6ArGxcmvv2igr', '2418', 'admin/image/', 'loading.gif', 'gif', 'image/gif', '2016-12-22 15:06:20', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('139', 'admin/image/2PxB7twF.png', 'http://static.xuehu365.com/admin/image/2PxB7twF.png', 'FgMfITUAo2Z1PMXhnmP15ULfd-hZ', '2968', 'admin/image/', 'file_icon_sum.png', 'png', 'image/png', '2016-12-24 11:03:58', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('140', 'admin/audio/2PwDhNyt.amr', 'http://static.xuehu365.com/admin/audio/2PwDhNyt.amr', 'Fg1a1eJ0bnkpdHxMAloH4UPNyDed', '44278', 'admin/audio/', '2ch12000hz128bit20170103T142454.amr', 'amr', 'application/octet-stream', '2017-01-03 14:59:21', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('141', 'admin/audio/2PvxLxDj.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLxDj.mp3', 'FoNJttGw_HBxGA9Q64ltiW0S1-TS', '47554', 'admin/audio/', '微信网页版_webwxgetvoice(1).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('142', 'admin/audio/2PvxLxCu.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLxCu.mp3', 'Fje-bVcIwOF-SFnzm8i1gJzd3fCG', '48418', 'admin/audio/', '微信网页版_webwxgetvoice(2).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('143', 'admin/audio/2PvxLxCQ.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLxCQ.mp3', 'FunEHhEHxQQiV5-K5D4k6uoYTSHO', '49786', 'admin/audio/', '微信网页版_webwxgetvoice(3).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('144', 'admin/audio/2PvxLxBv.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLxBv.mp3', 'Fufl8UubRLuaeO8jE8aI0RoARsYj', '57058', 'admin/audio/', '微信网页版_webwxgetvoice(4).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('145', 'admin/audio/2PvxLxBO.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLxBO.mp3', 'FsVgwfc4JuObiBcbHYftAV2AVxg0', '56986', 'admin/audio/', '微信网页版_webwxgetvoice(5).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('146', 'admin/audio/2PvxLxAs.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLxAs.mp3', 'FloaJZYJJHfrWUZSmUVRR_xeR2FA', '44890', 'admin/audio/', '微信网页版_webwxgetvoice(6).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('147', 'admin/audio/2PvxLxAK.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLxAK.mp3', 'FuvxoknVRyNz-Fq8FY1Tds2RZ-IJ', '59074', 'admin/audio/', '微信网页版_webwxgetvoice(7).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('148', 'admin/audio/2PvxLx9m.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLx9m.mp3', 'FslD6PRlmOvnBs3lueVMLmHhvnXK', '56194', 'admin/audio/', '微信网页版_webwxgetvoice(8).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('149', 'admin/audio/2PvxLx9J.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLx9J.mp3', 'FmFgn4WESrLyPOjcBbYO4qjkqSGT', '59434', 'admin/audio/', '微信网页版_webwxgetvoice(9).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('150', 'admin/audio/2PvxLx8q.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLx8q.mp3', 'FmgTiTlqwuKPW_u22URIBPCOORG_', '55258', 'admin/audio/', '微信网页版_webwxgetvoice(10).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:04:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('151', 'admin/audio/2PvxLlCu.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLlCu.mp3', 'Fnm66iH2MxPRRIQjrn2BFPHsde1j', '59866', 'admin/audio/', '微信网页版_webwxgetvoice(11).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('152', 'admin/audio/2PvxLlC2.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLlC2.mp3', 'Fpr--PaoVHx1nvE8v_Bv-DWW-GxI', '26890', 'admin/audio/', '微信网页版_webwxgetvoice(12).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('153', 'admin/audio/2PvxLlBY.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLlBY.mp3', 'Fmce16CEgJXmoPfp2cMeVzOQFC7k', '59866', 'admin/audio/', '微信网页版_webwxgetvoice(13).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('154', 'admin/audio/2PvxLlAz.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLlAz.mp3', 'Fg1_V9Xdq1fKb--_ydRejh_GAkFG', '57562', 'admin/audio/', '微信网页版_webwxgetvoice(14).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('155', 'admin/audio/2PvxLlAU.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLlAU.mp3', 'FiynykcDA1ZUNahhAsEhh3cVajGa', '55618', 'admin/audio/', '微信网页版_webwxgetvoice(15).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('156', 'admin/audio/2PvxLlA0.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLlA0.mp3', 'FrhNQJHjcbsJ5DvmJfu0ET5_uxeF', '57994', 'admin/audio/', '微信网页版_webwxgetvoice(16).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('157', 'admin/audio/2PvxLl9W.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLl9W.mp3', 'FgE4NSudE2_PF2kYpZ5sphd5if8m', '57058', 'admin/audio/', '微信网页版_webwxgetvoice(17).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('158', 'admin/audio/2PvxLl90.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLl90.mp3', 'FkK6_4jzoOfaHFYRxluvZ2CmCXkK', '58858', 'admin/audio/', '微信网页版_webwxgetvoice(18).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('159', 'admin/audio/2PvxLl8R.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLl8R.mp3', 'Fp6odxhrk9V_mBzuzehH06R-wnhX', '55978', 'admin/audio/', '微信网页版_webwxgetvoice(19).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('160', 'admin/audio/2PvxLl7h.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLl7h.mp3', 'Fn54hcND5_PnKgrktCw7aOkjzjjv', '27250', 'admin/audio/', '微信网页版_webwxgetvoice(20).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:05:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('161', 'admin/audio/2PvxLd2a.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLd2a.mp3', 'FoLRBK_dmwJ-4nDs_FJD19bymJmE', '56050', 'admin/audio/', '微信网页版_webwxgetvoice(21).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('162', 'admin/audio/2PvxLd1u.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLd1u.mp3', 'FsfVKElqB4PMZ4JJVNe65PNUjlvl', '59650', 'admin/audio/', '微信网页版_webwxgetvoice(22).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('163', 'admin/audio/2PvxLd1D.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLd1D.mp3', 'Fl_BuDsbKToy3xzBxEwDVisJ62LX', '29554', 'admin/audio/', '微信网页版_webwxgetvoice(23).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('164', 'admin/audio/2PvxLd0W.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLd0W.mp3', 'FgWIZAK5qD2L4NyiInzmcEdnF3ay', '54538', 'admin/audio/', '微信网页版_webwxgetvoice(24).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('165', 'admin/audio/2PvxLd01.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLd01.mp3', 'FgjroOXVkFTrNPTxwYibTDt-q2J2', '57922', 'admin/audio/', '微信网页版_webwxgetvoice(25).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('166', 'admin/audio/2PvxLWa1.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLWa1.mp3', 'FsxRgxBVxoQa38a9qSjAZ6tkKQBk', '56698', 'admin/audio/', '微信网页版_webwxgetvoice(26).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:33', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('167', 'admin/audio/2PvxLWZJ.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLWZJ.mp3', 'FnM2MBW7YgnklnIFvhDI8NTEuKBO', '38554', 'admin/audio/', '微信网页版_webwxgetvoice(27).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:33', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('168', 'admin/audio/2PvxLWYr.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLWYr.mp3', 'Fle1iu_jAIY13ocI3tnuRpQiOyB1', '39490', 'admin/audio/', '微信网页版_webwxgetvoice(28).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:33', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('169', 'admin/audio/2PvxLWYO.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLWYO.mp3', 'Fmur4w2tWthTesL-BGrkn2AmQVbB', '58282', 'admin/audio/', '微信网页版_webwxgetvoice(29).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:33', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('170', 'admin/audio/2PvxLWXq.mp3', 'http://static.xuehu365.com/admin/audio/2PvxLWXq.mp3', 'FhKriWCh1fWOtVN8bIEHYKpziT97', '54682', 'admin/audio/', '微信网页版_webwxgetvoice(30).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:06:33', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('171', 'admin/file/2PvxLIMH.xls', 'http://static.xuehu365.com/admin/file/2PvxLIMH.xls', 'FvvDt4mT7i143yba_CIfifDTJFkO', '47616', 'admin/file/', '平台迁移回归测试列表.xls', 'xls', 'application/vnd.ms-excel', '2017-01-06 10:07:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('182', 'admin/audio/2PvxKxiw.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKxiw.mp3', 'Fhkb_d7ui4fnQ0aiI_281rpoWGc3', '59434', 'admin/audio/', '微信网页版_webwxgetvoice(31).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:08:47', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('183', 'admin/audio/2PvxKtSL.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtSL.mp3', 'Fm6LD9BSjFqBmeUOUu5CX44MZFFS', '59866', 'admin/audio/', '微信网页版_webwxgetvoice(32).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('184', 'admin/audio/2PvxKtRe.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtRe.mp3', 'Fkp3VFq3ALLL5ZbHD1we0rX9Q2Dc', '56266', 'admin/audio/', '微信网页版_webwxgetvoice(33).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('185', 'admin/audio/2PvxKtR0.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtR0.mp3', 'FtgmKBtlQ5K1ko-MhQUjNU2cc1Zb', '53170', 'admin/audio/', '微信网页版_webwxgetvoice(34).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('186', 'admin/audio/2PvxKtQU.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtQU.mp3', 'FjRAI140s5Njoz_9kCFYJwWrR3uv', '54394', 'admin/audio/', '微信网页版_webwxgetvoice(35).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('187', 'admin/audio/2PvxKtPj.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtPj.mp3', 'FhZaesuzvyWbqv39SI4NpsRxspHB', '35530', 'admin/audio/', '微信网页版_webwxgetvoice(36).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('188', 'admin/audio/2PvxKtPD.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtPD.mp3', 'Fl7vEWLJsSo-x18UGYW2JT6LiPQS', '28114', 'admin/audio/', '微信网页版_webwxgetvoice(37).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('189', 'admin/audio/2PvxKtOi.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtOi.mp3', 'FkxpYlg8ODPkpPvkl8X8zZh-G-i2', '54538', 'admin/audio/', '微信网页版_webwxgetvoice(38).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('190', 'admin/audio/2PvxKtO4.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtO4.mp3', 'FskJuGhYgTxC4CiZPajCFTZur5a2', '47626', 'admin/audio/', '微信网页版_webwxgetvoice(39).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('191', 'admin/audio/2PvxKtNN.mp3', 'http://static.xuehu365.com/admin/audio/2PvxKtNN.mp3', 'FvTcsXXinQ9fRdkzbS51o133c-xW', '54034', 'admin/audio/', '微信网页版_webwxgetvoice(40).mp3', 'mp3', 'audio/mp3', '2017-01-06 10:09:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('192', 'admin/video/2PunQFBX.mp4', 'http://static.xuehu365.com/admin/video/2PunQFBX.mp4', 'lk6Eq67mQCTD6QGf-5YtMVX-I_4d', '9827570', 'admin/video/', 'Starving - Hailee Steinfeld.mp4', 'mp4', 'video/mp4', '2017-01-18 17:19:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('199', 'app/file2PubkTKSf1.xlsx', 'http://static.xuehu365.com/app/file2PubkTSf.xlsx', '1152020184', '10000', null, '我可以肯定地告诉大家，中国开放的大门不会关上，只会越来越大。中国经济发展前景是光明的，能为外商投资带来丰厚回报，为各国企业提供广阔机遇。.xlsx', 'xls', 'application/xls', '2017-01-22 11:28:30', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('200', 'admin/file/2PuRSI7T.doc', 'http://static.xuehu365.com/admin/file/2PuRSI7T.doc', 'Fnm7hTvsvD3E_uW8_SMg6IRdlGYe', '1207296', 'admin/file/', '学乎客户端接口规范-2.0.doc', 'doc', 'application/msword', '2017-01-22 11:29:15', '7', '0');
INSERT INTO `t_file_mapping` VALUES ('201', 'admin/file/2PuRSGYZ.docx', 'http://static.xuehu365.com/admin/file/2PuRSGYZ.docx', 'FiRoYp3ru81qa6mxEq-U0QUSdgAM', '469840', 'admin/file/', '课程重构一期修改-技术 08-12.docx', 'docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2017-01-22 11:29:21', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('202', 'admin/file/2PuRS9Vz.xlsx', 'http://static.xuehu365.com/admin/file/2PuRS9Vz.xlsx', 'Fje7woCOms_X7ae__N1RoU0jR9g7', '31081', 'admin/file/', '测试用例03011.xlsx', 'xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2017-01-22 11:29:49', '5', '0');
INSERT INTO `t_file_mapping` VALUES ('203', 'admin/file/2PuRS3Nv.txt', 'http://static.xuehu365.com/admin/file/2PuRS3Nv.txt', 'FkA5VuPDxyX9ERb8766OXpz9gfAB', '292', 'admin/file/', '938-课程运营.txt', 'txt', 'text/plain', '2017-01-22 11:30:12', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('204', 'admin/file/2PuRPmuz.ppt', 'http://static.xuehu365.com/admin/file/2PuRPmuz.ppt', 'FlJ0Dnu1FavlFH2o0M5IonlNDtJx', '1807360', 'admin/file/', '益策月度述职报告.ppt', 'ppt', 'application/vnd.ms-powerpoint', '2017-01-22 11:39:12', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('205', 'admin/image/2PsvsUmd.jpg', 'http://static.xuehu365.com/admin/image/2PsvsUmd.jpg', 'FgpGmiKul9oNMnKQJq-gCGLCFApX', '162947', 'admin/image/', '16121527154002805.jpg', 'jpg', 'image/jpeg', '2017-02-07 11:34:46', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('206', 'admin/image/2PsqZWrH.jpg', 'http://static.xuehu365.com/admin/image/2PsqZWrH.jpg', 'FmziwaVSAkKBCdB_Ej2HpP-6EUUE', '106858', 'admin/image/', '38066387_1409410859658_800x600.jpg', 'jpg', 'image/jpeg', '2017-02-08 09:21:28', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('207', 'admin/image/2PsqZTRi.jpg', 'http://static.xuehu365.com/admin/image/2PsqZTRi.jpg', 'Fusm5vxNh1RcRE8mgi7VLInjJigi', '455445', 'admin/image/', '9172105_1435759779698_800x600.jpg', 'jpg', 'image/jpeg', '2017-02-08 09:21:41', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('208', 'app/file/2Psq1iVq.docx', 'http://static.xuehu365.com/app/file/2Psq1iVq.docx', '128446996', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-02-08 11:35:46', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('209', 'admin/video/2Psq0bhi.mp4', 'http://static.xuehu365.com/admin/video/2Psq0bhi.mp4', 'lmxxzCaK3dR1wD2YHF4GhiF2y7Ul', '10023522', 'admin/video/', 'xiaoming.mp4', 'mp4', 'video/mp4', '2017-02-08 11:40:11', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('210', 'app/file/2Pspx7fD.ppt', 'http://static.xuehu365.com/app/file/2Pspx7fD.ppt', '267535306', '742912', null, '移动应用测试案例.ppt', 'ppt', 'application/ppt', '2017-02-08 11:54:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('211', 'admin/image/2PspwE3R.jpg', 'http://static.xuehu365.com/admin/image/2PspwE3R.jpg', 'Fv62e3f6tr0uNEAT0QuLtjaFnuz_', '75697', 'admin/image/', '16042209289492678.jpg', 'jpg', 'image/jpeg', '2017-02-08 11:57:35', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('212', 'admin/image/2PspVRj1.jpg', 'http://static.xuehu365.com/admin/image/2PspVRj1.jpg', 'FohCISdbnctyHwkWAsP-rrCk5B5n', '121569', 'admin/image/', 'QQ图片20170208134335.jpg', 'jpg', 'image/jpeg', '2017-02-08 13:43:57', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('213', 'app/file/2PspHTCL.xlsx', 'http://static.xuehu365.com/app/file/2PspHTCL.xlsx', '127782952', '22350', null, '安装、卸载、升级、兼容适配、前后台切换、交叉通用APP测试用例V0.1.xlsx', 'xls', 'application/xls', '2017-02-08 14:39:28', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('214', 'app/file/2PspHJ6W.pdf', 'http://static.xuehu365.com/app/file/2PspHJ6W.pdf', '174768353', '391189', null, '201606201346285d9e1476-3e3e-43db-91b9-dd0e9bf36257.pdf', 'pdf', 'application/pdf', '2017-02-08 14:40:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('215', 'app/file/2PspGvjn.txt', 'http://static.xuehu365.com/app/file/2PspGvjn.txt', '155551307', '31147', null, '3416a75f4cea9109507cacd8e2f2aefc.txt', 'txt', 'application/txt', '2017-02-08 14:41:37', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('216', 'app/file/2PspFuWu.txt', 'http://static.xuehu365.com/app/file/2PspFuWu.txt', '684526420', '8033', null, 'log1.txt', 'txt', 'application/txt', '2017-02-08 14:45:42', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('217', 'app/file/2PspFjYF.txt', 'http://static.xuehu365.com/app/file/2PspFjYF.txt', '585609087', '14658', null, 'log1.txt', 'txt', 'application/txt', '2017-02-08 14:46:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('218', 'app/file/2PspED9j.doc', 'http://static.xuehu365.com/app/file/2PspED9j.doc', '184896930', '23552', null, '15082811554376060-1.doc', 'doc', 'application/doc', '2017-02-08 14:52:25', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('219', 'admin/image/2PsolOxH.jpeg', 'http://static.xuehu365.com/admin/image/2PsolOxH.jpeg', 'FiP6DPg2HqqntKrtX5Cmb3mcjab6', '13483', 'admin/image/', 'b395718a468544f4ba4bc2aae71e9c8f20170208140739.jpeg', 'jpeg', 'image/jpeg', '2017-02-08 16:46:51', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('221', 'admin/video/2PsjJwmp.mp4', 'http://static.xuehu365.com/admin/video/2PsjJwmp.mp4', 'lkkRewpZHoKyyWF6GP0NgcptF1aI', '4917089', 'admin/video/', '测试视频3.mp4', 'mp4', 'video/mp4', '2017-02-09 15:07:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('222', 'admin/application/2PsjC4kO.pdf', 'http://static.xuehu365.com/admin/application/2PsjC4kO.pdf', 'Fu2WbvU_5ZTBjTksSqhA0T5-d5F9', '1575682', 'admin/application/', '2016年资产证券化发展报告(1).pdf', 'pdf', 'application/pdf', '2017-02-09 15:38:32', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('223', 'app/file/2Psj6j3A.txt', 'http://static.xuehu365.com/app/file/2Psj6j3A.txt', '182244354', '5406102', null, '专项@app测试.txt', 'txt', 'application/txt', '2017-02-09 16:01:00', '4', '0');
INSERT INTO `t_file_mapping` VALUES ('224', 'app/file/2PsiyNCy.doc', 'http://static.xuehu365.com/app/file/2PsiyNCy.doc', '216303476', '566784', null, '学乎客户端接口规范（最新）.doc', 'doc', 'application/doc', '2017-02-09 16:33:01', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('225', 'app/file/2PsiyIyQ.ppt', 'http://static.xuehu365.com/app/file/2PsiyIyQ.ppt', '117820721', '742912', null, '移动应用测试案例.ppt', 'ppt', 'application/ppt', '2017-02-09 16:33:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('226', 'app/file/2PsiyEL0.docx', 'http://static.xuehu365.com/app/file/2PsiyEL0.docx', '241426880', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-02-09 16:33:48', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('227', 'app/file/2Psiy5cK.pptx', 'http://static.xuehu365.com/app/file/2Psiy5cK.pptx', '119385327', '1423902', null, 'Python编程入门.pptx', 'ppt', 'application/ppt', '2017-02-09 16:34:10', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('228', 'app/file/2PsixzHU.xlsx', 'http://static.xuehu365.com/app/file/2PsixzHU.xlsx', '45269595', '34429', null, 'app-2.0-0921.xlsx', 'xls', 'application/xls', '2017-02-09 16:34:29', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('229', 'app/file/2Psixpec.xls', 'http://static.xuehu365.com/app/file/2Psixpec.xls', '227447898', '47616', null, '平台迁移回归测试列表.xls', 'xls', 'application/xls', '2017-02-09 16:35:08', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('230', 'app/file/2Psixg43.pdf', 'http://static.xuehu365.com/app/file/2Psixg43.pdf', '264519351', '391189', null, '201606201346285d9e1476-3e3e-43db-91b9-dd0e9bf36257.pdf', 'pdf', 'application/pdf', '2017-02-09 16:35:44', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('231', 'app/file/2PsixZih.txt', 'http://static.xuehu365.com/app/file/2PsixZih.txt', '180150589', '357', null, 'android-0927-bug.txt', 'txt', 'application/txt', '2017-02-09 16:36:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('232', 'app/file/2Psegq3C.xlsx', 'http://static.xuehu365.com/app/file/2Psegq3C.xlsx', '3176804', '22350', null, '安装、卸载、升级、兼容适配、前后台切换、交叉通用APP测试用例V0.1.xlsx', 'xls', 'application/xls', '2017-02-10 10:07:42', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('233', 'app/file/2PsdcVXe.pdf', 'http://static.xuehu365.com/app/file/2PsdcVXe.pdf', '1291245116', '391338', null, '马云最近的思考.pdf', 'pdf', 'application/pdf', '2017-02-10 14:31:28', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('234', 'admin/video/2PsdK3iG.mp4', 'http://static.xuehu365.com/admin/video/2PsdK3iG.mp4', 'Fp2uUqBI_O_rwU2BeZcyqYQ_4scc', '2237121', 'admin/video/', '4b6f0cfa8efeb01447a296e304fe2564.mp4', 'mp4', 'video/mp4', '2017-02-10 15:44:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('235', 'admin/video/2PsdJUFu.mp4', 'http://static.xuehu365.com/admin/video/2PsdJUFu.mp4', 'FuO4jj7usBvMSCZts88Zyqk21FAF', '2334068', 'admin/video/', '张学友-2.mp4', 'mp4', 'video/mp4', '2017-02-10 15:46:43', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('236', 'admin/video/2PsdJOpn.mp4', 'http://static.xuehu365.com/admin/video/2PsdJOpn.mp4', 'Fp2uUqBI_O_rwU2BeZcyqYQ_4scc', '2237121', 'admin/video/', '张学友-1.mp4', 'mp4', 'video/mp4', '2017-02-10 15:47:04', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('237', 'admin/video/2PsdJItw.mp4', 'http://static.xuehu365.com/admin/video/2PsdJItw.mp4', 'FkWLNlK_2Wj2ij89_iw3j3f-uDiS', '2401669', 'admin/video/', '张学友-3.mp4', 'mp4', 'video/mp4', '2017-02-10 15:47:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('238', 'admin/video/2PsdJFnq.mp4', 'http://static.xuehu365.com/admin/video/2PsdJFnq.mp4', 'Ft4yyTYKh_xXx0JqVr5dPYKVWMu3', '2202566', 'admin/video/', '张学友-4.mp4', 'mp4', 'video/mp4', '2017-02-10 15:47:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('239', 'admin/video/2PsdJCt1.mp4', 'http://static.xuehu365.com/admin/video/2PsdJCt1.mp4', 'FrWFVKCAQq1IWclXl7mZr0a3sgzE', '2306667', 'admin/video/', '张学友-5.mp4', 'mp4', 'video/mp4', '2017-02-10 15:47:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('240', 'admin/video/2PsdJ721.mp4', 'http://static.xuehu365.com/admin/video/2PsdJ721.mp4', 'FlEqle8NtudvzwhC1jLLp8HMIzlo', '2273296', 'admin/video/', '张学友-6.mp4', 'mp4', 'video/mp4', '2017-02-10 15:48:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('241', 'admin/video/2PsdJ3IJ.mp4', 'http://static.xuehu365.com/admin/video/2PsdJ3IJ.mp4', 'Fo-lO9eoOaJVf4nX5vwSON8hyVKq', '984445', 'admin/video/', 'Running Man20170101.mp4', 'mp4', 'video/mp4', '2017-02-10 15:48:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('242', 'admin/image/2PsdIrIo.jpg', 'http://static.xuehu365.com/admin/image/2PsdIrIo.jpg', 'FtFCkPbGIqAL2n8Mvb6us_d6ARAU', '25793', 'admin/image/', '4b6f0cfa8efeb01447a296e304fe2564.jpg', 'jpg', 'image/jpeg', '2017-02-10 15:49:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('243', 'admin/image/2PsdIrI5.jpg', 'http://static.xuehu365.com/admin/image/2PsdIrI5.jpg', 'Fh3pwG2PQwcVWaPi_si4YxKpufwE', '26137', 'admin/image/', '8bfed65f858338b1946b199751760ff4.jpg', 'jpg', 'image/jpeg', '2017-02-10 15:49:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('244', 'admin/image/2PsdIrHa.jpg', 'http://static.xuehu365.com/admin/image/2PsdIrHa.jpg', 'FkG2J1dbAJ8--ELfCt1FbxnQtk3U', '24374', 'admin/image/', '64b10fb3fa0c4c148c104cb95ad8a9a5.jpg', 'jpg', 'image/jpeg', '2017-02-10 15:49:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('245', 'admin/image/2PsdIrH7.jpg', 'http://static.xuehu365.com/admin/image/2PsdIrH7.jpg', 'Fr31RPVf-2VzzVJq9rYYhOw9Yhuk', '18701', 'admin/image/', '906cd9bfe71590f2a050a947c9d3b68e.jpg', 'jpg', 'image/jpeg', '2017-02-10 15:49:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('246', 'admin/image/2PsdIrG3.jpg', 'http://static.xuehu365.com/admin/image/2PsdIrG3.jpg', 'FoRJyX2V8zICr6umU7TGmEd3nMxE', '19284', 'admin/image/', '49611722be6fabab75c2a61719eb1816.jpg', 'jpg', 'image/jpeg', '2017-02-10 15:49:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('247', 'admin/image/2PsdIrE9.jpg', 'http://static.xuehu365.com/admin/image/2PsdIrE9.jpg', 'FqLHizuEM3oQlQD448sIJD3lKXeB', '13653', 'admin/image/', 'd7c44cb8819590a7c97554d8339fba0e.jpg', 'jpg', 'image/jpeg', '2017-02-10 15:49:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('248', 'admin/image/2PsdIrCz.jpg', 'http://static.xuehu365.com/admin/image/2PsdIrCz.jpg', 'FsqipZWXAylSkDQorTouFBZ_HoQl', '31042', 'admin/image/', 'f97214986ccaee29e6fe8b5b9bafd105.jpg', 'jpg', 'image/jpeg', '2017-02-10 15:49:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('249', 'app/file/2PscvReH.ppt', 'http://static.xuehu365.com/app/file/2PscvReH.ppt', '32299211', '742912', null, '移动应用测试案例.ppt', 'ppt', 'application/ppt', '2017-02-10 17:22:15', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('250', 'admin/image/2PsNJ64U.jpg', 'http://static.xuehu365.com/admin/image/2PsNJ64U.jpg', 'FmMdlIQ2UOzIrjYjA43T30n_Mtxm', '6091', 'admin/image/', 'u=1149649067,2598408610&fm=21&gp=0.jpg', 'jpg', 'image/jpeg', '2017-02-13 09:28:38', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('251', 'admin/image/2PsNJ3au.jpg', 'http://static.xuehu365.com/admin/image/2PsNJ3au.jpg', 'FhNa-oRjaAj3by-ytW8S1zGUVH4m', '7954', 'admin/image/', 'u=1151303498,4188597331&fm=21&gp=0.jpg', 'jpg', 'image/jpeg', '2017-02-13 09:28:47', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('262', 'app/file/2PsMnKK2.docx', 'http://static.xuehu365.com/app/file/2PsMnKK2.docx', '752477209', '20161', null, '学付宝使用协议.docx', 'doc', 'application/doc', '2017-02-13 11:34:49', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('263', 'app/file/2PsM1kdN.doc', 'http://static.xuehu365.com/app/file/2PsM1kdN.doc', '264687365', '566784', null, '学乎客户端接口规范（最新）.doc', 'doc', 'application/doc', '2017-02-13 14:43:50', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('264', 'app/file/2PsLmQcV.doc', 'http://static.xuehu365.com/app/file/2PsLmQcV.doc', '168225914', '566784', null, '2PsiyNCy.doc', 'doc', 'application/doc', '2017-02-13 15:44:43', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('265', 'app/file/2PsLmC1B.docx', 'http://static.xuehu365.com/app/file/2PsLmC1B.docx', '141629689', '103357', null, '2PsiyEL0.docx', 'doc', 'application/doc', '2017-02-13 15:45:37', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('266', 'app/file/2PsLm9Ks.pptx', 'http://static.xuehu365.com/app/file/2PsLm9Ks.pptx', '129483321', '1423902', null, 'Python编程入门.pptx', 'ppt', 'application/ppt', '2017-02-13 15:45:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('267', 'app/file/2PsLm6G3.ppt', 'http://static.xuehu365.com/app/file/2PsLm6G3.ppt', '146426892', '742912', null, '移动应用测试案例.ppt', 'ppt', 'application/ppt', '2017-02-13 15:46:01', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('268', 'app/file/2PsLm2XI.xlsx', 'http://static.xuehu365.com/app/file/2PsLm2XI.xlsx', '76292201', '34429', null, 'app-2.0-0921.xlsx', 'xls', 'application/xls', '2017-02-13 15:46:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('269', 'app/file/2PsLm0Hb.xls', 'http://static.xuehu365.com/app/file/2PsLm0Hb.xls', '225024166', '47616', null, '平台迁移回归测试列表.xls', 'xls', 'application/xls', '2017-02-13 15:46:22', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('270', 'app/file/2PsLkxYz.pdf', 'http://static.xuehu365.com/app/file/2PsLkxYz.pdf', '92217706', '391338', null, '马云最近的思考.pdf', 'pdf', 'application/pdf', '2017-02-13 15:50:32', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('271', 'app/file/2PsLkqh8.txt', 'http://static.xuehu365.com/app/file/2PsLkqh8.txt', '69417221', '5406102', null, '专项@app测试.txt', 'txt', 'application/txt', '2017-02-13 15:51:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('272', 'app/file/2PsLk5vc.doc', 'http://static.xuehu365.com/app/file/2PsLk5vc.doc', '253484886', '43008', null, '2016教育年会行程 11.09.doc', 'doc', 'application/doc', '2017-02-13 15:54:02', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('273', 'app/file/2PsLk20q.doc', 'http://static.xuehu365.com/app/file/2PsLk20q.doc', '260473886', '17408', null, '礼仪指引话术-附件2.doc', 'doc', 'application/doc', '2017-02-13 15:54:17', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('274', 'app/file/2PsLjtiM.doc', 'http://static.xuehu365.com/app/file/2PsLjtiM.doc', '246976425', '3349556', null, '1471933018[需求]隐藏可点播和显示面授开课信息151207 (1).doc', 'doc', 'application/doc', '2017-02-13 15:55:01', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('275', 'app/file/2PsLgDF2.txt', 'http://static.xuehu365.com/app/file/2PsLgDF2.txt', '129550021', '5406102', null, '专项@app测试.txt', 'txt', 'application/txt', '2017-02-13 16:09:33', '4', '0');
INSERT INTO `t_file_mapping` VALUES ('276', 'app/file/2PsLWits.pptx', 'http://static.xuehu365.com/app/file/2PsLWits.pptx', '2634569', '1423902', null, 'Python编程入门.pptx', 'ppt', 'application/ppt', '2017-02-13 16:47:07', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('277', 'app/file/2PsGE8A2.docx', 'http://static.xuehu365.com/app/file/2PsGE8A2.docx', '181936212', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-02-14 14:32:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('278', 'app/file/2PsGDBJx.xlsx', 'http://static.xuehu365.com/app/file/2PsGDBJx.xlsx', '626644954', '23386', null, '跨界学院产品内容核对清单0610.xlsx', 'xls', 'application/xls', '2017-02-14 14:36:01', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('279', 'app/file/2PsGDARm.docx', 'http://static.xuehu365.com/app/file/2PsGDARm.docx', '37300200', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-02-14 14:36:22', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('280', 'app/file/2PsFxV5F.xlsx', 'http://static.xuehu365.com/app/file/2PsFxV5F.xlsx', '138174084', '22350', null, '安装、卸载、升级、兼容适配、前后台切换、交叉通用APP测试用例V0.1.xlsx', 'xls', 'application/xls', '2017-02-14 15:38:19', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('281', 'app/file/2PsFtJ7Y.doc', 'http://static.xuehu365.com/app/file/2PsFtJ7Y.doc', '220021628', '566784', null, '学乎客户端接口规范（最新）.doc', 'doc', 'application/doc', '2017-02-14 15:55:00', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('282', 'app/file/2PsFtGrb.pdf', 'http://static.xuehu365.com/app/file/2PsFtGrb.pdf', '222496608', '391338', null, '马云最近的思考.pdf', 'pdf', 'application/pdf', '2017-02-14 15:55:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('283', 'app/file/2PsFtBX7.txt', 'http://static.xuehu365.com/app/file/2PsFtBX7.txt', '88793845', '5406102', null, '专项@app测试.txt', 'txt', 'application/txt', '2017-02-14 15:55:38', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('284', 'app/file/2PsFt6Cx.xlsx', 'http://static.xuehu365.com/app/file/2PsFt6Cx.xlsx', '168704965', '22350', null, '安装、卸载、升级、兼容适配、前后台切换、交叉通用APP测试用例V0.1.xlsx', 'xls', 'application/xls', '2017-02-14 15:55:48', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('285', 'app/file/2PsFt4FB.pptx', 'http://static.xuehu365.com/app/file/2PsFt4FB.pptx', '159724469', '1423902', null, 'Python编程入门.pptx', 'ppt', 'application/ppt', '2017-02-14 15:55:59', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('286', 'app/file/2PsFoukl.pdf', 'http://static.xuehu365.com/app/file/2PsFoukl.pdf', '187122505', '5849996', null, '虫师_RobotFramewok自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-14 16:12:43', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('287', 'app/file/2PsFp3XR.pdf', 'http://static.xuehu365.com/app/file/2PsFp3XR.pdf', '249393648', '9025284', null, 'Selenium 1.0 初学者指南.pdf', 'pdf', 'application/pdf', '2017-02-14 16:13:14', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('288', 'app/file/2PsFojKz.pdf', 'http://static.xuehu365.com/app/file/2PsFojKz.pdf', '40497766', '5787859', null, 'selenium2 python自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-14 16:14:10', '3', '0');
INSERT INTO `t_file_mapping` VALUES ('289', 'app/file/2PsFlo1I.pdf', 'http://static.xuehu365.com/app/file/2PsFlo1I.pdf', '242336356', '5787859', null, 'selenium2 python自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-14 16:24:56', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('290', 'app/file/2PsFljd4.pdf', 'http://static.xuehu365.com/app/file/2PsFljd4.pdf', '111330891', '3426047', null, 'Selenium_Testing_Tools_Cookbook.pdf', 'pdf', 'application/pdf', '2017-02-14 16:25:10', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('291', 'app/file/2PsFkFbQ.pdf', 'http://static.xuehu365.com/app/file/2PsFkFbQ.pdf', '60829316', '9025284', null, 'Selenium 1.0 初学者指南.pdf', 'pdf', 'application/pdf', '2017-02-14 16:31:56', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('292', 'app/file/2PsFjUUx.pdf', 'http://static.xuehu365.com/app/file/2PsFjUUx.pdf', '95030686', '5849996', null, '虫师_RobotFramewok自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-14 16:34:07', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('297', 'admin/image/2PsBd88D.jpg', 'http://static.xuehu365.com/admin/image/2PsBd88D.jpg', 'FiX-GEJCjwTB9aMx9ofdDU7hPkpq', '5839', 'admin/image/', 'u=4050684637,870783349&fm=21&gp=0.jpg', 'jpg', 'image/jpeg', '2017-02-15 09:24:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('298', 'admin/image/2PsBd6vt.jpg', 'http://static.xuehu365.com/admin/image/2PsBd6vt.jpg', 'Fq7J9IfZiDbSkxRmx9__HMKh0ObA', '9978', 'admin/image/', 'u=4198594792,1119433812&fm=21&gp=0.jpg', 'jpg', 'image/jpeg', '2017-02-15 09:24:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('299', 'admin/image/2PsBcymA.jpg', 'http://static.xuehu365.com/admin/image/2PsBcymA.jpg', 'Fq7J9IfZiDbSkxRmx9__HMKh0ObA', '9978', 'admin/image/', 'u=4198594792,1119433812&fm=21&gp=0.jpg', 'jpg', 'image/jpeg', '2017-02-15 09:24:55', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('303', 'app/file/学乎iOS版上传文件帮助.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助.pdf', 'pdf', 'application/pdf', '2017-02-15 10:52:43', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('304', 'app/file/明天你好.ppt', 'http://static.xuehu365.com/app/file/明天你好.ppt', 'Fsbjg5XLzXIONQ__8RiGlDSFer2v', '742912', null, '明天你好.ppt', 'ppt', 'application/ppt', '2017-02-15 10:56:15', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('315', 'app/file/2PsB1mmW.txt', 'http://static.xuehu365.com/app/file/2PsB1mmW.txt', '1133375872', '2560297', null, 'logs.txt', 'txt', 'application/txt', '2017-02-15 11:52:45', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('316', 'app/file/2PsB1Slh.ppt', 'http://static.xuehu365.com/app/file/2PsB1Slh.ppt', '34921066', '742912', null, '移动应用测试案例.ppt', 'ppt', 'application/ppt', '2017-02-15 11:53:59', '4', '0');
INSERT INTO `t_file_mapping` VALUES ('317', 'app/file/2PsAzDWL.txt', 'http://static.xuehu365.com/app/file/2PsAzDWL.txt', '1128216256', '2560297', null, 'logs.txt', 'txt', 'application/txt', '2017-02-15 12:03:09', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('334', 'app/file/（大于30M）思维导图的三招十八式完全版.pdf', 'http://static.xuehu365.com/app/file/（大于30M）思维导图的三招十八式完全版.pdf', 'lhWAtgjmXNpAAdDnrfBORHm6egg9', '42915146', null, '（大于30M）思维导图的三招十八式完全版.pdf', 'pdf', 'application/pdf', '2017-02-15 14:08:35', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('335', 'app/file/学乎Android版上传视频帮助-1.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-1.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-1.docx', 'docx', 'application/docx', '2017-02-15 14:12:53', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('336', 'app/file/iOS人机界面指南.pdf', 'http://static.xuehu365.com/app/file/iOS人机界面指南.pdf', 'lpyf-u2yYS97VTew-yX1as-KTK0F', '7832649', null, 'iOS人机界面指南.pdf', 'pdf', 'application/pdf', '2017-02-15 14:18:17', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('337', 'app/file/python基础教程至60课(基础)-1.docx', 'http://static.xuehu365.com/app/file/python基础教程至60课(基础)-1.docx', 'Fu-d5LyElSnQY755DwVth71BqcYV', '2474488', null, 'python基础教程至60课(基础)-1.docx', 'docx', 'application/docx', '2017-02-15 14:19:39', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('338', 'app/file/简明_Python_教程.doc', 'http://static.xuehu365.com/app/file/简明_Python_教程.doc', 'FqQpnBbjJ2vBVAuyVx6QjuacxHKL', '389632', null, '简明_Python_教程.doc', 'doc', 'application/doc', '2017-02-15 14:21:14', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('340', 'app/file/(null)', 'http://static.xuehu365.com/app/file/(null)', 'Fsbjg5XLzXIONQ__8RiGlDSFer2v', '742912', null, '1-明天%你好！.ppt', 'ppt', 'application/ppt', '2017-02-15 14:24:09', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('343', 'app/file/Python编程入门.pptx', 'http://static.xuehu365.com/app/file/Python编程入门.pptx', 'FlKvLNalZ8pVZtEoNBsfRsmGfZXy', '1423902', null, 'Python编程入门.pptx', 'pptx', 'application/pptx', '2017-02-15 14:26:40', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('348', 'app/file/专项@app测试.txt', 'http://static.xuehu365.com/app/file/专项@app测试.txt', 'ltA4xlagBbWuX3Z-62ius4uNBFaX', '5406102', null, '专项@app测试.txt', 'txt', 'application/txt', '2017-02-15 14:28:47', '3', '0');
INSERT INTO `t_file_mapping` VALUES ('349', 'app/file/平台迁移回归测试列表.xls', 'http://static.xuehu365.com/app/file/平台迁移回归测试列表.xls', 'FvvDt4mT7i143yba_CIfifDTJFkO', '47616', null, '平台迁移回归测试列表.xls', 'xls', 'application/xls', '2017-02-15 14:30:29', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('350', 'app/file/安装、卸载、升级、兼容适配、前后台切换、交叉通用APP测试用例V0.1.xlsx', 'http://static.xuehu365.com/app/file/安装、卸载、升级、兼容适配、前后台切换、交叉通用APP测试用例V0.1.xlsx', 'FveVwmYDukWFo6CbSeOWgLTeB4ga', '22350', null, '安装、卸载、升级、兼容适配、前后台切换、交叉通用APP测试用例V0.1.xlsx', '1.xlsx', 'application/1.xlsx', '2017-02-15 14:30:42', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('353', 'app/file/2Q7FU7b8-34.pdf', 'http://static.xuehu365.com/app/file/2Q7FU7b8-34.pdf', 'FvI_HIMSy2XYZiFEH8amMIbA0EQ6', '391189', null, '2Q7FU7b8-34.pdf', 'pdf', 'application/pdf', '2017-02-15 14:34:04', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('355', 'app/file/专项@app测试-1.txt', 'http://static.xuehu365.com/app/file/专项@app测试-1.txt', 'ltA4xlagBbWuX3Z-62ius4uNBFaX', '5406102', null, '专项@app测试-1.txt', 'txt', 'application/txt', '2017-02-15 14:38:49', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('368', 'app/file/周会分享-20160722.ppt', 'http://static.xuehu365.com/app/file/周会分享-20160722.ppt', 'FiVg_3vTwURoxMCsK4xQahQ9Qd2J', '1668096', null, '周会分享-20160722.ppt', 'ppt', 'application/ppt', '2017-02-15 15:08:36', '3', '0');
INSERT INTO `t_file_mapping` VALUES ('370', 'app/file/长音频处理目录 6.24更新-1.xlsx', 'http://static.xuehu365.com/app/file/长音频处理目录 6.24更新-1.xlsx', 'FodKQFHys7wQX3uMDakDCeULIYBe', '29255', null, '长音频处理目录 6.24更新-1.xlsx', '24更新-1.xlsx', 'application/24更新-1.xlsx', '2017-02-15 15:09:31', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('371', 'app/file/2PsABq07.pdf', 'http://static.xuehu365.com/app/file/2PsABq07.pdf', '226000891', '11623052', null, 'whitebook.pdf', 'pdf', 'application/pdf', '2017-02-15 15:19:28', '9', '0');
INSERT INTO `t_file_mapping` VALUES ('372', 'app/file/2PsABz1W.pdf', 'http://static.xuehu365.com/app/file/2PsABz1W.pdf', '101282694', '29726804', null, '许爱勤-Python3程序开发指南(第二版)-20160301.pdf', 'pdf', 'application/pdf', '2017-02-15 15:20:07', '5', '0');
INSERT INTO `t_file_mapping` VALUES ('373', 'app/file/2PsA6ucD.pdf', 'http://static.xuehu365.com/app/file/2PsA6ucD.pdf', '12295929', '5849996', null, '虫师_RobotFramewok自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-15 15:38:45', '5', '0');
INSERT INTO `t_file_mapping` VALUES ('374', 'app/file/2PsA4J1C.pdf', 'http://static.xuehu365.com/app/file/2PsA4J1C.pdf', '252506164', '9025284', null, 'Selenium 1.0 初学者指南.pdf', 'pdf', 'application/pdf', '2017-02-15 15:49:40', '3', '0');
INSERT INTO `t_file_mapping` VALUES ('375', 'app/file/2PsA2XGo.pdf', 'http://static.xuehu365.com/app/file/2PsA2XGo.pdf', '237028935', '5849996', null, '虫师_RobotFramewok自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-15 15:56:20', '5', '0');
INSERT INTO `t_file_mapping` VALUES ('376', 'app/file/2PsA2GIU.pdf', 'http://static.xuehu365.com/app/file/2PsA2GIU.pdf', '236009319', '5787859', null, 'selenium2 python自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-15 15:57:15', '3', '0');
INSERT INTO `t_file_mapping` VALUES ('377', 'app/file/2PsA1tYm.pdf', 'http://static.xuehu365.com/app/file/2PsA1tYm.pdf', '206220812', '391338', null, '马云最近的思考.pdf', 'pdf', 'application/pdf', '2017-02-15 15:58:31', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('378', 'app/file/2PsA1oMS.pdf', 'http://static.xuehu365.com/app/file/2PsA1oMS.pdf', '167027017', '9025284', null, 'Selenium 1.0 初学者指南.pdf', 'pdf', 'application/pdf', '2017-02-15 15:59:15', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('455', 'app/file/2Ps9Ui1s.xlsx', 'http://static.xuehu365.com/app/file/2Ps9Ui1s.xlsx', '1184467584', '10270', null, '开发人员信息名单.xlsx', 'xls', 'application/xls', '2017-02-15 18:10:20', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('459', 'admin/image/2Ps5UBrg.jpg', 'http://static.xuehu365.com/admin/image/2Ps5UBrg.jpg', 'Fh_0ffMD6LlUyInXS7zKEb1wrZ_w', '26074', 'admin/image/', '2PwxwHY8.jpg', 'jpg', 'image/jpeg', '2017-02-16 10:37:28', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('460', 'admin/image/2Ps5UAq1.jpg', 'http://static.xuehu365.com/admin/image/2Ps5UAq1.jpg', 'FtDAXg81ft7ZOIcQ9yGd000ZPBRH', '2129', 'admin/image/', '2PwswP9B.jpg', 'jpg', 'image/jpeg', '2017-02-16 10:37:32', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('463', 'admin/audio/2Ps5Tq5n.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Tq5n.mp3', 'FgCnIeTor5iRTCuj7bsNU4W3La2c', '170349', 'admin/audio/', '纵横职场 (1) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '42');
INSERT INTO `t_file_mapping` VALUES ('464', 'admin/audio/2Ps5TpoZ.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5TpoZ.mp3', 'FgCnIeTor5iRTCuj7bsNU4W3La2c', '170349', 'admin/audio/', '纵横职场 (1).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '42');
INSERT INTO `t_file_mapping` VALUES ('465', 'admin/audio/2Ps5Tpkh.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Tpkh.mp3', 'Fvd65--yR2XlWuv5rgOdlIaMoWHo', '31533', 'admin/audio/', '纵横职场 (2) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '7');
INSERT INTO `t_file_mapping` VALUES ('466', 'admin/audio/2Ps5Tphj.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Tphj.mp3', 'Fvd65--yR2XlWuv5rgOdlIaMoWHo', '31533', 'admin/audio/', '纵横职场 (2).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '7');
INSERT INTO `t_file_mapping` VALUES ('467', 'admin/audio/2Ps5TpdS.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5TpdS.mp3', 'FsDIx9XnmWE4TSCSKAPpKVEA73xw', '46797', 'admin/audio/', '纵横职场 (3) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '11');
INSERT INTO `t_file_mapping` VALUES ('468', 'admin/audio/2Ps5TpZa.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5TpZa.mp3', 'FsDIx9XnmWE4TSCSKAPpKVEA73xw', '46797', 'admin/audio/', '纵横职场 (3).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '11');
INSERT INTO `t_file_mapping` VALUES ('469', 'admin/audio/2Ps5TpWR.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5TpWR.mp3', 'Fg_DlFg7Yu_g0_biu_52VYMYT-ng', '240909', 'admin/audio/', '纵横职场 (4) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '60');
INSERT INTO `t_file_mapping` VALUES ('470', 'admin/audio/2Ps5TpRE.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5TpRE.mp3', 'Fg_DlFg7Yu_g0_biu_52VYMYT-ng', '240909', 'admin/audio/', '纵横职场 (4).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '60');
INSERT INTO `t_file_mapping` VALUES ('471', 'admin/audio/2Ps5TpKb.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5TpKb.mp3', 'FiUHdcY2i447fwz7SN-NcuoKFfZ2', '240429', 'admin/audio/', '纵横职场 (5) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '60');
INSERT INTO `t_file_mapping` VALUES ('472', 'admin/audio/2Ps5TpGv.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5TpGv.mp3', 'FiUHdcY2i447fwz7SN-NcuoKFfZ2', '240429', 'admin/audio/', '纵横职场 (5).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '60');
INSERT INTO `t_file_mapping` VALUES ('473', 'admin/audio/2Ps5Tp9z.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Tp9z.mp3', 'Fp1sC1bWSiZROpPflrzzlHLOjudi', '219309', 'admin/audio/', '纵横职场 (6) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '54');
INSERT INTO `t_file_mapping` VALUES ('474', 'admin/audio/2Ps5ToyX.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5ToyX.mp3', 'Fp1sC1bWSiZROpPflrzzlHLOjudi', '219309', 'admin/audio/', '纵横职场 (6).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '54');
INSERT INTO `t_file_mapping` VALUES ('475', 'admin/audio/2Ps5ToqI.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5ToqI.mp3', 'FqWWpl9O7fBCpUamRWvP6r9aLQQU', '168909', 'admin/audio/', '纵横职场 (7) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '42');
INSERT INTO `t_file_mapping` VALUES ('476', 'admin/audio/2Ps5Toll.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Toll.mp3', 'FqWWpl9O7fBCpUamRWvP6r9aLQQU', '168909', 'admin/audio/', '纵横职场 (7).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '42');
INSERT INTO `t_file_mapping` VALUES ('477', 'admin/audio/2Ps5TohT.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5TohT.mp3', 'FmzlWfXRm5pdNT-c7xOKtHo89g6D', '191949', 'admin/audio/', '纵横职场 (8) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '47');
INSERT INTO `t_file_mapping` VALUES ('478', 'admin/audio/2Ps5ToeG.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5ToeG.mp3', 'FmzlWfXRm5pdNT-c7xOKtHo89g6D', '191949', 'admin/audio/', '纵横职场 (8).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '47');
INSERT INTO `t_file_mapping` VALUES ('479', 'admin/audio/2Ps5ToSK.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5ToSK.mp3', 'Fm6lwIByShYQNPxNHebOsbv3AW5H', '211149', 'admin/audio/', '纵横职场 (9) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '52');
INSERT INTO `t_file_mapping` VALUES ('480', 'admin/audio/2Ps5ToM1.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5ToM1.mp3', 'Fm6lwIByShYQNPxNHebOsbv3AW5H', '211149', 'admin/audio/', '纵横职场 (9).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '52');
INSERT INTO `t_file_mapping` VALUES ('481', 'admin/audio/2Ps5ToIV.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5ToIV.mp3', 'FqqmPTBwFOprFuEyl8Sufeo4SeUN', '234669', 'admin/audio/', '纵横职场 (10) - 副本.mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('482', 'admin/audio/2Ps5ToEo.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5ToEo.mp3', 'FqqmPTBwFOprFuEyl8Sufeo4SeUN', '234669', 'admin/audio/', '纵横职场 (10).mp3', 'mp3', 'audio/mp3', '2017-02-16 10:38:52', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('483', 'admin/file/2Ps5OmDp.pdf', 'http://static.xuehu365.com/admin/file/2Ps5OmDp.pdf', 'Flk-5fBJdDJuc0qjXNsroSEaCQIB', '2928491', 'admin/file/', '2PvGR61X.pdf', 'pdf', 'application/pdf', '2017-02-16 10:58:58', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('484', 'admin/file/2Ps5OP15.pdf', 'http://static.xuehu365.com/admin/file/2Ps5OP15.pdf', 'Flk-5fBJdDJuc0qjXNsroSEaCQIB', '2928491', 'admin/file/', '李发海：赋能于人，财道家塾给你的无限可能.pdf', 'pdf', 'application/pdf', '2017-02-16 11:00:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('485', 'admin/file/2Ps5KpiY.pptx', 'http://static.xuehu365.com/admin/file/2Ps5KpiY.pptx', 'lpjFBfTNXSPj4OE9qsOzYOr2UH7S', '10056711', 'admin/file/', '2PvCjfaN(1).pptx', 'pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '2017-02-16 11:14:38', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('486', 'admin/file/2Ps5KZFc.pptx', 'http://static.xuehu365.com/admin/file/2Ps5KZFc.pptx', 'lpjFBfTNXSPj4OE9qsOzYOr2UH7S', '10056711', 'admin/file/', '财道家塾一周年暨2017“为你赋能”规划.pptx', 'pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '2017-02-16 11:15:41', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('487', 'admin/image/2Ps5Gw1U.png', 'http://static.xuehu365.com/admin/image/2Ps5Gw1U.png', 'FqsIlv4JMZKO0k6FCABI48XylU3D', '159874', 'admin/image/', '2Q3slYOV.png', 'png', 'image/png', '2017-02-16 11:30:07', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('488', 'admin/audio/2Ps5Fsma.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fsma.mp3', 'FgGXmf8NENb5DaKqstTtlUTZMOS4', '138189', 'admin/audio/', '1.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '34');
INSERT INTO `t_file_mapping` VALUES ('489', 'admin/audio/2Ps5FsfQ.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FsfQ.mp3', 'Fo6PoF0pIwbxOtFAOJss8W3z4WHQ', '232269', 'admin/audio/', '2.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('490', 'admin/audio/2Ps5Fsax.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fsax.mp3', 'FiCI4axIzJRZssSdA-MvLgY82mED', '230349', 'admin/audio/', '3.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '57');
INSERT INTO `t_file_mapping` VALUES ('491', 'admin/audio/2Ps5FsVf.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FsVf.mp3', 'FmuFyx0-X6X8T2zVR6R6gzXkhlHm', '201261', 'admin/audio/', '4.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '50');
INSERT INTO `t_file_mapping` VALUES ('492', 'admin/audio/2Ps5FsR6.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FsR6.mp3', 'FhMq4k1r4KuFWeiLtfoWKJex9PfT', '238989', 'admin/audio/', '5.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '59');
INSERT INTO `t_file_mapping` VALUES ('493', 'admin/audio/2Ps5FsKn.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FsKn.mp3', 'FuPwU86g6eaOeCsgbBPjATyqm1W8', '238509', 'admin/audio/', '6.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '59');
INSERT INTO `t_file_mapping` VALUES ('494', 'admin/audio/2Ps5FsFb.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FsFb.mp3', 'Ftd6KDaUPPXSY_FvrHdv7lMGrPKE', '219981', 'admin/audio/', '7.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '54');
INSERT INTO `t_file_mapping` VALUES ('495', 'admin/audio/2Ps5FsBw.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FsBw.mp3', 'Ftqtw6_FruThFwtKuHbDKJ93WPbk', '229293', 'admin/audio/', '8.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '57');
INSERT INTO `t_file_mapping` VALUES ('496', 'admin/audio/2Ps5Fs76.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fs76.mp3', 'Fjp3YhdsaClC2tuGVrVlMK6OyAPR', '238029', 'admin/audio/', '9.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '59');
INSERT INTO `t_file_mapping` VALUES ('497', 'admin/audio/2Ps5Fs3O.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fs3O.mp3', 'Fgs0hzkAhCMcwQpnrP6khsZDtHh5', '228045', 'admin/audio/', '10.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '56');
INSERT INTO `t_file_mapping` VALUES ('498', 'admin/audio/2Ps5Fryr.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fryr.mp3', 'FhEdRtFJLen7VWLPn8j4Vi7qYVCy', '236589', 'admin/audio/', '11.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '59');
INSERT INTO `t_file_mapping` VALUES ('499', 'admin/audio/2Ps5Frur.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Frur.mp3', 'FoA408AY5lhKl9_4UCLzlrCZrFvp', '232077', 'admin/audio/', '12.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '57');
INSERT INTO `t_file_mapping` VALUES ('500', 'admin/audio/2Ps5FrqC.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FrqC.mp3', 'FgICf3-d_Ry2ilyCwQhFOPYw0a2q', '222957', 'admin/audio/', '13.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '55');
INSERT INTO `t_file_mapping` VALUES ('501', 'admin/audio/2Ps5Frlv.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Frlv.mp3', 'Fo9GA5RkKZetO8FCHPHvw1h0XqYf', '233709', 'admin/audio/', '14.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('502', 'admin/audio/2Ps5Frgw.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Frgw.mp3', 'Fnjzmi78KkbAkbu493jgSfg3RIqG', '234861', 'admin/audio/', '15.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('503', 'admin/audio/2Ps5FrXS.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FrXS.mp3', 'Ft3AJmHOFXvfcAUhd_WpyZ3taBjO', '236205', 'admin/audio/', '16.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('504', 'admin/audio/2Ps5FrRb.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FrRb.mp3', 'FtkWJuGqwDWqXg5OgYmP7n1VrOxz', '236589', 'admin/audio/', '17.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '59');
INSERT INTO `t_file_mapping` VALUES ('505', 'admin/audio/2Ps5FrMm.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FrMm.mp3', 'Fim4W_HK5-_IUjdIc1lTwSVZbof7', '105069', 'admin/audio/', '18.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '26');
INSERT INTO `t_file_mapping` VALUES ('506', 'admin/audio/2Ps5FrIt.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FrIt.mp3', 'Fn1LUNSTUbG14qPh7dDDwsKWJ9GX', '235725', 'admin/audio/', '19.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('507', 'admin/audio/2Ps5FrDZ.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FrDZ.mp3', 'Fv5lVbozL-TzHrcOHrJbLlOvE07f', '149421', 'admin/audio/', '20.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '37');
INSERT INTO `t_file_mapping` VALUES ('508', 'admin/audio/2Ps5Fr54.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fr54.mp3', 'FiSb4lG1q-mzKrBd_2rQ9WFYUuIq', '185997', 'admin/audio/', '21.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '46');
INSERT INTO `t_file_mapping` VALUES ('509', 'admin/audio/2Ps5Fqzj.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fqzj.mp3', 'FgItunvjuOnJINNXSoGyzlg9QqdO', '141069', 'admin/audio/', '22.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '35');
INSERT INTO `t_file_mapping` VALUES ('510', 'admin/audio/2Ps5FquC.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FquC.mp3', 'FqgMaXObQOIv9D56WRpVtJl3kL5I', '232749', 'admin/audio/', '23.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('511', 'admin/audio/2Ps5FqnW.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FqnW.mp3', 'FivOYS_1B_7FgiwilhTuG_hhJRb1', '233997', 'admin/audio/', '24.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('512', 'admin/audio/2Ps5Fqhy.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fqhy.mp3', 'FvQ1kbUAJ-YXgZrDBiES5dmuLxPC', '172749', 'admin/audio/', '25.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '43');
INSERT INTO `t_file_mapping` VALUES ('513', 'admin/audio/2Ps5Fqck.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5Fqck.mp3', 'FgpxXdBERjAheUWXoQaYOKG0Pptk', '235341', 'admin/audio/', '26.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '58');
INSERT INTO `t_file_mapping` VALUES ('514', 'admin/audio/2Ps5FqWK.mp3', 'http://static.xuehu365.com/admin/audio/2Ps5FqWK.mp3', 'Fq9Bg4i6AVPeJ2YvLk5Z-iEk_6DJ', '227469', 'admin/audio/', '27.mp3', 'mp3', 'audio/mp3', '2017-02-16 11:34:18', '0', '56');
INSERT INTO `t_file_mapping` VALUES ('515', 'admin/image/2Ps5EB9j.jpg', 'http://static.xuehu365.com/admin/image/2Ps5EB9j.jpg', 'Fj2MwC98c4VCVA6ahlSbiwfLZ0bs', '93649', 'admin/image/', '848211578870290068.jpg', 'jpg', 'image/jpeg', '2017-02-16 11:41:04', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('516', 'admin/image/2Ps5CcXg.jpg', 'http://static.xuehu365.com/admin/image/2Ps5CcXg.jpg', 'FtgpPHYhUAzrkfika7f-gShf8AiA', '33639', 'admin/image/', '2Pv3BzLi_194585.jpg', 'jpg', 'image/jpeg', '2017-02-16 11:47:15', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('517', 'admin/file/2Ps59tGT.pdf', 'http://static.xuehu365.com/admin/file/2Ps59tGT.pdf', 'FsGQp032ytTGEGZGTPYpkpUTCgri', '678088', 'admin/file/', '2017年教育信息化工作要点.pdf', 'pdf', 'application/pdf', '2017-02-16 11:58:06', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('518', 'admin/file/2Ps4kPrn.xls', 'http://static.xuehu365.com/admin/file/2Ps4kPrn.xls', 'FuKikvsJzSuY9JsPs0bpA4VmJAdV', '977920', 'admin/file/', '约课-微训课课单（首批）2.14.xls', 'xls', 'application/vnd.ms-excel', '2017-02-16 13:39:17', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('519', 'admin/image/2Ps4jZCC.jpg', 'http://static.xuehu365.com/admin/image/2Ps4jZCC.jpg', 'FmyphrYHXTItHFncW5Yu296jiZ7c', '189143', 'admin/image/', '16120956242907593.jpg', 'jpg', 'image/jpeg', '2017-02-16 13:42:40', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('520', 'admin/image/2Ps4fY0N.jpg', 'http://static.xuehu365.com/admin/image/2Ps4fY0N.jpg', 'Fvi9tnA7TrJlWL1WLBgh6v2ogvqp', '12634', 'admin/image/', 'u=3795989795,1084401595&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-02-16 13:58:38', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('521', 'admin/image/2Ps4dgNc.jpg', 'http://static.xuehu365.com/admin/image/2Ps4dgNc.jpg', 'FlZC9KFSJ6pAeOypI1ED2aKYxMtH', '49406', 'admin/image/', 'timg.jpg', 'jpg', 'image/jpeg', '2017-02-16 14:06:02', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('522', 'app/file/2Ps3hYR9.xlsx', 'http://static.xuehu365.com/app/file/2Ps3hYR9.xlsx', '1141240768', '10270', null, '开发人员信息名单.xlsx', 'xls', 'application/xls', '2017-02-16 17:56:57', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('523', 'app/file/2PrznvVX.ppt', 'http://static.xuehu365.com/app/file/2PrznvVX.ppt', '49663456', '1668096', null, '周会分享-20160722.ppt', 'ppt', 'application/ppt', '2017-02-17 09:57:28', '5', '0');
INSERT INTO `t_file_mapping` VALUES ('524', 'app/file/2Prykdvj.xlsx', 'http://static.xuehu365.com/app/file/2Prykdvj.xlsx', '1139107848', '10270', null, '开发人员信息名单.xlsx', 'xls', 'application/xls', '2017-02-17 14:16:02', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('525', 'app/file/2PryXv1f.pdf', 'http://static.xuehu365.com/app/file/2PryXv1f.pdf', '214180617', '29726804', null, '许爱勤-Python3程序开发指南(第二版)-20160301.pdf', 'pdf', 'application/pdf', '2017-02-17 15:07:56', '13', '0');
INSERT INTO `t_file_mapping` VALUES ('526', 'app/file/2PryU8n7.pdf', 'http://static.xuehu365.com/app/file/2PryU8n7.pdf', '166282956', '11623052', null, 'whitebook.pdf', 'pdf', 'application/pdf', '2017-02-17 15:22:31', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('527', 'app/file/2PryGbAN.pdf', 'http://static.xuehu365.com/app/file/2PryGbAN.pdf', '142183298', '4800560', null, '2015_tester_report.pdf', 'pdf', 'application/pdf', '2017-02-17 16:15:28', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('528', 'app/file/2Pry7r9c.pptx', 'http://static.xuehu365.com/app/file/2Pry7r9c.pptx', '122187109', '2281512', null, '搜狐企业网盘.pptx', 'ppt', 'application/ppt', '2017-02-17 16:51:20', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('529', 'admin/file/2PrxrRrm.ppt', 'http://static.xuehu365.com/admin/file/2PrxrRrm.ppt', 'Fsbjg5XLzXIONQ__8RiGlDSFer2v', '742912', 'admin/file/', '1-明天%你好.ppt', 'ppt', 'application/vnd.ms-powerpoint', '2017-02-17 17:55:16', '8', '0');
INSERT INTO `t_file_mapping` VALUES ('530', 'admin/file/2Prtpp01.pdf', 'http://static.xuehu365.com/admin/file/2Prtpp01.pdf', 'Fu6m31tnNT1DfKvIPXxbDfJr2iTR', '4138088', 'admin/file/', 'iOS人机界面指导手册.pdf', 'pdf', 'application/pdf', '2017-02-18 10:26:49', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('531', 'admin/file/2PrtpagJ.pdf', 'http://static.xuehu365.com/admin/file/2PrtpagJ.pdf', 'lpyf-u2yYS97VTew-yX1as-KTK0F', '7832649', 'admin/file/', 'iOS人机界面指南.pdf', 'pdf', 'application/pdf', '2017-02-18 10:27:44', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('532', 'admin/file/2PrtopVR.pdf', 'http://static.xuehu365.com/admin/file/2PrtopVR.pdf', 'FsKS4u6bmUP-itaTWJNElKFSUn1i', '3218448', 'admin/file/', 'iPhone OS编程指南.pdf', 'pdf', 'application/pdf', '2017-02-18 10:30:45', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('533', 'admin/file/2PrtoKvh.txt', 'http://static.xuehu365.com/admin/file/2PrtoKvh.txt', 'ltA4xlagBbWuX3Z-62ius4uNBFaX', '5406102', 'admin/file/', '专项@app测试.txt', 'txt', 'text/plain', '2017-02-18 10:32:43', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('534', 'app/file/学乎Android版上传视频帮助.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助.docx', 'docx', 'application/docx', '2017-02-18 16:16:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('539', 'app/file/学乎iOS版上传文件帮助-2.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-2.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-2.pdf', 'pdf', 'application/pdf', '2017-02-18 16:19:01', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('540', 'app/file/学乎iOS版上传文件帮助-3.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-3.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-3.pdf', 'pdf', 'application/pdf', '2017-02-18 16:19:15', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('541', 'app/file/2PrguMs2.ppt', 'http://static.xuehu365.com/app/file/2PrguMs2.ppt', '24607433', '742912', null, '移动应用测试案例.ppt', 'ppt', 'application/ppt', '2017-02-20 15:30:19', '4', '0');
INSERT INTO `t_file_mapping` VALUES ('542', 'admin/image/2PrgK3b4.jpg', 'http://static.xuehu365.com/admin/image/2PrgK3b4.jpg', 'Fv5SmLVPfUdOeWyKii-vW2H6cTo3', '101103', 'admin/image/', '20140313101331_137.jpg', 'jpg', 'image/jpeg', '2017-02-20 17:54:31', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('543', 'admin/image/2PrgK1hK.jpg', 'http://static.xuehu365.com/admin/image/2PrgK1hK.jpg', 'Fj5QBNCSwsGo5trCRTt4EDFnkfMO', '30208', 'admin/image/', '20140313101332_83.jpg', 'jpg', 'image/jpeg', '2017-02-20 17:54:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('544', 'admin/image/2PrgJ2ln.jpg', 'http://static.xuehu365.com/admin/image/2PrgJ2ln.jpg', 'FlYE3_grphom4x4QFmyPrT8o14cI', '22349', 'admin/image/', '20140313101332_916.jpg', 'jpg', 'image/jpeg', '2017-02-20 17:58:33', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('547', 'app/file/开发人员信息名单.xlsx', 'http://static.xuehu365.com/app/file/开发人员信息名单.xlsx', 'FqZ5ykY3cp46aLRvDEqT6jz-_T4S', '10270', null, '开发人员信息名单.xlsx', 'xlsx', 'application/xlsx', '2017-02-21 11:10:36', '3', '0');
INSERT INTO `t_file_mapping` VALUES ('548', 'admin/image/2Prc6oW3.jpeg', 'http://static.xuehu365.com/admin/image/2Prc6oW3.jpeg', 'FnuShgHVk6VVF7fz8-xfPb9pZtDw', '30592', 'admin/image/', 'a1ed44af1822467ba122cce26dffaeff20170210152055.jpeg', 'jpeg', 'image/jpeg', '2017-02-21 11:12:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('551', 'app/file/开发人员信息名单-2.xlsx', 'http://static.xuehu365.com/app/file/开发人员信息名单-2.xlsx', 'FqZ5ykY3cp46aLRvDEqT6jz-_T4S', '10270', null, '开发人员信息名单-2.xlsx', 'xlsx', 'application/xlsx', '2017-02-21 14:37:14', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('555', 'app/file/~$工作小汇报.xlsx', 'http://static.xuehu365.com/app/file/~$工作小汇报.xlsx', 'Fkn9WtqPre13_5A5PlHvUfdNo10K', '171', null, '~$工作小汇报.xlsx', 'xlsx', 'application/xlsx', '2017-02-21 14:38:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('560', 'app/file/学乎iOS版上传文件帮助-4.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-4.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-4.pdf', 'pdf', 'application/pdf', '2017-02-21 15:27:44', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('561', 'app/file/学乎iOS版上传文件帮助-5.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-5.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-5.pdf', 'pdf', 'application/pdf', '2017-02-21 15:28:12', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('562', 'app/file/学乎iOS版上传文件帮助-7.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-7.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-7.pdf', 'pdf', 'application/pdf', '2017-02-21 15:28:49', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('563', 'app/file/学乎Android版上传视频帮助-3.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-3.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-3.docx', 'docx', 'application/docx', '2017-02-21 15:29:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('565', 'app/file/学乎Android版上传视频帮助-2.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-2.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-2.docx', 'docx', 'application/docx', '2017-02-21 15:31:22', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('567', 'app/file/学乎Android版上传视频帮助-4.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-4.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-4.docx', 'docx', 'application/docx', '2017-02-21 15:32:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('568', 'app/file/学乎Android版上传视频帮助-5.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-5.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-5.docx', 'docx', 'application/docx', '2017-02-21 15:32:40', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('569', 'app/file/学乎Android版上传视频帮助-6.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-6.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-6.docx', 'docx', 'application/docx', '2017-02-21 15:32:51', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('570', 'app/file/学乎Android版上传视频帮助-7.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-7.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-7.docx', 'docx', 'application/docx', '2017-02-21 15:33:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('571', 'app/file/学乎Android版上传视频帮助-8.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-8.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-8.docx', 'docx', 'application/docx', '2017-02-21 15:33:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('573', 'app/file/学乎iOS版上传文件帮助-1.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-1.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-1.pdf', 'pdf', 'application/pdf', '2017-02-21 15:33:40', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('578', 'app/file/学乎iOS版上传文件帮助-6.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-6.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-6.pdf', 'pdf', 'application/pdf', '2017-02-21 15:37:42', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('580', 'app/file/学乎iOS版上传文件帮助-8.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-8.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-8.pdf', 'pdf', 'application/pdf', '2017-02-21 15:38:21', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('581', 'app/file/学乎iOS版上传文件帮助-9.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-9.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-9.pdf', 'pdf', 'application/pdf', '2017-02-21 15:38:34', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('582', 'app/file/学乎iOS版上传文件帮助-10.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-10.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-10.pdf', 'pdf', 'application/pdf', '2017-02-21 15:38:43', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('583', 'app/file/学乎iOS版上传文件帮助-11.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-11.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-11.pdf', 'pdf', 'application/pdf', '2017-02-21 15:38:54', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('584', 'app/file/学乎iOS版上传文件帮助-12.pdf', 'http://static.xuehu365.com/app/file/学乎iOS版上传文件帮助-12.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助-12.pdf', 'pdf', 'application/pdf', '2017-02-21 15:39:06', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('585', 'app/file/学乎Android版上传视频帮助-9.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-9.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-9.docx', 'docx', 'application/docx', '2017-02-21 15:39:25', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('586', 'app/file/学乎Android版上传视频帮助-10.docx', 'http://static.xuehu365.com/app/file/学乎Android版上传视频帮助-10.docx', 'FtFXX-0ZXoragakBXQBgwpw0gUtq', '13123', null, '学乎Android版上传视频帮助-10.docx', 'docx', 'application/docx', '2017-02-21 15:39:40', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('587', 'app/file/2Prb18OC.xlsx', 'http://static.xuehu365.com/app/file/2Prb18OC.xlsx', '1194888280', '10270', null, '开发人员信息名单.xlsx', 'xls', 'application/xls', '2017-02-21 15:41:09', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('588', 'app/file/2PrazKNn.pdf', 'http://static.xuehu365.com/app/file/2PrazKNn.pdf', '134762490', '3426047', null, 'Selenium_Testing_Tools_Cookbook.pdf', 'pdf', 'application/pdf', '2017-02-21 15:48:17', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('589', 'app/file/2PraywN2.doc', 'http://static.xuehu365.com/app/file/2PraywN2.doc', '137210130', '566784', null, '学乎客户端接口规范（最新）.doc', 'doc', 'application/doc', '2017-02-21 15:50:02', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('590', 'app/file/2PraygDN.docx', 'http://static.xuehu365.com/app/file/2PraygDN.docx', '53150897', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-02-21 15:50:46', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('591', 'app/file/2PraycUz.ppt', 'http://static.xuehu365.com/app/file/2PraycUz.ppt', '72009003', '742912', null, '移动应用测试案例.ppt', 'ppt', 'application/ppt', '2017-02-21 15:51:06', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('592', 'app/file/2PrayYNb.pptx', 'http://static.xuehu365.com/app/file/2PrayYNb.pptx', '186636818', '1423902', null, 'Python编程入门.pptx', 'ppt', 'application/ppt', '2017-02-21 15:51:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('593', 'app/file/2PrayKIM.txt', 'http://static.xuehu365.com/app/file/2PrayKIM.txt', '18500955', '476', null, '其他-0927-bug.txt', 'txt', 'application/txt', '2017-02-21 15:52:10', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('594', 'app/file/2PrayGiM.xls', 'http://static.xuehu365.com/app/file/2PrayGiM.xls', '85180102', '47616', null, '平台迁移回归测试列表.xls', 'xls', 'application/xls', '2017-02-21 15:52:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('595', 'app/file/2PrayD3F.xlsx', 'http://static.xuehu365.com/app/file/2PrayD3F.xlsx', '169449497', '34429', null, 'app-2.0-0921.xlsx', 'xls', 'application/xls', '2017-02-21 15:52:38', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('596', '开发人员信息名单-3.xlsx', 'http://static.xuehu365.com/2PrawsoT_230193.xlsx', 'FqZ5ykY3cp46aLRvDEqT6jz-_T4S', '10270', null, '开发人员信息名单-3.xlsx', 'xlsx', 'application/xlsx', '2017-02-21 15:59:22', '4', '0');
INSERT INTO `t_file_mapping` VALUES ('597', 'app/file/2PrawUez.docx', 'http://static.xuehu365.com/app/file/2PrawUez.docx', '180279260', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-02-21 15:59:27', '4', '0');
INSERT INTO `t_file_mapping` VALUES ('598', 'app/file/2PrawSSC.doc', 'http://static.xuehu365.com/app/file/2PrawSSC.doc', '156302181', '566784', null, '学乎客户端接口规范（最新）.doc', 'doc', 'application/doc', '2017-02-21 15:59:36', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('599', 'app/file/2PrawPfe.ppt', 'http://static.xuehu365.com/app/file/2PrawPfe.ppt', '199850809', '742912', null, '移动应用测试案例.ppt', 'ppt', 'application/ppt', '2017-02-21 15:59:49', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('600', '开发人员信息名单-4.xlsx', 'http://static.xuehu365.com/2PrawQOF_230193.xlsx', 'FqZ5ykY3cp46aLRvDEqT6jz-_T4S', '10270', null, '开发人员信息名单-4.xlsx', 'xlsx', 'application/xlsx', '2017-02-21 15:59:50', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('601', 'app/file/2PrawMg6.pptx', 'http://static.xuehu365.com/app/file/2PrawMg6.pptx', '58407722', '1423902', null, 'Python编程入门.pptx', 'ppt', 'application/ppt', '2017-02-21 15:59:59', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('602', 'app/file/2PrawJwN.xls', 'http://static.xuehu365.com/app/file/2PrawJwN.xls', '171264601', '47616', null, '平台迁移回归测试列表.xls', 'xls', 'application/xls', '2017-02-21 16:00:08', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('603', 'app/file/2PrawHKj.xlsx', 'http://static.xuehu365.com/app/file/2PrawHKj.xlsx', '116929736', '34429', null, 'app-2.0-0921.xlsx', 'xls', 'application/xls', '2017-02-21 16:00:18', '5', '0');
INSERT INTO `t_file_mapping` VALUES ('604', 'app/file/2PrawFah.pdf', 'http://static.xuehu365.com/app/file/2PrawFah.pdf', '62100232', '5787859', null, 'selenium2 python自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-21 16:00:36', '3', '0');
INSERT INTO `t_file_mapping` VALUES ('605', '开发人员信息名单-5.xlsx', 'http://static.xuehu365.com/2Praw9M7_230193.xlsx', 'FqZ5ykY3cp46aLRvDEqT6jz-_T4S', '10270', null, '开发人员信息名单-5.xlsx', 'xlsx', 'application/xlsx', '2017-02-21 16:00:56', '8', '0');
INSERT INTO `t_file_mapping` VALUES ('606', 'app/file/2Praw5a6.txt', 'http://static.xuehu365.com/app/file/2Praw5a6.txt', '157709634', '476', null, '其他-0927-bug.txt', 'txt', 'application/txt', '2017-02-21 16:01:03', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('607', 'app/file/2PranP8g.pdf', 'http://static.xuehu365.com/app/file/2PranP8g.pdf', '44818628', '3426047', null, 'Selenium_Testing_Tools_Cookbook.pdf', 'pdf', 'application/pdf', '2017-02-21 16:35:39', '3', '0');
INSERT INTO `t_file_mapping` VALUES ('608', 'app/file/2PralCeO.pdf', 'http://static.xuehu365.com/app/file/2PralCeO.pdf', '208808213', '5849996', null, '虫师_RobotFramewok自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-21 16:44:29', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('609', 'app/file/2PrajEKY.docx', 'http://static.xuehu365.com/app/file/2PrajEKY.docx', '238757106', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-02-21 16:52:08', '6', '0');
INSERT INTO `t_file_mapping` VALUES ('610', 'app/file/2PrV6nEL.doc', 'http://static.xuehu365.com/app/file/2PrV6nEL.doc', '172053717', '30208', null, '挂网-附件3：关于住院医师规范化培训培训年限的通知.doc', 'doc', 'application/doc', '2017-02-22 15:56:17', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('611', '学乎iOS版上传文件帮助.pdf', 'http://static.xuehu365.com/2PrUc0nk_219347.pdf', 'Fp2eCF-ecesX57Nf3ticI7J0kxvD', '224239', null, '学乎iOS版上传文件帮助.pdf', 'pdf', 'application/pdf', '2017-02-22 17:58:28', '7', '0');
INSERT INTO `t_file_mapping` VALUES ('612', 'admin/video/2PrQick8.mov', 'http://static.xuehu365.com/admin/video/2PrQick8.mov', 'FsM6kMJ4MPb-RZ7yo9zZvogTxZ9u', '738912', 'admin/video/', 'VID_20170218_101354[1].mov', 'mov', 'video/quicktime', '2017-02-23 09:57:18', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('613', 'admin/video/2PrQiXfR.mov', 'http://static.xuehu365.com/admin/video/2PrQiXfR.mov', 'FsM6kMJ4MPb-RZ7yo9zZvogTxZ9u', '738912', 'admin/video/', '1.mov', 'mov', 'video/quicktime', '2017-02-23 09:57:37', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('614', 'admin/video/2PrQiVHd.mov', 'http://static.xuehu365.com/admin/video/2PrQiVHd.mov', 'FmU3bg7r1StZZdGyycQtQ-BRDCpa', '1387466', 'admin/video/', '会更好.mov', 'mov', 'video/quicktime', '2017-02-23 09:57:46', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('615', 'app/file/2PrQeTx4.doc', 'http://static.xuehu365.com/app/file/2PrQeTx4.doc', '75937983', '17408', null, '礼仪指引话术-附件2.doc', 'doc', 'application/doc', '2017-02-23 10:13:52', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('616', '2Q7FU7b8.pdf', 'http://static.xuehu365.com/2PrQcXV3_237955.pdf', 'FvI_HIMSy2XYZiFEH8amMIbA0EQ6', '391189', null, '2Q7FU7b8.pdf', 'pdf', 'application/pdf', '2017-02-23 10:22:25', '2', '0');
INSERT INTO `t_file_mapping` VALUES ('618', 'admin/video/2PrPXHKH.mov', 'http://static.xuehu365.com/admin/video/2PrPXHKH.mov', 'lvrAljxLaA2glBN0c2QqgSSdna8B', '6238141', 'admin/video/', '动画拍拍.mov', 'mov', 'video/quicktime', '2017-02-23 14:48:38', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('619', 'app/file/2PrPRCjb.docx', 'http://static.xuehu365.com/app/file/2PrPRCjb.docx', '122404521', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-02-23 15:12:42', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('620', 'app/file/2PrPRB0B.pdf', 'http://static.xuehu365.com/app/file/2PrPRB0B.pdf', '34352787', '5787859', null, 'selenium2 python自动化测试.pdf', 'pdf', 'application/pdf', '2017-02-23 15:13:02', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('621', '在selenium中使用css选择器进行元素定位（二）.docx', 'http://static.xuehu365.com/2PrPPnIX_219347.docx', 'Fg3AIMlDk11M5qjZlhGPFW66EUci', '16709', null, '在selenium中使用css选择器进行元素定位（二）.docx', 'docx', 'application/docx', '2017-02-23 15:18:21', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('622', 'admin/image/2PqvqKKp.png', 'http://static.xuehu365.com/admin/image/2PqvqKKp.png', 'FtW7jaMByakAgb65IHO7RTIyb1zn', '10459', 'admin/image/', '二维码.png', 'png', 'image/png', '2017-02-28 16:41:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('623', 'admin/image/2PqkQG1f.png', 'http://static.xuehu365.com/admin/image/2PqkQG1f.png', 'Frd4ab70NlNxKUMVYm8dc0Dvbkkv', '20558', 'admin/image/', 'QQ图片20170228101717.png', 'png', 'image/png', '2017-03-02 15:33:41', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('624', 'app/file/2PqeDgcw.docx', 'http://static.xuehu365.com/app/file/2PqeDgcw.docx', '144463844', '103357', null, 'Mycat和Atlas数据架构介绍.docx', 'doc', 'application/doc', '2017-03-03 17:01:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('625', 'app/file/2PqeDeYi.pptx', 'http://static.xuehu365.com/app/file/2PqeDeYi.pptx', '177431379', '1423902', null, 'Python编程入门.pptx', 'ppt', 'application/ppt', '2017-03-03 17:01:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('626', 'app/file/2PqeDbjq.xls', 'http://static.xuehu365.com/app/file/2PqeDbjq.xls', '148113044', '47616', null, '平台迁移回归测试列表.xls', 'xls', 'application/xls', '2017-03-03 17:01:34', '1', '0');
INSERT INTO `t_file_mapping` VALUES ('627', 'app/file/2PqeDZSU.pdf', 'http://static.xuehu365.com/app/file/2PqeDZSU.pdf', '157281517', '3426047', null, 'Selenium_Testing_Tools_Cookbook.pdf', 'pdf', 'application/pdf', '2017-03-03 17:01:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('628', 'app/file/2PqeDUzx.txt', 'http://static.xuehu365.com/app/file/2PqeDUzx.txt', '171925530', '476', null, '其他-0927-bug.txt', 'txt', 'application/txt', '2017-03-03 17:02:00', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('629', 'admin/image/2PqIbNpK.jpeg', 'http://static.xuehu365.com/admin/image/2PqIbNpK.jpeg', 'FhzXPsjOM91FHEO5pswKuDMZjfZD', '31611', 'admin/image/', 'timg.jpeg', 'jpeg', 'image/jpeg', '2017-03-07 09:45:07', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('630', 'admin/image/2PqIb03a.jpg', 'http://static.xuehu365.com/admin/image/2PqIb03a.jpg', 'FuF45kq65MAocGu-XPYyYJ75iyCd', '79162', 'admin/image/', '033.jpg', 'jpg', 'image/jpeg', '2017-03-07 09:46:38', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('631', 'admin/image/2PqIargF.jpeg', 'http://static.xuehu365.com/admin/image/2PqIargF.jpeg', 'FvxysVA9dJta-Z3BlvMyQzVmkVOQ', '46067', 'admin/image/', '20150515015506_5cxFy.thumb.700_0.jpeg', 'jpeg', 'image/jpeg', '2017-03-07 09:47:11', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('632', 'admin/image/2Pq0yoZE.png', 'http://static.xuehu365.com/admin/image/2Pq0yoZE.png', 'Fvmka1EROkXKe7_OtfEzlLx896ON', '24071', 'admin/image/', 'bg_geren.png', 'png', 'image/png', '2017-03-10 10:04:57', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('633', 'admin/image/2Pq0Z1Bq.jpg', 'http://static.xuehu365.com/admin/image/2Pq0Z1Bq.jpg', 'Fie6zPjXJM94inyCuTsjGBEv5b8s', '39931', 'admin/image/', '马云：我与特朗普的会面及最近的思考.jpg', 'jpg', 'image/jpeg', '2017-03-10 11:47:25', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('634', 'admin/image/2PpRedbW.png', 'http://static.xuehu365.com/admin/image/2PpRedbW.png', 'FlkgKKweUglUCAQY3VDOudDJe-C8', '279513', 'admin/image/', 'QQ截图20170316110407.png', 'png', 'image/png', '2017-03-16 11:04:37', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('635', 'admin/image/2PpQcUgL.jpg', 'http://static.xuehu365.com/admin/image/2PpQcUgL.jpg', 'FqQ4kaW_c5uT_hH47R6M75OGTf_h', '50620', 'admin/image/', 'one 拷贝.jpg', 'jpg', 'image/jpeg', '2017-03-16 15:19:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('636', 'admin/image/2Pn4kakg.jpg', 'http://static.xuehu365.com/admin/image/2Pn4kakg.jpg', 'FntHwopCs9hdijMEn8JvyVdnoC9B', '65211', 'admin/image/', 'webwxgetmsgimg (11).jpg', 'jpg', 'image/jpeg', '2017-04-10 14:03:00', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('637', 'admin/image/2PmoESW1.jpg', 'http://static.xuehu365.com/admin/image/2PmoESW1.jpg', 'FtZOuebjHmqu2GU7DM7NiPrnhM70', '44155', 'admin/image/', 'webwxgetmsgimg (1).jpg', 'jpg', 'image/jpeg', '2017-04-13 09:50:59', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('638', 'admin/image/2PmKpGRl.jpg', 'http://static.xuehu365.com/admin/image/2PmKpGRl.jpg', 'FhKSbjSkWs6UAS6bbXMAYox5G4fg', '95652', 'admin/image/', 'u46.jpg', 'jpg', 'image/jpeg', '2017-04-18 10:32:57', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('639', 'admin/image/2PmKn375.jpg', 'http://static.xuehu365.com/admin/image/2PmKn375.jpg', 'FhKSbjSkWs6UAS6bbXMAYox5G4fg', '95652', 'admin/image/', 'u46.jpg', 'jpg', 'image/jpeg', '2017-04-18 10:41:45', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('640', 'admin/image/2PmKirbp.jpg', 'http://static.xuehu365.com/admin/image/2PmKirbp.jpg', 'FhKSbjSkWs6UAS6bbXMAYox5G4fg', '95652', 'admin/image/', 'u46.jpg', 'jpg', 'image/jpeg', '2017-04-18 10:58:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('641', 'admin/image/2PmKiVWd.gif', 'http://static.xuehu365.com/admin/image/2PmKiVWd.gif', 'FkjGVkSSVBjhFgNxF5Y9gb1TTOo2', '2808433', 'admin/image/', '0066cVzBly1fdzfkr9j95g308c069npe.gif', 'gif', 'image/gif', '2017-04-18 10:59:48', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('642', 'admin/image/2PmKfYkK.gif', 'http://static.xuehu365.com/admin/image/2PmKfYkK.gif', 'FkjGVkSSVBjhFgNxF5Y9gb1TTOo2', '2808433', 'admin/image/', '0066cVzBly1fdzfkr9j95g308c069npe.gif', 'gif', 'image/gif', '2017-04-18 11:11:30', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('643', 'admin/image/2PmKfF3Z.jpg', 'http://static.xuehu365.com/admin/image/2PmKfF3Z.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:12:46', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('644', 'admin/image/2PmKesrN.jpg', 'http://static.xuehu365.com/admin/image/2PmKesrN.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:14:11', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('645', 'admin/image/2PmKdeml.jpg', 'http://static.xuehu365.com/admin/image/2PmKdeml.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:19:04', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('646', 'admin/image/2PmKdWhD.jpg', 'http://static.xuehu365.com/admin/image/2PmKdWhD.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:19:35', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('647', 'admin/image/2PmKdJ8W.jpg', 'http://static.xuehu365.com/admin/image/2PmKdJ8W.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:20:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('648', 'admin/image/2PmKcoTe.jpg', 'http://static.xuehu365.com/admin/image/2PmKcoTe.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:22:25', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('649', 'admin/image/2PmKcaHP.jpg', 'http://static.xuehu365.com/admin/image/2PmKcaHP.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:23:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('650', 'admin/image/2PmKc3th.jpg', 'http://static.xuehu365.com/admin/image/2PmKc3th.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:25:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('651', 'admin/image/2PmKbx8u.jpg', 'http://static.xuehu365.com/admin/image/2PmKbx8u.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:25:50', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('652', 'admin/image/2PmKb5fi.jpg', 'http://static.xuehu365.com/admin/image/2PmKb5fi.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:29:15', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('653', 'admin/image/2PmKZCyM.jpg', 'http://static.xuehu365.com/admin/image/2PmKZCyM.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:36:44', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('654', 'admin/image/2PmKYHX5.jpg', 'http://static.xuehu365.com/admin/image/2PmKYHX5.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:40:25', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('655', 'admin/image/2PmKYEpD.jpg', 'http://static.xuehu365.com/admin/image/2PmKYEpD.jpg', 'FvGRqLfF0xhSv9b2O0vWhuIVIx-p', '223723', 'admin/image/', 'timg.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:40:35', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('656', 'admin/image/2PmKXqxh.jpg', 'http://static.xuehu365.com/admin/image/2PmKXqxh.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:42:07', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('657', 'admin/image/2PmKXmlf.jpg', 'http://static.xuehu365.com/admin/image/2PmKXmlf.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:42:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('658', 'admin/image/2PmKXa9a.jpg', 'http://static.xuehu365.com/admin/image/2PmKXa9a.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:43:12', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('659', 'admin/image/2PmKXVcv.jpg', 'http://static.xuehu365.com/admin/image/2PmKXVcv.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:43:29', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('660', 'admin/image/2PmKXPR5.jpg', 'http://static.xuehu365.com/admin/image/2PmKXPR5.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:43:53', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('661', 'admin/image/2PmKWp1t.jpg', 'http://static.xuehu365.com/admin/image/2PmKWp1t.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:46:13', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('662', 'admin/image/2PmKToCu.jpg', 'http://static.xuehu365.com/admin/image/2PmKToCu.jpg', 'FvGRqLfF0xhSv9b2O0vWhuIVIx-p', '223723', 'admin/image/', 'timg.jpg', 'jpg', 'image/jpeg', '2017-04-18 11:58:11', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('663', 'admin/image/2PmK3CbH.jpg', 'http://static.xuehu365.com/admin/image/2PmK3CbH.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 13:43:52', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('664', 'admin/image/2PmK31eE.jpg', 'http://static.xuehu365.com/admin/image/2PmK31eE.jpg', 'FvGRqLfF0xhSv9b2O0vWhuIVIx-p', '223723', 'admin/image/', 'timg.jpg', 'jpg', 'image/jpeg', '2017-04-18 13:44:34', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('665', 'admin/image/2PmK2wKE.jpg', 'http://static.xuehu365.com/admin/image/2PmK2wKE.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 13:44:54', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('666', 'admin/image/2PmK2K4S.jpg', 'http://static.xuehu365.com/admin/image/2PmK2K4S.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 13:47:22', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('667', 'admin/image/2PmK28rc.jpg', 'http://static.xuehu365.com/admin/image/2PmK28rc.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 13:48:05', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('668', 'admin/image/2PmK247d.jpg', 'http://static.xuehu365.com/admin/image/2PmK247d.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 13:48:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('669', 'admin/image/2PmK1Zq4.gif', 'http://static.xuehu365.com/admin/image/2PmK1Zq4.gif', 'FhJiyAHdGxFOqo2wyqUOnEAMmx13', '1845437', 'admin/image/', 'giphy (6).gif', 'gif', 'image/gif', '2017-04-18 13:50:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('670', 'admin/image/2PmJUuuR.jpg', 'http://static.xuehu365.com/admin/image/2PmJUuuR.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 16:00:03', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('671', 'admin/image/2PmJUsSa.jpg', 'http://static.xuehu365.com/admin/image/2PmJUsSa.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 16:00:12', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('672', 'admin/image/2PmJNk0Z.jpg', 'http://static.xuehu365.com/admin/image/2PmJNk0Z.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 16:28:33', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('673', 'admin/image/2PmJNhGV.jpg', 'http://static.xuehu365.com/admin/image/2PmJNhGV.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 16:28:44', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('674', 'admin/image/2PmJM0I4.jpg', 'http://static.xuehu365.com/admin/image/2PmJM0I4.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 16:35:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('675', 'admin/image/2PmJLt2t.gif', 'http://static.xuehu365.com/admin/image/2PmJLt2t.gif', 'FhJiyAHdGxFOqo2wyqUOnEAMmx13', '1845437', 'admin/image/', 'giphy (6).gif', 'gif', 'image/gif', '2017-04-18 16:35:55', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('676', 'admin/image/2PmJKUj8.jpg', 'http://static.xuehu365.com/admin/image/2PmJKUj8.jpg', 'Fv55GBp4efBppNi2-Erwdd-4Or-r', '18995', 'admin/image/', 'u=3813661029,557298991&fm=23&gp=0.jpg', 'jpg', 'image/jpeg', '2017-04-18 16:41:27', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('681', 'admin/image/2PlYQ00G.png', 'http://static.xuehu365.com/admin/image/2PlYQ00G.png', 'FhyBSj-yCAod_ZCGYdG2FKdmDh-I', '37947', 'admin/image/', '1.png', 'png', 'image/png', '2017-04-26 17:14:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('682', 'admin/image/2PlYPpn8.png', 'http://static.xuehu365.com/admin/image/2PlYPpn8.png', 'FuAOLHjldMHJwUqCDQ1ju3AMNQpI', '75576', 'admin/image/', '2.png', 'png', 'image/png', '2017-04-26 17:15:02', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('683', 'admin/image/2PlYPeld.png', 'http://static.xuehu365.com/admin/image/2PlYPeld.png', 'Fpaahb0ituPdkaOHsQIhLpzT2XMH', '50297', 'admin/image/', '3.png', 'png', 'image/png', '2017-04-26 17:15:45', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('684', 'admin/image/2PlYPWzD.png', 'http://static.xuehu365.com/admin/image/2PlYPWzD.png', 'FiIGTPYoVrV9G2hhCdM2zf2c2iQ-', '42212', 'admin/image/', '4.png', 'png', 'image/png', '2017-04-26 17:16:14', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('685', 'admin/image/2PlYPPjt.png', 'http://static.xuehu365.com/admin/image/2PlYPPjt.png', 'FkThDKlQafCYhezeb2ixkxpZQDpl', '46430', 'admin/image/', '5.png', 'png', 'image/png', '2017-04-26 17:16:42', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('686', 'admin/image/2PlYPKAk.png', 'http://static.xuehu365.com/admin/image/2PlYPKAk.png', 'Fs73kcDCwYz5VALkN-D_2XKHiTMi', '42839', 'admin/image/', '6.png', 'png', 'image/png', '2017-04-26 17:17:04', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('687', 'admin/image/2PlUEBeH.jpg', 'http://static.xuehu365.com/admin/image/2PlUEBeH.jpg', 'Fo82IclDqqP95etGI6XsN856kCzx', '21773', 'admin/image/', 'timg.jpg', 'jpg', 'image/jpeg', '2017-04-27 10:26:23', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('688', 'admin/image/2PlUDqEh.jpg', 'http://static.xuehu365.com/admin/image/2PlUDqEh.jpg', 'Fk_IBP_la7PaWImsAak8XbUjXNLV', '23166', 'admin/image/', 'timg (1).jpg', 'jpg', 'image/jpeg', '2017-04-27 10:27:46', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('689', 'admin/image/2PlUDdGo.jpg', 'http://static.xuehu365.com/admin/image/2PlUDdGo.jpg', 'FhfyT2VH7ERJNMoOeNSyR1Ce0zf6', '15510', 'admin/image/', 'timg (2).jpg', 'jpg', 'image/jpeg', '2017-04-27 10:28:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('690', 'admin/image/2PlUDB7Z.jpg', 'http://static.xuehu365.com/admin/image/2PlUDB7Z.jpg', 'FosmfMY9c5RRGFJZ8TA0OKgzXyWh', '23802', 'admin/image/', 'timg (3).jpg', 'jpg', 'image/jpeg', '2017-04-27 10:30:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('691', 'admin/image/2PlUCzwv.jpg', 'http://static.xuehu365.com/admin/image/2PlUCzwv.jpg', 'FihDCcvGZLuU3tma3AR5e7EixQuf', '147665', 'admin/image/', 'timg (4).jpg', 'jpg', 'image/jpeg', '2017-04-27 10:31:07', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('692', 'admin/image/2PlUCbxZ.jpg', 'http://static.xuehu365.com/admin/image/2PlUCbxZ.jpg', 'Fqn8JvXh9knZxAAacpyXYTbQoS8W', '31536', 'admin/image/', 'timg (5).jpg', 'jpg', 'image/jpeg', '2017-04-27 10:32:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('693', 'admin/image/2PlTx0Hs.png', 'http://static.xuehu365.com/admin/image/2PlTx0Hs.png', 'FiX59vJLvdy3a13NmwkdZHuSkA6u', '3051', 'admin/image/', 'index_icon_category_all.png', 'png', 'image/png', '2017-04-27 11:34:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('694', 'admin/image/2PlTx0DJ.png', 'http://static.xuehu365.com/admin/image/2PlTx0DJ.png', 'FtH9GoPxa5NcxCjrHyt7gbSFWR72', '4190', 'admin/image/', 'index_icon_category_cw.png', 'png', 'image/png', '2017-04-27 11:34:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('695', 'admin/image/2PlTx0Cn.png', 'http://static.xuehu365.com/admin/image/2PlTx0Cn.png', 'FleLRnAUKi6a_zD_09gQHcpNmj69', '4661', 'admin/image/', 'index_icon_category_hr.png', 'png', 'image/png', '2017-04-27 11:34:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('696', 'admin/image/2PlTx0CE.png', 'http://static.xuehu365.com/admin/image/2PlTx0CE.png', 'FvvVgHHdw0ddULxEFBvmA_u7nTPo', '4324', 'admin/image/', 'index_icon_category_ldl.png', 'png', 'image/png', '2017-04-27 11:34:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('697', 'admin/image/2PlTx0BT.png', 'http://static.xuehu365.com/admin/image/2PlTx0BT.png', 'FiMHGBJwpUiHlmkH2c-NTLN5l1FK', '3763', 'admin/image/', 'index_icon_category_yx.png', 'png', 'image/png', '2017-04-27 11:34:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('698', 'admin/image/2PlT2lNJ.jpg', 'http://static.xuehu365.com/admin/image/2PlT2lNJ.jpg', 'FtelRtZ0Z7e6l-v4OwACHDDnTKqc', '37654', 'admin/image/', 'banner1.jpg', 'jpg', 'image/jpeg', '2017-04-27 15:18:02', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('699', 'admin/image/2PlT22EF.jpg', 'http://static.xuehu365.com/admin/image/2PlT22EF.jpg', 'FktG_rAgE-reIZLiHJ4AXKTOPzuR', '36628', 'admin/image/', '热点广告-1.jpg', 'jpg', 'image/jpeg', '2017-04-27 15:20:56', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('700', 'admin/image/2PlSh1K2.jpg', 'http://static.xuehu365.com/admin/image/2PlSh1K2.jpg', 'FonQ1bRO6sLvlkdtwJkA3gVPLW-i', '14726', 'admin/image/', '问道-1.jpg', 'jpg', 'image/jpeg', '2017-04-27 16:44:24', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('701', 'admin/image/2PlSggqF.jpg', 'http://static.xuehu365.com/admin/image/2PlSggqF.jpg', 'FhYGxveegT3aXatxboUFKSWQe-7Z', '16699', 'admin/image/', '问道-2.jpg', 'jpg', 'image/jpeg', '2017-04-27 16:45:43', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('702', 'admin/image/2PlSgTiw.jpg', 'http://static.xuehu365.com/admin/image/2PlSgTiw.jpg', 'FoklhYx-WaBLmcdjXEgqUzogyyBq', '19976', 'admin/image/', '问道-3.jpg', 'jpg', 'image/jpeg', '2017-04-27 16:46:33', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('703', 'admin/image/2PlSgFCC.jpg', 'http://static.xuehu365.com/admin/image/2PlSgFCC.jpg', 'Fg3xhQQJ7Xs4JwHf4IPI8_hJNkGQ', '15623', 'admin/image/', '问道-4.jpg', 'jpg', 'image/jpeg', '2017-04-27 16:47:29', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('704', 'admin/image/2PlSg2EX.jpg', 'http://static.xuehu365.com/admin/image/2PlSg2EX.jpg', 'FkPOgd9xO-uftSOK80Unu44Pwq2t', '14408', 'admin/image/', '问道-5.jpg', 'jpg', 'image/jpeg', '2017-04-27 16:48:19', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('705', 'admin/image/2PlSf8bX.jpg', 'http://static.xuehu365.com/admin/image/2PlSf8bX.jpg', 'Fgl_59e5Kg6X8khTT5FnZWU-Xrfj', '24636', 'admin/image/', 'd000baa1cd11728b66bf1c85c8fcc3cec2fd2c82.jpg', 'jpg', 'image/jpeg', '2017-04-27 16:51:53', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('706', 'admin/image/2PlSf6fq.jpg', 'http://static.xuehu365.com/admin/image/2PlSf6fq.jpg', 'FoRXrSFw61RqrOCVpHjO43i2SoOz', '72085', 'admin/image/', '9d82d158ccbf6c81aa626234b93eb13532fa4090.jpg', 'jpg', 'image/jpeg', '2017-04-27 16:52:00', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('707', 'admin/image/2PlSaDKU.jpg', 'http://static.xuehu365.com/admin/image/2PlSaDKU.jpg', 'Fgl_59e5Kg6X8khTT5FnZWU-Xrfj', '24636', 'admin/image/', 'd000baa1cd11728b66bf1c85c8fcc3cec2fd2c82.jpg', 'jpg', 'image/jpeg', '2017-04-27 17:11:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('708', 'admin/image/2PlSaAAC.jpg', 'http://static.xuehu365.com/admin/image/2PlSaAAC.jpg', 'FozvYilznopc8K9ps4F17WzKGSar', '304098', 'admin/image/', '0bd162d9f2d3572c577977a68913632762d0c335.jpg', 'jpg', 'image/jpeg', '2017-04-27 17:11:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('709', 'admin/image/2PlSNkyA.jpg', 'http://static.xuehu365.com/admin/image/2PlSNkyA.jpg', 'Fj16f07kK5r_SgQ6WnSm1nwl1GCZ', '17501', 'admin/image/', '混轻-1.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:00:55', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('710', 'admin/image/2PlSJ1nq.jpg', 'http://static.xuehu365.com/admin/image/2PlSJ1nq.jpg', 'Fhc3AtHqhi-_qKuhPtRJuYQDw5B6', '16778', 'admin/image/', '混轻-2.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:19:42', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('711', 'admin/image/2PlSInIo.jpg', 'http://static.xuehu365.com/admin/image/2PlSInIo.jpg', 'FkCy86gHY02bK440UejZojhm84eg', '12362', 'admin/image/', '混轻-3.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:20:38', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('712', 'admin/image/2PlSIOt2.jpg', 'http://static.xuehu365.com/admin/image/2PlSIOt2.jpg', 'FnB5mVfE8J8vwvHIm2oir4pUyfIs', '14799', 'admin/image/', '混轻-4.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:22:12', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('713', 'admin/image/2PlSHTQo.jpg', 'http://static.xuehu365.com/admin/image/2PlSHTQo.jpg', 'FmvJu2tVuI6SaddErMr2aswiwoiH', '10294', 'admin/image/', '微训-1.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:25:53', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('714', 'admin/image/2PlSHPVV.jpg', 'http://static.xuehu365.com/admin/image/2PlSHPVV.jpg', 'Fp9Cmcq2oeD2d2Phj0QVE8Uwvtxc', '12406', 'admin/image/', '微训-2.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:26:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('715', 'admin/image/2PlSHFGW.jpg', 'http://static.xuehu365.com/admin/image/2PlSHFGW.jpg', 'FhwGjTfMQi2p2eIwutJpwCOTNjjI', '16658', 'admin/image/', '解决方案-1.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:26:47', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('716', 'admin/image/2PlSHAKc.jpg', 'http://static.xuehu365.com/admin/image/2PlSHAKc.jpg', 'Fn93Ymux2s4OfJfdLfIHbypm6J_w', '17393', 'admin/image/', '解决方案-2.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:27:06', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('717', 'admin/image/2PlSH5Cm.jpg', 'http://static.xuehu365.com/admin/image/2PlSH5Cm.jpg', 'FjfiXvAUG7Fw21pb-5RaZIVTnKDm', '17497', 'admin/image/', '解决方案-3.jpg', 'jpg', 'image/jpeg', '2017-04-27 18:27:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('718', 'admin/image/2PlOVX0Y.jpg', 'http://static.xuehu365.com/admin/image/2PlOVX0Y.jpg', 'FhP-i5sg-93a7iOuevrJCy4ZP4rm', '13637', 'admin/image/', '活动-1.jpg', 'jpg', 'image/jpeg', '2017-04-28 09:55:08', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('719', 'admin/image/2PlOVSIo.jpg', 'http://static.xuehu365.com/admin/image/2PlOVSIo.jpg', 'Fke_VNSAIvNFso49e0LPfPr5kTMW', '11374', 'admin/image/', '活动-2.jpg', 'jpg', 'image/jpeg', '2017-04-28 09:55:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('720', 'admin/image/2PlOKflA.jpg', 'http://static.xuehu365.com/admin/image/2PlOKflA.jpg', 'FtT08HUk6BWdVHIXPufcw9do9Z5U', '19965', 'admin/image/', '1.jpg', 'jpg', 'image/jpeg', '2017-04-28 10:38:16', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('721', 'admin/image/2PlOKL24.jpg', 'http://static.xuehu365.com/admin/image/2PlOKL24.jpg', 'FvyTMByHUfbDTZxPHbvenoLsAwnm', '17892', 'admin/image/', '2.jpg', 'jpg', 'image/jpeg', '2017-04-28 10:39:35', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('722', 'admin/image/2PlOKCn6.jpg', 'http://static.xuehu365.com/admin/image/2PlOKCn6.jpg', 'FtQPg-UjBwHAnpJ9m8gojYJRYBWb', '16332', 'admin/image/', '3.jpg', 'jpg', 'image/jpeg', '2017-04-28 10:40:07', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('723', 'admin/image/2PlNXenO.jpg', 'http://static.xuehu365.com/admin/image/2PlNXenO.jpg', 'FtkcQEUh3_9yPLdR05r0mXXgijf3', '22156', 'admin/image/', '16101352449034943.jpg', 'jpg', 'image/jpeg', '2017-04-28 13:52:58', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('724', 'admin/image/2PlNWYPc.jpg', 'http://static.xuehu365.com/admin/image/2PlNWYPc.jpg', 'FmpQQINdqHaDJSXcWp5ds-Nu6zBy', '39103', 'admin/image/', '1493358909(1).jpg', 'jpg', 'image/jpeg', '2017-04-28 13:57:20', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('725', 'admin/image/2PlNVc3F.jpg', 'http://static.xuehu365.com/admin/image/2PlNVc3F.jpg', 'FhrF7aH3o7Z9k5LlxFkTaDAL9BJH', '36198', 'admin/image/', '1493359153(1).jpg', 'jpg', 'image/jpeg', '2017-04-28 14:01:05', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('726', 'admin/image/2PlNUzcy.jpg', 'http://static.xuehu365.com/admin/image/2PlNUzcy.jpg', 'FncDuhCJHiaV8ya0EvnMfiILqQmT', '9302', 'admin/image/', '微训-3.jpg', 'jpg', 'image/jpeg', '2017-04-28 14:03:32', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('727', 'admin/image/2PlNURqI.jpg', 'http://static.xuehu365.com/admin/image/2PlNURqI.jpg', 'FmEGb9K4kmAgyjT91VLyLAuNEuA6', '50556', 'admin/image/', '使用指南.jpg', 'jpg', 'image/jpeg', '2017-04-28 14:05:42', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('728', 'admin/image/2PlNUJWa.jpg', 'http://static.xuehu365.com/admin/image/2PlNUJWa.jpg', 'Fr_RtV-B6euN4tSk9esdBjbLjO8U', '46992', 'admin/image/', '线下9大会场精彩分享-首页banner.jpg', 'jpg', 'image/jpeg', '2017-04-28 14:06:14', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('729', 'admin/image/2PlNUHfx.jpg', 'http://static.xuehu365.com/admin/image/2PlNUHfx.jpg', 'FvXqTkWAH3C6tPhSAJDR5f2Q6Iag', '96866', 'admin/image/', 'APP广告图.jpg', 'jpg', 'image/jpeg', '2017-04-28 14:06:21', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('730', 'admin/image/2Pl1DLEL.png', 'http://static.xuehu365.com/admin/image/2Pl1DLEL.png', 'FjocPsD-4Z37tk5jDtuH43iH3kTR', '3376', 'admin/image/', 'index_logo_caidao.png', 'png', 'image/png', '2017-05-02 09:31:39', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('731', 'admin/image/2Pl18ZG0.png', 'http://static.xuehu365.com/admin/image/2Pl18ZG0.png', 'FjocPsD-4Z37tk5jDtuH43iH3kTR', '3376', 'admin/image/', 'index_logo_caidao.png', 'png', 'image/png', '2017-05-02 09:50:36', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('732', 'admin/image/2Pl18RYC.png', 'http://static.xuehu365.com/admin/image/2Pl18RYC.png', 'Flkld0gUaY6wEK64_0DsIrpD7BxD', '3367', 'admin/image/', 'index_logo_xuedao.png', 'png', 'image/png', '2017-05-02 09:51:06', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('733', 'admin/image/2PkS1a8s.jpg', 'http://static.xuehu365.com/admin/image/2PkS1a8s.jpg', 'FgzGDXFqsJryldzVZWE3iGcY5eVj', '185046', 'admin/image/', 'QQ图片20170424093920.jpg', 'jpg', 'image/jpeg', '2017-05-08 09:57:53', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('734', 'admin/image/2PkQk8tQ.jpg', 'http://static.xuehu365.com/admin/image/2PkQk8tQ.jpg', 'Fqu7Vs_E-U31TxBogjFvwMEzuw9t', '31371', 'admin/image/', 'QQ图片20170508151150.jpg', 'jpg', 'image/jpeg', '2017-05-08 15:13:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('735', 'admin/image/2PkQk8qt.jpg', 'http://static.xuehu365.com/admin/image/2PkQk8qt.jpg', 'FrplVGujztm63E023ZWLy2jfAaaS', '101822', 'admin/image/', 'QQ图片20170508151216.jpg', 'jpg', 'image/jpeg', '2017-05-08 15:13:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('736', 'admin/image/2PkQk8q4.jpg', 'http://static.xuehu365.com/admin/image/2PkQk8q4.jpg', 'Fh9SMAED05g2bVXQ0jAeWTyFUADz', '25346', 'admin/image/', 'QQ图片20170508151226.jpg', 'jpg', 'image/jpeg', '2017-05-08 15:13:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('737', 'admin/image/2PkQk8pc.jpg', 'http://static.xuehu365.com/admin/image/2PkQk8pc.jpg', 'FkBxrGszNkdgov7lbodiC4bRwHW7', '12118', 'admin/image/', 'QQ图片20170508151230.jpg', 'jpg', 'image/jpeg', '2017-05-08 15:13:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('738', 'admin/image/2PkQk8p3.jpg', 'http://static.xuehu365.com/admin/image/2PkQk8p3.jpg', 'FvFZdaFCwVN1uSm9xD9XC-RuP_pK', '55202', 'admin/image/', 'QQ图片20170508151235.jpg', 'jpg', 'image/jpeg', '2017-05-08 15:13:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('739', 'admin/image/2PkQk8oZ.jpg', 'http://static.xuehu365.com/admin/image/2PkQk8oZ.jpg', 'Fs-aZRGkXH4yVIBtztE6G1jIme3Y', '40098', 'admin/image/', 'QQ图片20170508151238.jpg', 'jpg', 'image/jpeg', '2017-05-08 15:13:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('740', 'admin/image/2PkQk8mA.jpg', 'http://static.xuehu365.com/admin/image/2PkQk8mA.jpg', 'Fve-SHLio2OJYaTWl4KlWOlAiX4e', '163373', 'admin/image/', 'QQ图片20170508151243.jpg', 'jpg', 'image/jpeg', '2017-05-08 15:13:26', '0', '0');
INSERT INTO `t_file_mapping` VALUES ('741', 'admin/image/2PjhO3QI.svg', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', 'FmUhtFwxPvR0cLMweL0LrftTi09L', '414', 'admin/image/', 'default.svg', 'svg', 'image/svg+xml', '2017-05-16 09:23:25', '0', '0');

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL COMMENT '名称',
  `name` varchar(64) NOT NULL COMMENT '标题',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `parent` int(11) DEFAULT '0' COMMENT '父权限ID',
  `ancestors` varchar(255) DEFAULT NULL COMMENT '祖先ID路径',
  `level` tinyint(4) DEFAULT NULL COMMENT '层级',
  `priority` tinyint(5) DEFAULT '0' COMMENT '优先级',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='菜单/权限';

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES ('1', 'management.view', '系统管理', '', 'fa fa-gears', null, '1', '0', '-1', '1');
INSERT INTO `t_permission` VALUES ('2', 'permission.view', '菜单/权限', '', 'fa fa-list', '1', '1,2', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('3', 'user.view', '用户管理', '', 'fa fa-users', '1', '1,3', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('4', 'role.view', '角色管理', '', 'fa fa-wheelchair', '1', '1,4', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('5', 'department.view', '部门管理', '', 'fa fa-building-o', '1', '1,5', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('6', 'dict.view', '数据字典', '存储常用、公共数据', 'fa fa-book', '1', '1,6', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('10', 'permission.edit', '编辑', '编辑菜单、权限', null, '2', '1,2,27', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('11', 'permission.delete', '删除', '删除菜单、权限', null, '2', '1,2,27', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('12', 'user.edit', '编辑', '编辑用户信息', null, '3', '1,3,30', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('13', 'user.delete', '删除', '删除用户信息', null, '3', '1,3,31', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('14', 'role.edit', '编辑', '编辑角色信息', null, '4', '1,4,14', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('15', 'role.delete', '删除', '删除角色信息', null, '4', '1,4,15', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('16', 'department.edit', '编辑', '新增部门信息', null, '5', '1,5,37', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('17', 'department.delete', '删除', '删除部门信息', null, '5', '1,5,38', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('18', 'dict.edit', '编辑', '编辑字典信息', null, '6', '1,6,41', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('19', 'dict.delete', '删除', '删除字典信息', null, '6', '1,6,42', '3', '0', '1');
INSERT INTO `t_permission` VALUES ('29', 'xuehuapp.view', '学乎APP', '', 'fa fa-tablet', null, '29', '0', '1', '1');
INSERT INTO `t_permission` VALUES ('30', 'courseclassify.view', '课程分类', '', 'fa fa-sitemap', '29', '44,46', '1', '3', '1');
INSERT INTO `t_permission` VALUES ('31', 'indexclassifyconfig.view', '首页分类', '', 'fa fa-indent', '29', '29,31', '1', '4', '1');
INSERT INTO `t_permission` VALUES ('32', 'version.view', 'APP版本', '', 'fa fa-code-fork', '29', '44,48', '1', '2', '1');
INSERT INTO `t_permission` VALUES ('33', 'indexcourseconfig.view', '课程配置', '配置APP首页要推送的课程', 'fa fa-book', '29', '44,49', '1', '5', '1');
INSERT INTO `t_permission` VALUES ('34', 'channel.view', '渠道管理', '安卓发布渠道管理', 'fa fa-android', '29', '44,50', '1', '1', '1');
INSERT INTO `t_permission` VALUES ('35', 'xhcommunity.view', '学乎社群', '', 'fa fa-object-group', null, '35', '0', '0', '1');
INSERT INTO `t_permission` VALUES ('36', 'roomtopic.view', '直播主题', '', 'fa fa-video-camera', '35', '35,36', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('37', 'community.view0', '直播室', '', 'fa fa-tv', '35', '35,37', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('38', 'indexcourseconfig.edit', '编辑', '', null, '33', '44,49,38', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('39', 'indexcourseconfig.delete', '删除', '', null, '33', '44,49,59', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('40', 'indexclassifyconfig.edit', '编辑', '', null, '31', '44,47,61', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('41', 'indexclassifyconfig.delete', '删除', '', null, '31', '44,47,62', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('42', 'version.edit', '编辑', '', null, '32', '44,48,67', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('43', 'version.delete', '删除', '', null, '32', '44,48,68', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('44', 'channel.edit', '编辑', '', null, '34', '44,50,70', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('45', 'channel.delete', '删除', '', null, '34', '44,50,71', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('46', 'roomroom.edit', '编辑', '', null, '37', '54,56,73', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('47', 'roomroom.delete', '删除', '', null, '37', '54,56,74', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('48', 'roomtopic.edit', '编辑', '', null, '36', '54,55,76', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('49', 'roomtopic.delete', '删除', '', null, '36', '54,55,77', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('55', 'courseclassify.edit', '编辑', '', null, '30', '44,46,64', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('66', 'courseclassify.delete', '删除', '', null, '30', '44,46,65', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('70', 'operating.view', '学乎运营', '', 'fa fa-rss', null, '70', '0', '0', '1');
INSERT INTO `t_permission` VALUES ('71', 'article.view', '运营文章', '', 'fa fa-commenting-o', '70', '70,71', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('72', 'article.edit', '编辑', '', '', '71', '70,71,72', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('73', 'article.delete', '删除', '', '', '71', '70,71,73', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('74', 'weixin.editmenu', '公众号菜单', '', 'fa fa-list-ol', '70', '70,74', '1', '-1', '1');
INSERT INTO `t_permission` VALUES ('75', 'news.view', '消息管理', '', 'fa fa-newspaper-o', '70', '70,75', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('76', 'newstype.view', '消息类型', '', 'fa fa-object-ungroup', '70', '70,76', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('77', 'news.edit', '编辑', '', '', '75', '70,75,77', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('78', 'news.delete', '删除', '', '', '75', '70,75,78', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('79', 'newstype.delete', '删除', '', '', '76', '70,76,79', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('80', 'newstype.edit', '编辑', '', '', '76', '70,76,80', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('82', 'community.view', '社群', '', 'fa fa-object-ungroup', '35', '35,82', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('83', 'community.edit', '编辑', '', '', '82', '35,82,83', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('84', 'community.delete', '删除', '', '', '82', '35,82,84', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('85', 'roomtopic.intern', '直播主题 (实习人员)', '', 'fa fa-video-camera', '35', '35,85', '1', '1', '1');
INSERT INTO `t_permission` VALUES ('103', 'theme.view', '话题', '话题', 'fa fa-asl-interpreting', '70', '70,103', '1', '0', '1');
INSERT INTO `t_permission` VALUES ('104', 'theme.edit', '编辑', '', 'fa fa-asterisk', '103', '70,103,104', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('105', 'theme.delete', '删除', '', '', '103', '70,103,105', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('106', 'indexconfig.view', '首页配置', '', 'fa fa-universal-access', '29', '29,106', '1', '6', '1');
INSERT INTO `t_permission` VALUES ('107', 'indexconfig.edit', '编辑', '', '', '106', '29,106,107', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('108', 'indexconfig.delete', '删除', '', '', '106', '29,106,108', '2', '0', '1');
INSERT INTO `t_permission` VALUES ('109', 'counsel.view', '咨询反馈', '', 'fa fa-vine', '70', '70,109', '1', '0', '1');

-- ----------------------------
-- Table structure for t_reset_record
-- ----------------------------
DROP TABLE IF EXISTS `t_reset_record`;
CREATE TABLE `t_reset_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `username` varchar(32) DEFAULT NULL COMMENT '用户名',
  `reset_key` varchar(32) DEFAULT NULL COMMENT '重置密钥',
  `valid` int(1) DEFAULT '1' COMMENT '是否有效, 1-是 0-否',
  `request_time` datetime DEFAULT NULL COMMENT '申请时间',
  `expiration_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_reset_record
-- ----------------------------
INSERT INTO `t_reset_record` VALUES ('71', 'hunan_he@163.com', null, 'ed9800ccb9829046c2a54ff8ecb6a6ac', '0', '2016-02-27 21:53:40', '2016-02-28 21:53:40');
INSERT INTO `t_reset_record` VALUES ('72', 'CLIPTHEME@CLIPTHEME.cn', null, 'bb08a3222452742c54029acf6d2ae0d1', '1', '2016-02-27 22:02:37', '2016-02-28 22:02:37');
INSERT INTO `t_reset_record` VALUES ('73', 'CLIPTHEME@CLIPTHEME.cn', null, '123c1a4480b4a3c273de247b76e4dc81', '1', '2016-02-27 22:04:53', '2016-02-28 22:04:53');
INSERT INTO `t_reset_record` VALUES ('74', 'CLIPTHEME@CLIPTHEME.cn', null, 'a9cb367b665412f930fc6d3ae6b4a942', '1', '2016-02-27 22:05:10', '2016-02-28 22:05:10');
INSERT INTO `t_reset_record` VALUES ('75', 'CLIPTHEME@CLIPTHEME.cn', null, 'effa315f284c9826b56158deccdc2fcc', '1', '2016-02-27 22:06:09', '2016-02-28 22:06:09');
INSERT INTO `t_reset_record` VALUES ('76', 'CLIPTHEME@CLIPTHEME.cn', null, 'ac1c1940489fccde8576ff8c522b3881', '1', '2016-02-27 22:06:37', '2016-02-28 22:06:37');
INSERT INTO `t_reset_record` VALUES ('77', 'CLIPTHEME@CLIPTHEME.cn', null, '634f3d294ab80cb699524d8ebb00a020', '1', '2016-02-27 22:06:53', '2016-02-28 22:06:53');
INSERT INTO `t_reset_record` VALUES ('78', 'CLIPTHEME@CLIPTHEME.cn', null, '9d976a269a031876dc65cf5ab032c05e', '1', '2016-02-27 22:08:39', '2016-02-28 22:08:39');
INSERT INTO `t_reset_record` VALUES ('79', 'CLIPTHEME@CLIPTHEME.cn', null, '8cbd6ce0dadbd047168560a9dbbd9aec', '1', '2016-02-28 17:16:17', '2016-02-29 17:16:17');
INSERT INTO `t_reset_record` VALUES ('80', 'CLIPTHEME1@123', null, '6ba9ecd9dcfa17f2db570deddc53a1be', '1', '2016-02-28 17:16:42', '2016-02-29 17:16:42');
INSERT INTO `t_reset_record` VALUES ('81', 'CLIPTHEME1@123', null, '126edf303b6b4f78a906292014209087', '1', '2016-02-28 17:17:59', '2016-02-29 17:17:59');
INSERT INTO `t_reset_record` VALUES ('82', 'CLIPTHEME1@123', null, 'd4dd50e1ebe9b573f428fea8b9c9a109', '1', '2016-02-28 17:18:15', '2016-02-29 17:18:15');
INSERT INTO `t_reset_record` VALUES ('83', 'CLIPTHEME1@123', null, '3187a5d9d04bd29ea35c8b5a518ed73a', '1', '2016-02-28 17:18:50', '2016-02-29 17:18:50');
INSERT INTO `t_reset_record` VALUES ('84', 'CLIPTHEME1@123', null, '0ab2f1eee170c7563bd46b5bae5ad5f1', '1', '2016-02-28 17:19:38', '2016-02-29 17:19:38');
INSERT INTO `t_reset_record` VALUES ('85', 'CLIPTHEME1@123', null, 'be397305c64f6005fa9848da7c013634', '1', '2016-02-28 17:21:34', '2016-02-29 17:21:34');
INSERT INTO `t_reset_record` VALUES ('86', 'hunan_he@163.com', null, '01e30568f7e2b8d439dca7997836ac91', '0', '2016-02-28 17:25:57', '2016-02-29 17:25:57');
INSERT INTO `t_reset_record` VALUES ('87', 'hunan_he@163.com', null, 'fe27f8f1422172b5841a87c1841f4bbf', '0', '2016-02-28 17:26:17', '2016-02-29 17:26:17');
INSERT INTO `t_reset_record` VALUES ('88', 'hunan_he@163.com', null, '5346a559083bc0f9957f22c7cb92ce9a', '0', '2016-02-28 17:28:43', '2016-02-29 17:28:43');
INSERT INTO `t_reset_record` VALUES ('89', 'hunan_he@163.com', null, 'f09254d8e2214920c8e34ca8015666b3', '0', '2016-02-28 17:29:07', '2016-02-29 17:29:07');
INSERT INTO `t_reset_record` VALUES ('90', 'hunan_he@163.com', null, '7b395c04278d7e1dcaabaa577fa2d21a', '0', '2016-02-28 17:30:18', '2016-02-29 17:30:18');
INSERT INTO `t_reset_record` VALUES ('91', 'hunan_he@163.com', null, '5207ee766445dd28028d08bb193e6109', '0', '2016-02-28 17:41:04', '2016-02-29 17:41:04');
INSERT INTO `t_reset_record` VALUES ('92', 'hunan.me@gmail.com', null, 'c09a9df69b0aef479d9e6a323f2e18ab', '1', '2016-03-02 20:06:12', '2016-03-03 20:06:12');
INSERT INTO `t_reset_record` VALUES ('93', 'hunan.me@gmail.com', null, '2c48a1e60bc069509ac7b2786820c5ac', '1', '2016-03-02 20:06:26', '2016-03-03 20:06:26');
INSERT INTO `t_reset_record` VALUES ('94', 'hunan.me@gmail.com', null, '7c5ef7c990dfaf933f7a793378d9760a', '1', '2016-03-02 20:06:48', '2016-03-03 20:06:48');
INSERT INTO `t_reset_record` VALUES ('95', 'hunan.me@gmail.com', null, '8b2a04fd24c465bbcc103a0f17dc7e46', '1', '2016-03-02 20:07:17', '2016-03-03 20:07:17');
INSERT INTO `t_reset_record` VALUES ('96', 'hunan.me@gmail.com', null, 'b214a474638af38640c0c84c32853291', '1', '2016-05-27 11:01:21', '2016-05-28 11:01:21');
INSERT INTO `t_reset_record` VALUES ('97', 'hunan.me@gmail.com', null, 'a80ef0bb64cdb88e94ce6488769f6d9e', '1', '2016-05-27 11:01:51', '2016-05-28 11:01:51');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL COMMENT '角色名',
  `description` varchar(128) DEFAULT NULL COMMENT '备注',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_role_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('-1', '系统管理员', '默认拥有所有权限', '1');
INSERT INTO `t_role` VALUES ('0', '普通用户', '系统默认角色', '1');
INSERT INTO `t_role` VALUES ('1', '客服', null, '1');
INSERT INTO `t_role` VALUES ('2', '课程运营', null, '1');
INSERT INTO `t_role` VALUES ('11', '学习顾问', null, '1');
INSERT INTO `t_role` VALUES ('84', '哇哈哈', '哇另一个哈', '1');

-- ----------------------------
-- Table structure for t_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permission`;
CREATE TABLE `t_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `perm_id` int(11) NOT NULL,
  `perm` varchar(32) NOT NULL COMMENT '权限标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_permission
-- ----------------------------
INSERT INTO `t_role_permission` VALUES ('379', '0', '106', 'indexconfig.view');
INSERT INTO `t_role_permission` VALUES ('380', '0', '33', 'indexcourseconfig.view');
INSERT INTO `t_role_permission` VALUES ('381', '0', '31', 'indexclassifyconfig.view');
INSERT INTO `t_role_permission` VALUES ('382', '0', '30', 'courseclassify.view');
INSERT INTO `t_role_permission` VALUES ('383', '0', '32', 'version.view');
INSERT INTO `t_role_permission` VALUES ('384', '0', '29', 'xuehuapp.view');
INSERT INTO `t_role_permission` VALUES ('385', '0', '34', 'channel.view');
INSERT INTO `t_role_permission` VALUES ('386', '0', '2', 'permission.view');
INSERT INTO `t_role_permission` VALUES ('387', '0', '3', 'user.view');
INSERT INTO `t_role_permission` VALUES ('388', '0', '4', 'role.view');
INSERT INTO `t_role_permission` VALUES ('389', '0', '5', 'department.view');
INSERT INTO `t_role_permission` VALUES ('390', '0', '6', 'dict.view');
INSERT INTO `t_role_permission` VALUES ('391', '0', '10', 'permission.edit');
INSERT INTO `t_role_permission` VALUES ('392', '0', '11', 'permission.delete');
INSERT INTO `t_role_permission` VALUES ('393', '0', '12', 'user.edit');
INSERT INTO `t_role_permission` VALUES ('394', '0', '13', 'user.delete');
INSERT INTO `t_role_permission` VALUES ('395', '0', '14', 'role.edit');
INSERT INTO `t_role_permission` VALUES ('396', '0', '15', 'role.delete');
INSERT INTO `t_role_permission` VALUES ('397', '0', '16', 'department.edit');
INSERT INTO `t_role_permission` VALUES ('398', '0', '17', 'department.delete');
INSERT INTO `t_role_permission` VALUES ('399', '0', '18', 'dict.edit');
INSERT INTO `t_role_permission` VALUES ('400', '0', '19', 'dict.delete');
INSERT INTO `t_role_permission` VALUES ('401', '0', '38', 'indexcourseconfig.edit');
INSERT INTO `t_role_permission` VALUES ('402', '0', '39', 'indexcourseconfig.delete');
INSERT INTO `t_role_permission` VALUES ('403', '0', '40', 'indexclassifyconfig.edit');
INSERT INTO `t_role_permission` VALUES ('404', '0', '41', 'indexclassifyconfig.delete');
INSERT INTO `t_role_permission` VALUES ('405', '0', '42', 'version.edit');
INSERT INTO `t_role_permission` VALUES ('406', '0', '43', 'version.delete');
INSERT INTO `t_role_permission` VALUES ('407', '0', '44', 'channel.edit');
INSERT INTO `t_role_permission` VALUES ('408', '0', '45', 'channel.delete');
INSERT INTO `t_role_permission` VALUES ('409', '0', '55', 'courseclassify.edit');
INSERT INTO `t_role_permission` VALUES ('410', '0', '66', 'courseclassify.delete');
INSERT INTO `t_role_permission` VALUES ('414', '0', '107', 'indexconfig.edit');
INSERT INTO `t_role_permission` VALUES ('415', '0', '108', 'indexconfig.delete');
INSERT INTO `t_role_permission` VALUES ('416', '0', '1', 'management.view');

-- ----------------------------
-- Table structure for t_site
-- ----------------------------
DROP TABLE IF EXISTS `t_site`;
CREATE TABLE `t_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `theme` varchar(255) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点';

-- ----------------------------
-- Records of t_site
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL COMMENT '用户名',
  `pass` varchar(32) DEFAULT NULL COMMENT '密码',
  `nick` varchar(32) DEFAULT NULL COMMENT '昵称姓名',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(13) DEFAULT NULL COMMENT '手机号',
  `role_id` int(11) NOT NULL DEFAULT '-1' COMMENT '角色ID',
  `department_id` int(11) DEFAULT NULL COMMENT '所属部门',
  `gender` int(11) DEFAULT '0' COMMENT '性别0-女,1-男',
  `status` tinyint(1) DEFAULT NULL COMMENT '用户状态',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `visited` datetime DEFAULT NULL COMMENT '最近访问时间',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  UNIQUE KEY `uk_email` (`email`),
  UNIQUE KEY `uk_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'taeyeon', '31DC1514F9974A1CB1201BC15FE6F498', 'Yoona', 'http://static.xuehu365.com/admin/img/2Q9G0QAs.jpg', 'taeyeon@mail.kr', '18927543078', '-1', '19', '0', '1', '2014-12-24 00:00:00', '2017-05-24 20:30:02', '2013-07-09 21:26:54', '2017-06-02 00:39:22');
INSERT INTO `t_user` VALUES ('17', 'admin', 'F6FDFFE48C908DEB0F4C3BD36C032E72', '管理员', 'http://static.xuehu365.com/admin/img/2Q5U2FsG.png', 'hunan.me@gmail.com', '13412317173', '-1', '19', '1', '1', '2015-08-31 00:00:00', '2017-05-20 23:22:52', '2014-07-15 09:57:39', '2017-06-02 00:14:48');
INSERT INTO `t_user` VALUES ('18', 'test', 'F6FDFFE48C908DEB0F4C3BD36C032E72', '测试用户', 'http://static.xuehu365.com/admin/img/2Q9G0QAs.jpg', 'test@test.test', '1234567890', '0', '19', '0', '1', null, null, '2016-06-06 14:18:59', '2017-06-02 00:17:36');
INSERT INTO `t_user` VALUES ('20', 'a123', '02BDD5A0D322DE0BA211CF5E1C823E69', '无真实姓名', 'http://static.xuehu365.com/admin/img/2Q9G0QAs.jpg', '2651236249@qq.com', '15920501235', '0', null, '0', '1', null, '2016-09-20 17:05:40', '2016-09-19 17:07:47', '2016-09-20 17:05:40');
INSERT INTO `t_user` VALUES ('21', 'backflow', 'B32CE2C5D088583FE2F44DA7F82592B4', '胡楠', 'http://static.xuehu365.com/admin/img/2Q9G0QAs.jpg', 'backflow@backflow.cn', '18027546311', '0', null, '0', '1', null, '2017-06-05 21:01:01', '2016-09-19 17:41:44', '2017-06-02 00:41:18');
INSERT INTO `t_user` VALUES ('22', 'one1', '2CA28855C3D34EFF260CEC944FD6B70E', '', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', '2961625156@qq.com', '18920701001', '2', '18', '0', '1', null, '2016-09-20 16:48:39', '2016-09-20 14:39:03', '2017-06-03 00:20:48');
INSERT INTO `t_user` VALUES ('25', 'five', 'E722E929AC6C12F1A1BB043DA7CBE9B6', '', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', '123@', '12', '0', '19', '0', '1', null, null, '2016-09-20 17:02:53', '2017-06-02 00:34:13');
INSERT INTO `t_user` VALUES ('26', 'aaaa', 'A9C4020FA0FC89C333AFE8FA91228D28', '小明', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', '12345678@qq.com', '12345678912', '11', '19', '0', '1', null, '2016-09-21 09:59:32', '2016-09-21 09:43:45', '2017-06-02 00:34:46');
INSERT INTO `t_user` VALUES ('27', 'lazyone', 'ADEC27E3C7C9C73D8E5842684CE0A5A0', '', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', '13@11.com', '13767676666', '0', '18', '0', '0', null, '2017-03-01 10:32:44', '2016-09-27 18:42:53', '2017-06-05 21:26:51');
INSERT INTO `t_user` VALUES ('28', '张毅斐', '3D17AB9CEF539340BBB773AFB8317A63', '张毅斐', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13601034390', '0', '19', '0', '1', null, null, null, '2017-06-02 00:35:55');
INSERT INTO `t_user` VALUES ('29', '宋亚玲', 'BFCEE929CF63AA4C5BCEE3FE486247B5', '宋亚玲', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18811218428', '0', '19', '0', '1', null, null, null, '2017-06-02 00:39:32');
INSERT INTO `t_user` VALUES ('30', '赵心怡', 'F63CFF1FDF49AF020A317EE7375822A7', '赵心怡', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18802733761', '0', '19', '0', '1', null, null, null, '2017-06-02 00:32:30');
INSERT INTO `t_user` VALUES ('31', '甄亚茹', 'F96886E89966E0C3DD6FC2B5DDCE6275', '甄亚茹', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13051763065', '0', null, '0', '1', null, null, null, '2017-06-03 00:25:05');
INSERT INTO `t_user` VALUES ('32', '桂露玲', '8002A0CDB60CF6873824ED2DCB34635B', '桂露玲', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13661274869', '0', '19', '0', '1', null, null, null, '2017-06-02 00:17:18');
INSERT INTO `t_user` VALUES ('33', '樊欢', 'DCB61B4DF243E461955D9FD68198A036', '樊欢', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13439544614', '0', '19', '0', '1', null, null, null, '2017-06-02 00:15:52');
INSERT INTO `t_user` VALUES ('34', '张超', '3020A99BDF49FE7D6720FE1F8D6F1EA1', '张超', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18811718780', '0', '19', '0', '1', null, null, null, '2017-06-02 00:15:55');
INSERT INTO `t_user` VALUES ('35', '赵婉', '762A64B662B071164D3433BD04C87887', '赵婉', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15001056516', '0', '19', '0', '1', null, null, null, '2017-06-02 00:30:09');
INSERT INTO `t_user` VALUES ('36', '严斌', '3C35E8AF2A7E0D20568C8EE1BA3A3FC6', '严斌', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15910416381', '0', '19', '0', '1', null, null, null, '2017-06-02 00:29:58');
INSERT INTO `t_user` VALUES ('37', '刘宇', 'E9972F0834F99F8BACFE9C61308A27DE', '刘宇', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13901251351', '0', '19', '0', '1', null, null, null, '2017-06-02 00:40:04');
INSERT INTO `t_user` VALUES ('38', '刘海灵', 'A56A0F6B31AF78BEFCFF113692CD6588', '刘海灵', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18801037645', '0', null, '0', '1', null, '2017-05-21 14:40:06', null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('39', '符才锦', '0E15E56DDF37DFDCDC6C135E81CD617B', '符才锦', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18510982669', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('40', '冯子伟', '4648DC424EFFA1FCC9AC7E3563CA7B5F', '冯子伟', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13760746493', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('41', '林小凤', '7AD4A16C31A73207906C5D8F48811092', '林小凤', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15975387375', '0', '10', '0', '1', null, '2016-09-28 16:48:55', null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('42', '尹璐', '3DE0B6E212BB6C8360A816D8CCD9CC1C', '尹璐', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13538795725', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('43', '陈舒祺', 'CD3F389D9B7B33F0937A922CA09E7B02', '陈舒祺', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '17050097838', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('44', '周梅桂', '9020C115393D2080193F86A2178AA683', '周梅桂', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15817064247', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('45', '张红', '195A07A44112F107C5B6DF5CAE0DD997', '张红', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13824491253', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('46', '刘莉茹', '4D9314C8BCE2D732F915398D4B8CCE37', '刘莉茹', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13544597937', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('47', '张纯燕', '4111B5E74AD22609D2EA5B7E9C64DA31', '张纯燕', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15013126366', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('48', '唐存勇', '45688DE2C926126A748DC410AC20DC55', '唐存勇', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13710251392', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('49', '曾富', '20BC6BA0E3A02D02A68D299986F7341E', '曾富', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13798185862', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('50', '柳晓燕', '9627840EDBBE01BB1480623095EFB03A', '柳晓燕', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13710921714', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('51', '钟雪芬', 'BAD99A881185B59DEED8194E953F6B56', '钟雪芬', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13288830078', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('52', '黄燕', '77639F9528C7949473C597CE1DDBB487', '黄燕', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13318881851', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('53', '庞宁', 'B979D2EF1ABFB00127482905993490E8', '庞宁', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13512793972', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('54', '王丽娟', '82066F194006042D092EA6C084107EB9', '王丽娟', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13544481102', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('55', '蔡日诚', 'DC68A980C02BC14FC7865938F7B49679', '蔡日诚', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15918433074', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('56', '郑伟成', '2FED7D9D9BCF5459B593CE5C8AEB8B40', '郑伟成', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13632325434', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('57', '叶善勇', '484A982211B80494D87C9032F965FCE1', '叶善勇', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18122384801', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('58', '陈巧', '731E90CB80128AB967D677DFDEC590B9', '陈巧', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15999943610', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('59', '何鹏', '55CE0FC48F7CA8BE49093D382E5068A2', '何鹏', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15810789885', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('60', '韩宁宁', 'EA9E7B849584087795DC4FDBA75A3824', '韩宁宁', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13553001608', '0', null, '0', '1', null, '2017-05-21 14:51:42', null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('61', '齐海萍', '77E374D5895DDB7038D12FAE2E399BC0', '齐海萍', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15192691026', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('62', '张铎', 'A2CD97DA89144C89935E4D93585DE60F', '张铎', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13206462848', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('63', '杜亭亭', 'A6E7F40511A704436A081FFEE249408D', '杜亭亭', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15863046982', '2', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('64', '于馨甜', 'CEA976FD560BCA2F6B83BE71FE0FBB92', '于馨甜', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18561917001', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('65', '胡改霞', 'F8AC61F35F0C56AF6F512070B8FB9E54', '胡改霞', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15982106459', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('66', '苏航', 'CBEEAC3E33B91CE597B818BB3AEC6FEC', '苏航', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15908990012', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('67', '刘欢', 'E8121926512BAA2934A4FF12A4495BB0', '刘欢', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18562869488', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('68', '邱彩丽', '9447BFA0F031AC457000D20CB2E12A27', '邱彩丽', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15689953297', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('69', '张小燕', '09B0EA670A96B2EDCC0F3B9B67BD057D', '张小燕', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18650014307', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('70', '林芝', 'D78A37FABB07F5D7FE10FFDC7034D252', '林芝', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13003925728', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('71', '胡璐津', 'FE7E31756EB696EFB60FEB69EA590C2A', '胡璐津', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15160012523', '5', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('72', '许娇娇', '3056C191B3FC098D45812E735378E2C7', '许娇娇', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13063069086', '2', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('73', '解语花', 'E6EB76497FBD369D52A423BF59F16603', '解语花', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15860723282', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('74', '袁振', 'AF6E163EBBD4C663281262EDC5497F1E', '袁振', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15907106231', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('75', '吴玲玲', 'CF50084D89501B55CED2A0A585DA9F6A', '吴玲玲', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18627178105', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('76', '胡珩娟', '8A93DE2D0BEF190E5E5A703F9C5CAAB1', '胡珩娟', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18571768960', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('77', '陈哲', '7458E80481A62A58A099B2E51DD29705', '陈哲', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18674012992', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('78', '罗国兵', '0AF67B125EFFD04C15264561FD6DBFA2', '罗国兵', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18207154014', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('79', '李丙兰', 'E71DDD67245858D410DAF9E667283148', '李丙兰', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13487080805', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('80', '杜珍', '52B120D81306FB40657F9588816E0BBB', '杜珍', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15007169749', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('81', '周仕琪', '7AAAAD8C7C6AAD37D0CEA8B008DACA7D', '周仕琪', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18627758523', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('82', '马登强', '9BA56A3D906E4746A38787F1950F87A9', '马登强', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18782287308', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('83', '刘家兴', '822CFCE20FEC282EAB2A8C839CEB28F7', '刘家兴', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18382108188', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('84', '范楷', '7DA890B85C03DAB48C0E9F51A8852CE0', '范楷', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18628389780', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('85', '雷雨', 'D9A8509AB067FA2E6DF17085EB598ABF', '雷雨', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13683474120', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('86', '周晓娟', '25392B81B7CCD56922D5A714C20A5B79', '周晓娟', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13982034166', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('87', '钟元斌', 'A47A101AFB676C14617B2C951752CA0F', '钟元斌', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13882261750', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('88', '王金秀', '45002F9C3A744D2DB467F34BA36DB3ED', '王金秀', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15982879701', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('89', '刘颖嫦', 'F57C54C5E8A54179A307DA75157BE36B', '刘颖嫦', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13880103610', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('90', '钟膦榔', '534593F98A6065524BD0A9D3BDF76EE4', '钟膦榔', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18628087952', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('91', '王静瑞', '359267716073E5FEBE51D6C9C41D1161', '王静瑞', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13568814432', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('92', '方丽', '7727EF7C30D6B9D4DC1BAA38A07F02A2', '方丽', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18502387600', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('93', '曾晶', 'F5F0DE8D38FE24F650919665A3A70171', '曾晶', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13677672482', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('94', '成娜娜', '09E45FD502E5A720DDD894BB832F3768', '成娜娜', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18323731353', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('95', '李勇', 'FD1007F3116238CA11DFA4936C275856', '李勇', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15320436268', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('96', '孙庆', '46C1AEB855F3F53B4DEA4BBF9F30E8FD', '孙庆', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18723954400', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('97', '朱丹', 'FC19385CF317D81AF88D8B1503690784', '朱丹', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15111927620', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('98', '吴春霞', '29613CB09AF804B5DAA0A46C81F6CC87', '吴春霞', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15102384023', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('99', '杨宇', '526D77200409C134AF1804F865696047', '杨宇', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18030847238', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('100', '黄伟婕', '0E00CC92B9CDBE1257EEEE1E80E0166B', '黄伟婕', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18523173193', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('101', '牟敏', '32E99691AEF0EF88E799C6A2779FA64D', '牟敏', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13926522653', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('102', '莫愁', '7475070316EC14E4235E63A21ABF07AD', '莫愁', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13602661900', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('103', '王琳', '61BBA0212E365FD6AA26317F1B69B476', '王琳', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13480633406', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('104', '蓝逸娴', '7EFC0D20E184D9A0CB07E6666E7ED234', '蓝逸娴', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13590285840', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('105', '周浩', 'B0920C5E8B31CBCD7336760D2088C681', '周浩', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18948711630', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('106', '李刚', 'F9ADFA151DBF3C832082E6FCACFFDEDE', '李刚', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18689498360', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('107', '王翠翠', '3B57DB79B36BC01D87AE5363AAD175A4', '王翠翠', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15972920169', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('108', '郭俊', '34CA419A4F775AC8C4C2E0212A87BD27', '郭俊', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13560747342', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('109', '李梦飞', '2225BEA84C2E159354AA326C409E1D14', '李梦飞', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18617104918', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('110', '吴春草', '110E6519CEB3EFCBE9B783E276D41C1F', '吴春草', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15099900836', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('111', '仇春燕', 'F42398B44CA66BF75876F7BA2FC1923D', '仇春燕', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18718692697', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('112', '郭翠斌', 'B1A97F63240FF5C106F415761FA1C157', '郭翠斌', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18611757734', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('113', '肖静', 'A27168198AF67840195496E9ECCCC044', '肖静', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13926573147', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('114', '刘荣霞', 'E979CC2CF8FBCFD080010381204A9F9C', '刘荣霞', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13662267126', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('115', '谢晓康', '9C9E9C277CBF055A57FF7D43801972DD', '谢晓康', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18684989830', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('116', '唐小婷', '6CDA0F8EE10E904049E21B2B99F017EA', '唐小婷', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18520852907', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('117', '李红旭', 'A9032FDA3135826C146EE781E88D0664', '李红旭', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13168882221', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('118', '曾清', 'DAAC675A9D4CC8A46EC1988F35F610E4', '曾清', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18382016125', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('119', '陈艳', 'C8B254EE400975D54DB84421B3A43630', '陈艳', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '17381803842', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('120', '关鹤麟', 'FEAB7B9333C9C44D065B257D0ED2BE45', '关鹤麟', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18611475025', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('121', '杨慧', 'C4F9DD2644B199601EA1BAEAF03788C7', '杨慧', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13811585237', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('122', '位乾乾', 'F7FC2C2622448C7C4400A6760985E496', '位乾乾', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15311580256', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('123', '甘倩', 'FD9B59D2DA7F2C228EA1A63FD07C27DF', '甘倩', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13659037060', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('124', '胡羽芬', '36A77D2F0BB1555FB53769B48F3DA56F', '胡羽芬', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15296979562', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('125', '吴高敏', '240E77AB2A8B06B7021444C731D855C5', '吴高敏', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18520098369', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('126', '梁丽贞', 'B49E21CDFA72CB0B4F77DAC64BFB00BA', '梁丽贞', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13711202935', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('127', '刘焕仪', '6F891905B427FD9B690B889F729DF01B', '刘焕仪', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15989051225', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('128', '陈可', '47AB42701E14ADD1C431F5360EFD21C7', '陈可', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18588853692', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('129', '李娜', 'CC5B54C9A152225A256416BA6D871653', '李娜', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15621461078', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('130', '吴闻卿', '42905384E6BF2434A76C079948D22A6B', '吴闻卿', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13761122498', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('131', '蓝丽湘', 'AF02CE730CE47B8F6B01B5AA5BFFDA95', '蓝丽湘', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15807694410', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('132', '王玲玲', '19401F99C25B8D17D7BE6FD5CF964867', '王玲玲', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18948711631', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('133', '潘婷', 'A4889A67FDB5B5024DC0E6FF5CA32044', '潘婷', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13260592758', '5', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('134', '陈素婷', 'AFCECE763B2E0160CF599A79609DEC67', '陈素婷', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13611429379', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('135', '李淑珍', '25C1E99A9916EEA9F0B4F898E6B3796F', '李淑珍', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18820979104', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('136', '彭勇', 'E4FF565688B94A48864F0C3128C2428A', '彭勇', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15717507914', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('137', '彭琳', '067A0AA2AF8EB371B26B00C1A4BEC904', '彭琳', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13720208750', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('138', '黄艳', 'FAC7C65CB4B07DFA5EDB892AD32E4F46', '黄艳', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '17786409259', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('139', '姜山', '18A8B7DE4071DF717FF049A8E1DB3F95', '姜山', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18971250912', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('140', '崔静', 'CDBB9F922F8A4262A4603BA44D4AE992', '崔静', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '17701310163', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('141', '勾贺', '36AEF9A248194047C93EA4CA4D903C59', '勾贺', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18811721712', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('142', '马政', 'FA2B9705A3A7BC13CA3FC72BEEF29BB2', '马政', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15533677851', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('143', '可海鸥', 'CBC2044C928A654477976593B7D88820', '可海鸥', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15910975274', '2', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('144', '李南楠', 'ED20E9DA2E4A3CD643A6DE6B1E2C700E', '李南楠', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18328597183', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('145', '王雨嫣', 'E79BE8EA6DF069F5F86DFAA1EEE1E941', '王雨嫣', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18202876506', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('146', '郭小琴', '656AE84197180EFE39D51BB8E2D02897', '郭小琴', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18215626366', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('147', '郑超', '31D465274DD863C5B168E3A4AD74EAF2', '郑超', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18224494207', '0', '14', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('148', '武琪', 'E89C2EC0BB54996B3BA56535AFA73B71', '武琪', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15111825830', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('149', '江勇志', 'C4BDE73F97124BC9C768F796DAFCD785', '江勇志', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15922701976', '0', '18', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('150', '林观凤', 'B48FD64511A628AF9AABCDF4FD31FDF6', '林观凤', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18825125595', '0', '10', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('151', '陈凌琳', 'B171CB3C5C0638C7E8DFF1334DD83292', '陈凌琳', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18559300671', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('152', '杨希', '113D4AAD11BAA2DF44559D4181F7E951', '杨希', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13818722375', '0', null, '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('153', '谢维', '61CD354796FC62FCDA5D3DD3105E8F50', '谢维', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '18948711635', '0', '11', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('154', '宋炼红', '424E091817409FB6D1A9BF0C6B82520B', '宋炼红', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '15327378577', '0', '17', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('155', '杜海莲', '0FAC0059185EEAFB0B6B7B07BD37BCDB', '杜海莲', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13128225488', '0', '1', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('156', '周莹', '00E4E07E812EA049AAA5369EF160E218', '周莹', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', null, '13570390640', '0', '1', '0', '1', null, null, null, '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('157', 'haha', '192298A586875DD11F885B0313C968D5', '', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', '', '15900000001', '0', null, '0', '1', null, '2017-02-17 10:24:05', '2017-02-16 11:41:11', '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('158', 'erin', '14A5A0DC6A6B633AC8AB1A07CAE31FE5', '王丹君', 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', '12@1.com', '15920506789', '0', '1', '0', '1', null, '2017-03-10 14:46:19', '2017-03-10 14:46:05', '2017-05-16 09:26:16');
INSERT INTO `t_user` VALUES ('159', '测试', '', null, 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', '222@11.com', '12312332123', '0', '19', '0', '1', null, null, '2017-05-21 16:15:11', '2017-06-02 00:39:57');
INSERT INTO `t_user` VALUES ('160', 'nennen', 'D3F6589F0FBE066D196093782338D292', null, 'http://static.xuehu365.com/admin/image/2PjhO3QI.svg', 'nennen@renren.com', '18988754212', '0', '19', '0', '1', '1989-01-01 00:00:00', null, '2017-06-01 20:55:43', '2017-06-02 00:13:01');

-- ----------------------------
-- Table structure for t_user_department
-- ----------------------------
DROP TABLE IF EXISTS `t_user_department`;
CREATE TABLE `t_user_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_user_department
-- ----------------------------

-- ----------------------------
-- Table structure for t_user_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_user_resource`;
CREATE TABLE `t_user_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `resource_id` int(11) NOT NULL COMMENT '资源ID',
  `resource_type` varchar(32) NOT NULL COMMENT '资源类型(类名)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_resource
-- ----------------------------
INSERT INTO `t_user_resource` VALUES ('13', '12', '120', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('14', '20', '120', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('46', '1', '124', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('47', '20', '124', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('48', '22', '124', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('60', '21', '131', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('61', '25', '131', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('65', '20', '127', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('66', '1', '121', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('67', '18', '121', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('68', '22', '121', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('69', '26', '121', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('74', '22', '130', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('75', '26', '197', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('92', '28', '195', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('93', '21', '195', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('94', '18', '195', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('95', '1', '195', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('122', '1', '238', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('123', '53', '254', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('124', '53', '255', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('125', '53', '256', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('126', '53', '260', 'RoomTopic');
INSERT INTO `t_user_resource` VALUES ('141', '79', '264', 'RoomTopic');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('21', '17', '1');
INSERT INTO `t_user_role` VALUES ('26', '3', '2');
INSERT INTO `t_user_role` VALUES ('27', '3', '3');
INSERT INTO `t_user_role` VALUES ('39', '18', '1');
INSERT INTO `t_user_role` VALUES ('46', '19', '4');
INSERT INTO `t_user_role` VALUES ('53', '20', '4');
INSERT INTO `t_user_role` VALUES ('55', '22', '4');
INSERT INTO `t_user_role` VALUES ('57', '23', '4');
INSERT INTO `t_user_role` VALUES ('58', '24', '4');
INSERT INTO `t_user_role` VALUES ('59', '25', '4');
INSERT INTO `t_user_role` VALUES ('60', '26', '4');
INSERT INTO `t_user_role` VALUES ('61', '27', '7');
INSERT INTO `t_user_role` VALUES ('71', '28', '13');
INSERT INTO `t_user_role` VALUES ('72', '29', '13');
INSERT INTO `t_user_role` VALUES ('73', '30', '13');
INSERT INTO `t_user_role` VALUES ('74', '31', '13');
INSERT INTO `t_user_role` VALUES ('75', '32', '13');
INSERT INTO `t_user_role` VALUES ('76', '33', '13');
INSERT INTO `t_user_role` VALUES ('77', '34', '13');
INSERT INTO `t_user_role` VALUES ('78', '35', '13');
INSERT INTO `t_user_role` VALUES ('79', '36', '13');
INSERT INTO `t_user_role` VALUES ('80', '37', '13');
INSERT INTO `t_user_role` VALUES ('81', '38', '13');
INSERT INTO `t_user_role` VALUES ('82', '39', '13');
INSERT INTO `t_user_role` VALUES ('83', '40', '13');
INSERT INTO `t_user_role` VALUES ('84', '41', '13');
INSERT INTO `t_user_role` VALUES ('85', '42', '13');
INSERT INTO `t_user_role` VALUES ('86', '43', '13');
INSERT INTO `t_user_role` VALUES ('87', '44', '13');
INSERT INTO `t_user_role` VALUES ('88', '45', '13');
INSERT INTO `t_user_role` VALUES ('89', '46', '13');
INSERT INTO `t_user_role` VALUES ('90', '47', '13');
INSERT INTO `t_user_role` VALUES ('91', '48', '13');
INSERT INTO `t_user_role` VALUES ('92', '49', '13');
INSERT INTO `t_user_role` VALUES ('93', '50', '13');
INSERT INTO `t_user_role` VALUES ('94', '51', '13');
INSERT INTO `t_user_role` VALUES ('95', '52', '13');
INSERT INTO `t_user_role` VALUES ('96', '53', '13');
INSERT INTO `t_user_role` VALUES ('97', '54', '13');
INSERT INTO `t_user_role` VALUES ('98', '55', '13');
INSERT INTO `t_user_role` VALUES ('99', '56', '13');
INSERT INTO `t_user_role` VALUES ('100', '57', '13');
INSERT INTO `t_user_role` VALUES ('101', '58', '13');
INSERT INTO `t_user_role` VALUES ('102', '59', '13');
INSERT INTO `t_user_role` VALUES ('103', '60', '13');
INSERT INTO `t_user_role` VALUES ('104', '61', '13');
INSERT INTO `t_user_role` VALUES ('105', '62', '13');
INSERT INTO `t_user_role` VALUES ('106', '63', '13');
INSERT INTO `t_user_role` VALUES ('107', '64', '13');
INSERT INTO `t_user_role` VALUES ('108', '65', '13');
INSERT INTO `t_user_role` VALUES ('109', '66', '13');
INSERT INTO `t_user_role` VALUES ('110', '67', '13');
INSERT INTO `t_user_role` VALUES ('111', '68', '13');
INSERT INTO `t_user_role` VALUES ('112', '69', '13');
INSERT INTO `t_user_role` VALUES ('113', '70', '13');
INSERT INTO `t_user_role` VALUES ('114', '71', '13');
INSERT INTO `t_user_role` VALUES ('115', '72', '13');
INSERT INTO `t_user_role` VALUES ('116', '73', '13');
INSERT INTO `t_user_role` VALUES ('117', '74', '13');
INSERT INTO `t_user_role` VALUES ('118', '75', '13');
INSERT INTO `t_user_role` VALUES ('119', '76', '13');
INSERT INTO `t_user_role` VALUES ('120', '77', '13');
INSERT INTO `t_user_role` VALUES ('121', '78', '13');
INSERT INTO `t_user_role` VALUES ('122', '79', '13');
INSERT INTO `t_user_role` VALUES ('123', '80', '13');
INSERT INTO `t_user_role` VALUES ('124', '81', '13');
INSERT INTO `t_user_role` VALUES ('125', '82', '14');
INSERT INTO `t_user_role` VALUES ('126', '83', '14');
INSERT INTO `t_user_role` VALUES ('127', '84', '14');
INSERT INTO `t_user_role` VALUES ('128', '85', '14');
INSERT INTO `t_user_role` VALUES ('129', '86', '14');
INSERT INTO `t_user_role` VALUES ('130', '87', '14');
INSERT INTO `t_user_role` VALUES ('131', '88', '14');
INSERT INTO `t_user_role` VALUES ('132', '89', '14');
INSERT INTO `t_user_role` VALUES ('133', '90', '14');
INSERT INTO `t_user_role` VALUES ('134', '91', '14');
INSERT INTO `t_user_role` VALUES ('135', '92', '14');
INSERT INTO `t_user_role` VALUES ('136', '93', '14');
INSERT INTO `t_user_role` VALUES ('137', '94', '14');
INSERT INTO `t_user_role` VALUES ('138', '95', '14');
INSERT INTO `t_user_role` VALUES ('139', '96', '14');
INSERT INTO `t_user_role` VALUES ('140', '97', '14');
INSERT INTO `t_user_role` VALUES ('141', '98', '14');
INSERT INTO `t_user_role` VALUES ('142', '99', '14');
INSERT INTO `t_user_role` VALUES ('143', '100', '14');
INSERT INTO `t_user_role` VALUES ('144', '101', '14');
INSERT INTO `t_user_role` VALUES ('145', '102', '14');
INSERT INTO `t_user_role` VALUES ('146', '103', '14');
INSERT INTO `t_user_role` VALUES ('147', '104', '14');
INSERT INTO `t_user_role` VALUES ('148', '105', '14');
INSERT INTO `t_user_role` VALUES ('149', '106', '14');
INSERT INTO `t_user_role` VALUES ('150', '107', '14');
INSERT INTO `t_user_role` VALUES ('151', '108', '14');
INSERT INTO `t_user_role` VALUES ('152', '109', '14');
INSERT INTO `t_user_role` VALUES ('153', '110', '14');
INSERT INTO `t_user_role` VALUES ('154', '111', '14');
INSERT INTO `t_user_role` VALUES ('155', '112', '14');
INSERT INTO `t_user_role` VALUES ('156', '113', '14');
INSERT INTO `t_user_role` VALUES ('157', '114', '14');
INSERT INTO `t_user_role` VALUES ('158', '115', '14');
INSERT INTO `t_user_role` VALUES ('159', '116', '14');
INSERT INTO `t_user_role` VALUES ('160', '117', '14');
INSERT INTO `t_user_role` VALUES ('161', '118', '11');
INSERT INTO `t_user_role` VALUES ('162', '119', '11');
INSERT INTO `t_user_role` VALUES ('163', '120', '11');
INSERT INTO `t_user_role` VALUES ('164', '121', '11');
INSERT INTO `t_user_role` VALUES ('165', '122', '11');
INSERT INTO `t_user_role` VALUES ('166', '123', '11');
INSERT INTO `t_user_role` VALUES ('167', '124', '11');
INSERT INTO `t_user_role` VALUES ('168', '125', '11');
INSERT INTO `t_user_role` VALUES ('169', '126', '11');
INSERT INTO `t_user_role` VALUES ('170', '127', '11');
INSERT INTO `t_user_role` VALUES ('171', '128', '11');
INSERT INTO `t_user_role` VALUES ('172', '129', '11');
INSERT INTO `t_user_role` VALUES ('173', '130', '11');
INSERT INTO `t_user_role` VALUES ('174', '131', '11');
INSERT INTO `t_user_role` VALUES ('175', '132', '11');
INSERT INTO `t_user_role` VALUES ('176', '133', '11');
INSERT INTO `t_user_role` VALUES ('177', '134', '11');
INSERT INTO `t_user_role` VALUES ('178', '135', '11');
INSERT INTO `t_user_role` VALUES ('179', '136', '11');
INSERT INTO `t_user_role` VALUES ('180', '137', '11');
INSERT INTO `t_user_role` VALUES ('181', '138', '11');
INSERT INTO `t_user_role` VALUES ('182', '139', '11');
INSERT INTO `t_user_role` VALUES ('183', '140', '12');
INSERT INTO `t_user_role` VALUES ('184', '141', '12');
INSERT INTO `t_user_role` VALUES ('185', '142', '12');
INSERT INTO `t_user_role` VALUES ('186', '143', '12');
INSERT INTO `t_user_role` VALUES ('187', '144', '12');
INSERT INTO `t_user_role` VALUES ('188', '145', '12');
INSERT INTO `t_user_role` VALUES ('189', '146', '12');
INSERT INTO `t_user_role` VALUES ('190', '147', '12');
INSERT INTO `t_user_role` VALUES ('191', '148', '12');
INSERT INTO `t_user_role` VALUES ('192', '149', '12');
INSERT INTO `t_user_role` VALUES ('193', '150', '12');
INSERT INTO `t_user_role` VALUES ('194', '151', '12');
INSERT INTO `t_user_role` VALUES ('195', '152', '12');
INSERT INTO `t_user_role` VALUES ('196', '153', '12');
INSERT INTO `t_user_role` VALUES ('197', '154', '12');
INSERT INTO `t_user_role` VALUES ('198', '155', '15');
INSERT INTO `t_user_role` VALUES ('199', '156', '15');
INSERT INTO `t_user_role` VALUES ('200', '158', '1');
INSERT INTO `t_user_role` VALUES ('220', '21', '4');
INSERT INTO `t_user_role` VALUES ('221', '21', '3');
INSERT INTO `t_user_role` VALUES ('222', '21', '2');
INSERT INTO `t_user_role` VALUES ('223', '21', '1');
