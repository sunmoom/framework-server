
package cn.backflow.lib.thirdpart;

import cn.backflow.lib.util.StringUtil;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Client;
import com.qiniu.http.Response;
import com.qiniu.processing.OperationManager;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.FileListing;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import com.qiniu.util.StringUtils;
import com.qiniu.util.UrlSafeBase64;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 七牛上传工具类, 请传入不同的命名空间(namespace.bucket)实例化调用
 *
 * @author Nandy
 */
public class QiniuUtil {
    public static final String QINIU_DOMAIN = "http://static.xuehu365.com/";
    // 密钥配置
    private static final Auth auth = Auth.create(
            "t_S7YjQEDjG9zMcfzT6mbqABRFYHeEsnkwPbHiV2", // ACCESS_KEY
            "xUBor0RCSc3tHqeJt4xSFbzA9qM0QQQ3ZjfeEJ6f"  // SECRET_KEY
    );
    //创建上传对象
    private static final UploadManager um = new UploadManager();
    private static final BucketManager bm = new BucketManager(auth);
    private static final OperationManager om = new OperationManager(auth);
    private static final Client client = new Client();
    private static Logger log = LoggerFactory.getLogger(QiniuUtil.class);
    // 文件保存的命名空间, 这里默认为 xuehu-app
    private Namespace namespace = Namespace.APP;

    public QiniuUtil() {
        this.namespace = Namespace.ADMIN;
    }

    public QiniuUtil(Namespace namespace) {
        this.namespace = namespace;
    }

    public static QiniuUtil getInstance(Namespace namespace) {
        return new QiniuUtil(namespace);
    }

    public static String buildUrl(Object key) {
        return QINIU_DOMAIN + key;
    }

    public static void main(String[] args) throws Exception {
        //        String audio = "http://static.xuehu365.com/app/audio/voice.amr";
        //        String notyfyURL = "http://test.app.xuehu365.com/thirdpart/qiniu/notify";
        //
        //        audioConvert(audio, notyfyURL, "mp3", "m4a");
        final QiniuUtil qiniuUtil = QiniuUtil.getInstance(Namespace.ADMIN);
        final StringMap policy = new StringMap();
        policy.put("callbackUrl", "http://test.app.xuehu365.com/thirdpart/qiniu/notify");
        policy.put("callbackBody", "key=$(key)&hash=$(etag)&w=$(imageInfo.width)&h=$(imageInfo.height)");
        qiniuUtil.executeUpload(new QiniuUploadCallback<Response>() {
            @Override
            public Response doUpload() throws QiniuException {
                return um.put(new File("D:\\angela.jpg"), "admin/angela.jpg", qiniuUtil.upToken(3600, policy));
            }
        });
    }

    /**
     * 按最大阈值获取图片缩放后的宽高
     *
     * @param width     原始图片宽度
     * @param height    原始图片高度
     * @param threshold 阈值
     * @return 缩放后的宽高数组
     */
    public static int[] getImageDimentionByThreshold(int width, int height, int threshold) {
        if (width <= threshold && height <= threshold)
            return new int[]{width, height};

        double w = width;
        double h = height;

        if (width > height) {
            width = threshold;
            height *= threshold / w;
        } else {
            height = threshold;
            width *= threshold / h;
        }
        return new int[]{width, height};
    }

    public static JSONObject imageinfo(String url) throws IOException, URISyntaxException {
        if (!url.startsWith(QINIU_DOMAIN)) {
            Dimension dimension = dimension(url);
            return new JSONObject()
                    .put("width", dimension.getWidth())
                    .put("height", dimension.getHeight());
        }
        return new JSONObject(client.get(url + "?imageInfo").bodyString());
    }

    public static JSONObject audioinfo(String url) throws QiniuException {
        return new JSONObject(client.get(url + "?avinfo").bodyString());
    }

    public static JSONObject videofo(String url) throws QiniuException {
        String str = client.get(url + "?avinfo").bodyString();
        return new JSONObject(str);
    }

    public static Dimension dimension(String url) throws IOException, URISyntaxException {
        Dimension dimension = new Dimension(227, 227);
        try (ImageInputStream in = ImageIO.createImageInputStream(new URL(url).openStream())) {
            final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
            if (readers.hasNext()) {
                ImageReader reader = readers.next();
                try {
                    reader.setInput(in);
                    dimension = new Dimension(reader.getWidth(0), reader.getHeight(0));
                } finally {
                    reader.dispose();
                }
            }
        }
        return dimension;
    }

    // 简单上传，使用默认策略，只需要设置上传的空间名就可以了
    public String getUpToken() {
        return auth.uploadToken(namespace.bucket);
    }

    /**
     * 根据文件类型与文件名创建key,
     *
     * @param type     FileType 文件类型枚举
     * @param filename 文件名
     * @return 用作上传与访问的key
     */
    public String buildKey(FileType type, String filename) throws InterruptedException {
        return namespace.name().toLowerCase() + "/" +   // 命名空间
                type.name().toLowerCase() + "/" +       // 文件类型
                StringUtil.reverse_shorten() + "." +    // 随机文件名
                FilenameUtils.getExtension(filename);   // 扩展名
    }

    /**
     * 根据文件类型与文件名创建key,
     *
     * @param folder   文件夹
     * @param filename 文件名
     * @return 用作上传与访问的key
     */
    public String buildKey(String folder, String filename) throws InterruptedException {
        return folder.toLowerCase() +      // 命名空间
                StringUtil.reverse_shorten() + "." +    // 随机文件名
                FilenameUtils.getExtension(filename);   // 扩展名
    }

    /**
     * 上传文件, 自动生成KEY
     *
     * @param file 文件
     * @return 上传成功, 返回文件key, 否则返回NULL
     */
    public String upload(final File file) {
        String key = StringUtil.reverse_shorten() + FilenameUtils.getExtension(file.getName());
        Response response = upload(file, key);
        return response.isOK() ? key : null;
    }

    public Response upload(final File file, final String key) {
        return executeUpload(new QiniuUploadCallback<Response>() {
            @Override
            public Response doUpload() throws QiniuException {
                return um.put(file, key, getUpToken());
            }
        });
    }

    /**
     * 上传文件, 自动生成KEY
     *
     * @param bytes     字节数组
     * @param folder    虚拟文件夹
     * @param extension 文件后缀名, 可为空
     * @return 上传成功, 返回文件key, 否则返回NULL
     */
    public String uploadWithAutoGeneratedKey(final byte[] bytes, String folder, String extension) throws Exception {
        if (folder == null || folder.isEmpty()) {
            throw new Exception("folder must be not null!");
        }
        folder = "/" + folder.replaceFirst("(^/*)|(/*$)", "") + "/";
        if (extension == null) {
            extension = "";
        }
        extension = "." + extension.replaceFirst("\\.", "");
        final String key = folder + StringUtil.reverse_shorten() + extension;
        Response response = executeUpload(new QiniuUploadCallback<Response>() {
            @Override
            public Response doUpload() throws QiniuException {
                return um.put(bytes, key, getUpToken());
            }
        });
        return response.isOK() ? key : null;
    }

    public Response upload(final byte[] bytes, final String key) {
        return executeUpload(new QiniuUploadCallback<Response>() {
            @Override
            public Response doUpload() throws QiniuException {
                return um.put(bytes, key, getUpToken());
            }
        });
    }

    public Response upload(final String filepath, final String key) {
        return executeUpload(new QiniuUploadCallback<Response>() {
            @Override
            public Response doUpload() throws QiniuException {
                return um.put(filepath, key, getUpToken());
            }
        });
    }

    public FileListing list(String prefix, String marker, int limit, String delimiter) throws QiniuException {
        return bm.listFiles(namespace.bucket, prefix, marker, limit, delimiter);

    }

    /**
     * 根据前缀获取文件列表的迭代器
     *
     * @param prefix    文件名前缀
     * @param limit     每次迭代的长度限制，最大1000，推荐值 100
     * @param delimiter 指定目录分隔符，列出所有公共前缀（模拟列出目录效果）。缺省值为空字符串
     * @return FileInfo迭代器
     */
    public BucketManager.FileListIterator createFileListIterator(String prefix, int limit, String delimiter) {
        return bm.createFileListIterator(namespace.bucket, prefix, limit, delimiter);
    }

    public void delete(final String... keys) throws QiniuException {
        if (keys == null) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (String key : keys) {
                    try {
                        bm.delete(namespace.bucket, key);
                    } catch (QiniuException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    /**
     * 七牛音频格式持久化转换
     *
     * @param url       源音频
     * @param notifyURL 回调接口地址
     * @param formats   目标音频格式后缀名
     */
    public String audioConvert(String url, String notifyURL, String... formats) {

        if (formats == null || formats.length < 1) {
            throw new IllegalArgumentException("formats can not be null or empty");
        }

        String key = url.replaceFirst(QINIU_DOMAIN, "");
        String name = key.substring(0, key.lastIndexOf("."));


        //设置转码操作参数
        List<String> list = new ArrayList<>();
        for (String f : formats) {
            if (!f.equals(url.substring(url.lastIndexOf(".") + 1))) {
                list.add("avthumb/" + f + "|saveas/" + UrlSafeBase64.encodeToString(namespace.bucket + ":" + name + "." + f));
            }
        }

        StringMap params = new StringMap()
                .putNotEmpty("notifyURL", notifyURL) // 回调URL
                .putNotEmpty("pipeline", "audio"); // 设置 pipeline 参数
        try {
            String persistid = om.pfop(namespace.bucket, key, StringUtils.join(list, ";"), params);
            //打印返回的persistid
            return persistid;
        } catch (QiniuException e) {
            //捕获异常信息
            Response r = e.response;
            // 请求失败时简单状态信息
            log.error("qiniuexception", e);
            try {
                // 响应的文本信息
                log.error("qiniuexception body:", r.bodyString());
            } catch (QiniuException e1) {
                //ignore
            }
        }
        return null;
    }

    public String upToken(long expires, StringMap policy) {
        return auth.uploadToken(namespace.bucket, null, expires, policy);
    }

    /**
     * 模板方法, 用于处理公共异常, 打印或记录日志
     */
    private Response executeUpload(QiniuUploadCallback<Response> callback) {
        Response res = null;
        try {
            res = callback.doUpload();
            System.out.println(res.bodyString());
        } catch (QiniuException e) {
            Response r = e.response;
            // 请求失败时打印的异常的信息
            System.out.println(r.toString());

            try { //响应的文本信息
                System.out.println(r.bodyString());
            } catch (QiniuException ignore) {
                // ignore
            }
        }
        return res;
    }

    public BucketManager getBucketManager() {
        return bm;
    }


    public enum Namespace {
        APP("xuehu365"), ADMIN("xuehu365"), SERVER("xuehu365");

        public String bucket;

        Namespace(String bucket) {
            this.bucket = bucket;
        }
    }

    public enum FileType {
        IMG, VIDEO, AUDIO, DOC;
    }

    /**
     * 回调接口, 实现该接口处理实际业务逻辑
     */
    private interface QiniuUploadCallback<T> {
        T doUpload() throws QiniuException;
    }
}