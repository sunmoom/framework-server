package cn.backflow.admin.controller;

import cn.backflow.admin.common.secure.Permissions;
import cn.backflow.admin.common.treeable.Treeable;
import cn.backflow.admin.entity.Permission;
import cn.backflow.admin.common.pagination.PageRequest;
import cn.backflow.admin.common.treeable.Treenode;
import cn.backflow.admin.controller.base.BaseSpringController;
import cn.backflow.admin.service.PermissionService;
import cn.backflow.lib.util.JsonMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.tree.Tree;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("permission")
public class PermissionController extends BaseSpringController {
    protected static final String DEFAULT_SORT_COLUMNS = "priority desc, updated desc"; //默认多列排序,example: username desc,created asc

    @Autowired
    private PermissionService permissionService;

    /* 查询 */
    @RequestMapping
    @Permissions("permission.view")
    public Object query(HttpServletRequest request) {
        PageRequest pageRequest = pageRequest(request, DEFAULT_SORT_COLUMNS);
        return permissionService.findPage(pageRequest);
    }

    /* treetable 列表 */
    @RequestMapping("treetable")
    @Permissions("permission.view")
    public Object treetable() {
        Collection<Permission> list = permissionService.findAll();
        return Treeable.sort(list, null, new ArrayList<>());
    }

    /* 获取 */
    @RequestMapping("{id}")
    @Permissions("permission.view")
    public Object byId(@PathVariable Integer id) throws Exception {
        return permissionService.getById(id);
    }

    /* 创建 */
    @RequestMapping("new")
    @Permissions("permission.edit")
    public Object _new() throws Exception {
        return new Permission();
    }

    @RequestMapping("jstree")
    public List<Treenode> jstree(
            @RequestParam(value = "pid", required = false) Integer pid,
            @RequestParam(value = "selected", required = false) Collection<Integer> selected,
            @RequestParam(value = "unselectable", required = false) Collection<Integer> unselectable) {
        List<Permission> permissions = permissionService.findAll(Collections.singletonMap("parentId", pid));
        return Treeable.jstree(permissions, selected, unselectable);
    }

    @RequestMapping("tree")
    public List<Treeable> tree(
            @RequestParam(value = "pid", required = false) Integer pid) {
        List<Permission> permissions = permissionService.findAll(Collections.singletonMap("parentId", pid));
        return Treeable.tree(permissions);
    }

    /* 保存新增 */
    @Permissions("permission.edit")
    @RequestMapping(method = RequestMethod.POST)
    public Object create(@Valid Permission permission, BindingResult errors) throws Exception {
        JsonMap json = JsonMap.succeed();
        if (errors.hasErrors()) {
            return filedErrors(errors, json);
        }
        permissionService.save(permission);
        return json;
    }

    /* 保存更新 */
    @Permissions("permission.edit")
    @RequestMapping(method = RequestMethod.PUT)
    public Object update(@Valid Permission permission, BindingResult errors) throws Exception {
        JsonMap json = JsonMap.succeed();
        if (errors.hasErrors()) {
            return filedErrors(errors, json);
        }
        permissionService.update(permission);
        return json;
    }

    /* 删除 */
    @Permissions("permission.delete")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public Object delete(@PathVariable Integer id) {
        permissionService.deleteById(id);
        return JsonMap.succeed();
    }

    /* 批量删除 */
    @Permissions("permission.delete")
    @RequestMapping(method = RequestMethod.DELETE)
    public Object delete(@RequestParam("items") Set<Integer> items) {
        permissionService.deleteBatch(items);
        return JsonMap.succeed();
    }
}