package cn.backflow.admin.controller.base;

import cn.backflow.admin.common.Constants;
import cn.backflow.admin.common.pagination.Page;
import cn.backflow.admin.common.pagination.PageRequest;
import cn.backflow.admin.entity.User;
import cn.backflow.lib.util.JsonMap;
import cn.backflow.lib.util.JsonUtil;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isBlank;

public abstract class BaseSpringController {
    // protected static Logger logger = LoggerFactory.getLogger(BaseSpringController.class);

    protected static int DEFAULT_PAGE_SIZE = 10, MAX_PAGE_SIZE = 500;
    public static final String PARAM_PAGESIZE = "ps", PARAM_PAGENUMBER = "pn", PARAM_SORTCOLUMNS = "sc";



    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-M-d"), true));
    }

    public PageRequest pageRequest(HttpServletRequest request, String defaultSortColumns) {
        return pageRequest(request, defaultSortColumns, DEFAULT_PAGE_SIZE);
    }

    public PageRequest pageRequest(HttpServletRequest request, String defaultSortColumns, Integer defaultPageSize) {
        String sort = request.getParameter(PARAM_SORTCOLUMNS);
        String numb = request.getParameter(PARAM_PAGENUMBER);
        String size = request.getParameter(PARAM_PAGESIZE);

        if (defaultPageSize == null) {
            defaultPageSize = DEFAULT_PAGE_SIZE;
        }
        int pageSize = parseNumber(size, defaultPageSize);
        if (pageSize > MAX_PAGE_SIZE) {
            pageSize = MAX_PAGE_SIZE;
        }

        int pageNumber = parseNumber(numb, 1);
        if (isBlank(sort)) {
            sort = defaultSortColumns;
        }

        PageRequest pr = new PageRequest(pageSize, pageNumber, sort);
        setParameters(pr, request);
        return pr;
    }

    private int parseNumber(String value, int defaultValue) {
        if (value == null || value.trim().isEmpty())
            return defaultValue;
        return value.matches("\\d+") ? Integer.valueOf(value) : defaultValue;
    }

    public PageRequest setParameters(PageRequest pr, HttpServletRequest request) {
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            String[] values = entry.getValue();
            String value = values[0].trim();
            if (value.isEmpty()) {
                continue;
            }
            if (values.length == 1) {
                pr.addInitialFilter(entry.getKey(), value);
                continue;
            }
            List<String> list = new ArrayList<>();
            for (String s : values) {
                s = s.trim();
                if (s.isEmpty()) {
                    continue;
                }
                list.add(s);
            }
            pr.addInitialFilter(entry.getKey(), list.toArray(new Object[list.size()]));
        }
        return pr.setRequestUri(request.getRequestURI());
    }

    protected User getCurrentUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(Constants.SESSION_USER_KEY);
    }

    /**
     * Errors mapping for jquery.validate.js showErrors() method
     *
     * @param errors BindingResult
     */
    protected JsonMap filedErrors(BindingResult errors, JsonMap json) {
        JsonMap child = json.success(false).child("errors");
        for (ObjectError e : errors.getAllErrors()) {
            child.put(e.getCode(), e.getDefaultMessage());
        }
        return json;
    }

    protected void toJsonMap(Page<?> page, PageRequest pageRequest, JsonMap json) {
        json.put("pageRequest", pageRequest);
        json.put("page", page);
    }

    /**
     * Ajax响应
     */
    private void ajaxResponse(HttpServletResponse response, Object object) throws Exception {
        String data;
        if (object instanceof String) {
            response.setContentType("text/html;charset=utf-8");
            data = object.toString();
        } else {
            response.setContentType("application/json;charset=utf-8");
            data = JsonUtil.toJson(object);
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(data);
        }
    }
}
