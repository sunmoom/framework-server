package cn.backflow.admin.controller.thirdpart;

import cn.backflow.admin.controller.base.BaseSpringController;
import com.qiniu.http.Response;
import com.qiniu.storage.model.FileInfo;
import cn.backflow.admin.entity.Dict;
import cn.backflow.admin.entity.FileMapping;
import cn.backflow.admin.entity.User;
import cn.backflow.admin.service.DictService;
import cn.backflow.admin.service.FileMappingService;
import cn.backflow.lib.thirdpart.QiniuUtil;
import cn.backflow.lib.util.JsonMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 第三方资源操作接口
 * Created by Nandy on 2016/7/1.
 */
@RestController
@RequestMapping("thirdpart")
public class ThirdpartController extends BaseSpringController {

    private QiniuUtil qiniuUtil = QiniuUtil.getInstance(QiniuUtil.Namespace.ADMIN);

    @Autowired
    private FileMappingService fileMappingService;
    @Autowired
    private DictService dictService;

    @RequestMapping(value = "filebrowser")
    public Object filebrowser(@RequestParam("action") String action, HttpServletRequest request) {
        User user = getCurrentUser(request);
        JsonMap json = JsonMap.succeed();
        try {
            switch (action) {
                case "folder":
                    List<Dict> dicts = dictService.findByCode("file_browser_folder");
                    json.put("folders", dicts);
                    break;
                case "files":
                    String folder = request.getParameter("folder");
                    json.put("files", fileMappingService.findByFolder(folder, 1, 50).getItems());
                    break;
                case "create":
                    break;
                case "upload":
                    doUpload(request, json);
                    break;
                case "move":
                    break;
                case "delete":
                    if (!user.isAdmin()) {
                        json.success(false);
                        json.msg("非管理员不能执行删除操作.");
                        break;
                    }
                    String[] keys = request.getParameterValues("keys[]");
                    fileMappingService.deleteByKeys(keys);
                    qiniuUtil.delete(keys);
                    break;
            }
        } catch (Exception e) {
            json.success(false);
            json.put("msg", e.getMessage());
        }
        return json;
    }

    private void doUpload(HttpServletRequest request, JsonMap json) throws InterruptedException, IOException {
        String folder;
        folder = request.getParameter("folder");
        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        List<MultipartFile> filelist = req.getFiles("files");
        List<FileMapping> mappings = new ArrayList<>();
        List<String> failed = new ArrayList<>();
        Date now = new Date();
        for (MultipartFile file : filelist) {
            String key = qiniuUtil.buildKey(folder, file.getOriginalFilename());
            Response res = qiniuUtil.upload(file.getBytes(), key);
            if (!res.isOK()) {
                failed.add(file.getOriginalFilename());
                continue;
            }
            mappings.add(FileMapping.fromFileAndQiniuResponse(file, res, folder, now));
        }
        fileMappingService.save(mappings);
        if (!failed.isEmpty()) {
            json.put("failed", failed);
        }
        json.put("succeed", mappings);
    }

    // {msg: 'File very big!', error: 1, images: []}
    @RequestMapping("jodit/upload")
    public Object jodit_upload(MultipartHttpServletRequest request) {
        JsonMap json = JsonMap.succeed();
        List<String> images = new ArrayList<>();
        String msg = "";
        try {
            for (Map.Entry<String, MultipartFile> entry : request.getFileMap().entrySet()) {
                MultipartFile file = entry.getValue();
                String key = qiniuUtil.buildKey("admin/img/", entry.getValue().getOriginalFilename());
                Response res = qiniuUtil.upload(entry.getValue().getBytes(), key);
                if (!res.isOK()) {
                    msg += file.getOriginalFilename() + "上传失败";
                    json.put("error", 1);
                } else {
                    images.add(QiniuUtil.buildUrl(key));
                }
            }
        } catch (Exception e) {
            json.msg(e.getMessage());
            json.put("error", 1);
            e.printStackTrace();
        }
        json.put("images", images);
        json.put("msg", msg);
        return json;
    }

    @RequestMapping("editor/upload")
    public void editor_upload(MultipartHttpServletRequest request, HttpServletResponse response) throws IOException {
        String result;
        try {
            MultipartFile file = request.getFile("file");
            String key = qiniuUtil.buildKey("admin/img/", file.getOriginalFilename());
            Response res = qiniuUtil.upload(file.getBytes(), key);
            if (!res.isOK()) {
                result = "error|" + file.getOriginalFilename() + "上传失败";
            } else {
                result = QiniuUtil.buildUrl(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = "error|" + e.getMessage();
        }
        response.getWriter().write(result);
    }

    private List<FileMapping> createFileMappings(FileInfo[] infos) {
        List<String> keys = new ArrayList<>();
        for (FileInfo info : infos) {
            keys.add(info.key);
        }
        return fileMappingService.findByKeys(keys);
    }
}