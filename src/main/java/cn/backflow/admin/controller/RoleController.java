package cn.backflow.admin.controller;

import cn.backflow.admin.common.pagination.PageRequest;
import cn.backflow.admin.common.secure.Permissions;
import cn.backflow.admin.common.treeable.Treeable;
import cn.backflow.admin.common.treeable.Treenode;
import cn.backflow.admin.controller.base.BaseSpringController;
import cn.backflow.admin.entity.Permission;
import cn.backflow.admin.entity.Role;
import cn.backflow.admin.entity.User;
import cn.backflow.admin.service.PermissionService;
import cn.backflow.admin.service.RoleService;
import cn.backflow.lib.util.JsonMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;

;

@RestController
@RequestMapping("role")
public class RoleController extends BaseSpringController {
    private static final String DEFAULT_SORT_COLUMNS = "id desc"; //默认多列排序,example: username desc,createTime asc

    private final PermissionService permissionService;
    private final RoleService roleService;

    @Autowired
    public RoleController(PermissionService permissionService, RoleService roleService) {
        this.permissionService = permissionService;
        this.roleService = roleService;
    }

    /* 列表 */
    @RequestMapping
    @Permissions("role.view")
    public Object index(HttpServletRequest request) throws Exception {
        PageRequest pageRequest = pageRequest(request, DEFAULT_SORT_COLUMNS);
        return roleService.findPage(pageRequest);
    }

    /* 显示 */
    @RequestMapping("{id}")
    @Permissions("role.view")
    public Role get(@PathVariable Integer id) throws Exception {
        if (id == null) {
            return new Role();
        }
        return roleService.getById(id);
    }

    @RequestMapping("perms")
    public Object perms(@RequestParam("id") Integer id, HttpServletRequest request) {
        JsonMap json = JsonMap.succeed();
        Map<Comparable, Permission> perms = permissionService.findMap(null);
        Map<Comparable, Permission> owns = permissionService.findMapByRoleId(id);
        List<Treenode> treenodes = Treeable.jstree(perms.values(), owns.keySet(), null);
        json.put("treenodes", treenodes);
        json.put("selected", owns.keySet());
        return json;
    }

    /* 获取指定角色拥有的权限 */
    @RequestMapping("owns")
    public List<Integer> owns(@RequestParam("id") Integer id) {
        return roleService.findOwnedPermissionId(Collections.singletonMap("roleId", id));
    }

    /* 获取当前用户所有角色 */
    @RequestMapping("all")
    public List<Role> all() {
        return roleService.findAll(Collections.singletonMap("status", 1));
    }

    /* 获取当前用户所有 ID=>角色 映射MAP */
    @RequestMapping("map")
    public Object map() {
        return roleService.findMap(Collections.singletonMap("status", 1), "id");
    }

    /* 保存新增 */
    @Permissions("role.edit")
    @RequestMapping(method = RequestMethod.POST)
    public Object create(@Valid Role role, BindingResult errors, HttpServletRequest request) throws Exception {
        JsonMap json = JsonMap.succeed();
        if (errors.hasErrors()) {
            return filedErrors(errors, json);
        }
        // String[] arr = request.getParameterValues("perms");
        // Set<String> perms = arr == null ? Collections.emptySet() : new HashSet<>(Arrays.asList(arr));
        try {
            roleService.save(role);
        } catch (Exception e) {
            if (e.getCause().getLocalizedMessage().startsWith("Duplicate entry")) {
                return json.success(false).msg("角色名 [" + role.getName() + "] 已存在!");
            }
        }
        return json.put("role", role);
    }

    /* 保存更新 */
    @Permissions("role.edit")
    @RequestMapping(method = RequestMethod.PUT)
    public Object update(@Valid Role role, BindingResult errors, HttpServletRequest request) throws Exception {
        JsonMap json = JsonMap.succeed();
        User user = getCurrentUser(request);
        if (role.getId() == -1 && !user.isAdmin()) {
            return json.success(false).msg("系统管理员角色不能乱动哦!");
        }
        if (errors.hasErrors()) {
            return filedErrors(errors, json);
        }
        try {
            roleService.update(role);
        } catch (Exception e) {
            if (e.getCause().getLocalizedMessage().startsWith("Duplicate entry")) {
                return json.success(false).msg("角色名 [" + role.getName() + "] 已存在!");
            }
        }
        return json.put("role", role);
    }

    /* 保存更新 */
    @Permissions("role.edit")
    @RequestMapping(value = "{id}/perms", method = RequestMethod.PUT)
    public Object perms(@RequestParam("perms") String[] perms) throws Exception {
        JsonMap json = JsonMap.succeed();

        return json;
    }

    /* 删除 */
    @Permissions("role.delete")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public Object delete(@PathVariable Integer id) {
        if (id == -1) {
            return JsonMap.fail("系统管理员角色不可删除");
        }
        if (id == 0) {
            return JsonMap.fail("不能删除系统默认角色");
        }
        roleService.deleteById(id);
        return JsonMap.succeed();
    }
}