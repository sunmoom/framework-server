package cn.backflow.admin.entity;

import cn.backflow.admin.entity.base.BaseEntity;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;


public class Version extends BaseEntity {

    public static final String TABLE_ALIAS = "APP版本";

    @Pattern(regexp = "^\\d{1,3}\\.\\d{1,3}(\\.\\d{1,3})?$", message = "版本格式错误, 请点击左侧问号查看版本号命名规范.")
    private String version;
    @Max(127)
    private int forceUpdate = 0;
    @Max(127)
    private int updateByUser = 0;
    @Max(127)
    private int updateByPercent = 0;
    @NotBlank(message = "请输入更新内容描述.")
    @Length(max = 500)
    private String content;
    @Max(127)
    private Integer status;
    @NotBlank
    @Length(max = 250)
    private String link;

    public void setVersion(String value) {
        this.version = value;
    }

    public String getVersion() {
        return this.version;
    }

    public void setForceUpdate(int value) {
        this.forceUpdate = value;
    }

    public int getForceUpdate() {
        return this.forceUpdate;
    }

    public void setUpdateByUser(int value) {
        this.updateByUser = value;
    }

    public int getUpdateByUser() {
        return this.updateByUser;
    }

    public void setUpdateByPercent(int value) {
        this.updateByPercent = value;
    }

    public int getUpdateByPercent() {
        return this.updateByPercent;
    }

    public void setContent(String value) {
        this.content = value;
    }

    public String getContent() {
        return this.content;
    }

    public void setStatus(Integer value) {
        this.status = value;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setLink(String value) {
        this.link = value;
    }

    public String getLink() {
        return this.link;
    }

}