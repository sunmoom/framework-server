package cn.backflow.admin.entity;

import cn.backflow.admin.entity.base.BaseEntity;
import cn.backflow.lib.util.DateUtil;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.util.StringMap;
import cn.backflow.lib.thirdpart.QiniuUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

public class FileMapping extends BaseEntity {

    public static final String TABLE_ALIAS = "文件映射";

    private String key;
    private String url;
    private String hash;
    private long size;
    private String folder;
    private String filename;
    private String extention;
    private String mimetype;
    private Date uploadTime;

    public static FileMapping fromFileAndQiniuResponse(MultipartFile file, Response res, String folder, Date time) throws QiniuException {
        FileMapping mapping = new FileMapping();
        if (res.isJson()) {
            StringMap map = res.jsonToMap();
            mapping.key = map.get("key").toString();
            mapping.hash = map.get("hash").toString();
        }
        mapping.url = QiniuUtil.buildUrl(mapping.key);
        mapping.size = file.getSize();
        mapping.folder = folder;
        mapping.filename = file.getOriginalFilename();
        mapping.extention = FilenameUtils.getExtension(mapping.filename);
        mapping.mimetype = file.getContentType();
        mapping.uploadTime = time;
        return mapping;
    }

    public static FileMapping fromFileInfo(FileInfo info) {
        FileMapping mapping = new FileMapping();
        mapping.key = info.key;
        mapping.hash = info.hash;
        mapping.size = info.fsize;
        mapping.url = QiniuUtil.buildUrl(info.key);
        mapping.filename = info.key.substring(info.key.lastIndexOf("/") + 1);
        mapping.mimetype = info.mimeType;
        mapping.uploadTime = new Date(info.putTime);
        mapping.extention = FilenameUtils.getExtension(info.key);
        return mapping;
    }

    public FileMapping() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getExtention() {
        return extention;
    }

    public void setExtention(String extention) {
        this.extention = extention;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public String getUploadTimeString() {
        return DateUtil.formatDate(uploadTime, DateUtil.DATE_TIME_FORMAT);
    }
}