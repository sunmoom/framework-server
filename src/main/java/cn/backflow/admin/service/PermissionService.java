package cn.backflow.admin.service;

import cn.backflow.admin.common.treeable.Treeable;
import cn.backflow.admin.dao.PermissionDao;
import cn.backflow.admin.dao.RoleDao;
import cn.backflow.admin.entity.Permission;
import cn.backflow.admin.entity.User;
import cn.backflow.admin.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@CacheConfig(cacheNames = "perm_cache")
public class PermissionService extends BaseService<Permission, Integer> {

    private final PermissionDao permissionDao;
    private final RoleDao roleDao;

    @Autowired
    public PermissionService(PermissionDao permissionDao, RoleDao roleDao) {
        this.permissionDao = permissionDao;
        this.roleDao = roleDao;
    }

    @Override
    @Transactional
    @CacheEvict(allEntries = true)
    public int save(Permission entity) throws DataAccessException {
        setPermissionAncestorAndLevel(entity);
        return super.saveOrUpdate(entity);
    }


    @Transactional
    @CacheEvict(allEntries = true)
    public int update(Permission entity) throws DataAccessException {
        setPermissionAncestorAndLevel(entity);
        return super.saveOrUpdate(entity);
    }

    @Override
    @Transactional
    @CacheEvict(allEntries = true)
    public int deleteById(Integer id) throws DataAccessException {
        List<Permission> children = findByParentId(id);
        if (children.size() > 1)
            throw new IncorrectResultSizeDataAccessException("您要删除的记录含有子元素, 删除失败!", 1);
        roleDao.deleteRolePermissionsByPermissionId(id);
        return super.deleteById(id);
    }

    public List<Permission> findAll() throws DataAccessException { return super.findAll(null); }

    /**
     * 查找子元素
     *
     * @param parentId 父元素ID
     */
    public List<Permission> findByParentId(Integer parentId) {
        return permissionDao.findAll(Collections.singletonMap("parentId", parentId));
    }

    /**
     * 设置权限层级与祖先路径
     */
    private void setPermissionAncestorAndLevel(Permission permission) {
        if (permission.getId() == null)
            permissionDao.save(permission);
        int level = 0;
        String ancestors = permission.getId().toString(), ancestorNames = permission.getName();
        if (permission.getParent() != null) {
            Permission parent = getById(permission.getParent());
            level = parent.getLevel() + 1;
            ancestors = parent.getAncestors() + "," + ancestors;
        }
        permission.setLevel(level);
        permission.setAncestors(ancestors);
    }

    /**
     * 查询当前用户的所有权限
     *
     * @param user 当前登录用户
     * @return Map&lt;PK, Permission&gt;
     */
    public Map<Comparable, Permission> findMap(User user) {
        Map<String, Object> params = null;
        if (user != null && !user.isAdmin()) {
            params = Collections.singletonMap("userId", user.getId());
        }
        return permissionDao.findMap(params, "id");
    }

    public Map<Comparable, Permission> findMapByRoleId(Integer roleId) {
        return permissionDao.findMap(Collections.singletonMap("roleId", roleId),"id");
    }
}