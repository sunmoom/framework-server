package cn.backflow.admin.service;

import cn.backflow.admin.dao.VersionDao;
import cn.backflow.admin.entity.Version;
import cn.backflow.admin.entity.VersionChannel;
import cn.backflow.admin.entity.VersionUser;
import cn.backflow.admin.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VersionService extends BaseService<Version, Integer> {

    @Autowired
    private VersionDao versionDao;

    /**
     * 按关键字或ID查询用户
     *
     * @param keyword 关键字
     * @param userIds 用户ID集合
     * @return Map形式封装的用户列表
     */
    public List<Map<String, Object>> searchUsers(String keyword, Collection<Integer> userIds) {
        return versionDao.searchUsers(keyword, userIds);
    }


    /**
     * 存储与版本相关联的用户与渠道
     *
     * @param version    版本对象
     * @param whiteUsers 用户白名单ID数组
     * @param blackUsers 用户黑名单ID数组
     * @param channels   渠道ID数组
     */
    public void persistWithUsersAndChannels(Version version, String[] whiteUsers, String[] blackUsers, String[] channels) {

        // 先保存版本
        super.saveOrUpdate(version);

        // 先清空该版本关联的渠道
        versionDao.deleteVersionChannel(new VersionChannel(version.getId(), null));
        // 保存关联的渠道
        if (channels != null && channels.length > 0) {
            List<VersionChannel> vcs = new ArrayList<>();
            for (String s : channels) {
                vcs.add(new VersionChannel(version.getId(), s));
            }
            versionDao.saveVersionChannels(vcs);
        }
        // 先清空该版本关联的所有用户, 不论是白名单或是黑名单
        versionDao.deleteVersionUser(new VersionUser(version.getId(), null, null));
        // 待保存的用户关联列表
        List<VersionUser> vus = new ArrayList<>();
        // 添加白名单用户到待保存列表
        if (whiteUsers != null && whiteUsers.length > 0) {
            for (String s : whiteUsers)
                vus.add(new VersionUser(version.getId(), Integer.parseInt(s), 0));
        }
        // 添加黑名单用户到待保存列表
        if (blackUsers != null && blackUsers.length > 0) {
            for (String s : blackUsers)
                vus.add(new VersionUser(version.getId(), Integer.parseInt(s), 1));
        }
        // 批量保存所有关联的用户
        if (vus.size() > 0)
            versionDao.saveVersionUsers(vus);
    }

    /**
     * 查询指定版本关联的白名单用户ID集合
     *
     * @param versionId 版本ID
     */
    public Set<Integer> findWhiteUsers(Integer versionId) {
        return versionDao.findVersionUserMap(new VersionUser(versionId, null, 0), "userId").keySet();
    }

    /**
     * 查询指定版本关联的黑名单用户ID集合
     *
     * @param versionId 版本ID
     */
    public Set<Integer> findBlackUsers(Integer versionId) {
        return versionDao.findVersionUserMap(new VersionUser(versionId, null, 1), "userId").keySet();
    }

    /**
     * 查询版本关联的所有关联用户实体集合
     *
     * @param versionId 版本ID
     */
    public List<VersionUser> findVersionUsers(Integer versionId) {
        return versionDao.findVersionUsers(new VersionUser(versionId, null, null));
    }
}