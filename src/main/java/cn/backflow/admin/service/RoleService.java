package cn.backflow.admin.service;

import cn.backflow.admin.dao.PermissionDao;
import cn.backflow.admin.dao.RoleDao;
import cn.backflow.admin.entity.Permission;
import cn.backflow.admin.entity.RolePermission;
import cn.backflow.admin.entity.Role;
import cn.backflow.admin.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class RoleService extends BaseService<Role, Integer> {

    private final PermissionDao permissionDao;
    private final RoleDao roleDao;

    @Autowired
    public RoleService(PermissionDao permissionDao, RoleDao roleDao) {
        this.permissionDao = permissionDao;
        this.roleDao = roleDao;
    }

    public List<Integer> findOwnedPermissionId(Object parameter) {
        return roleDao.findPermissionId(parameter);
    }

    @Transactional
    @CacheEvict(value = "perm_cache", allEntries = true)
    public int persistWithPermission(Role role, Set<String> perms) {
        // 保存角色
        int effected = role.getId() == null ? save(role) : update(role);
        // 清空角色权限
        deleteRolePermissionsByRoleId(role.getId());

        if (!perms.isEmpty()) {
            // 根据ID获取权限标识
            List<Permission> permissions = permissionDao.findAll(Collections.singletonMap("ids", perms));
            List<RolePermission> rps = new ArrayList<>();
            permissions.forEach(p -> rps.add(new RolePermission(role.getId(), p.getId(), p.getCode())));
            // 保存角色权限
            roleDao.insertRolePermission(rps);
        }
        return effected;
    }

    @Transactional
    @CacheEvict(value = "perm_cache", allEntries = true)
    public void saveUserRoles(Integer userId, Integer[] roles) {
        roleDao.deleteUserRoles(userId);
        roleDao.saveUserRoles(userId, roles);
    }

    @Transactional
    public void deleteRolePermissionsByRoleId(Integer roleId) {
        roleDao.deleteRolePermissions(Collections.singletonMap("roleId", roleId));
    }
}