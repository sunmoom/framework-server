package cn.backflow.admin.dao;

import cn.backflow.admin.dao.base.BaseMyBatisDao;
import cn.backflow.admin.entity.Dict;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public class DictDao extends BaseMyBatisDao<Dict, Integer> {

    /**
     * 查询返回
     *
     * @param code   字典编码
     * @param mapKey map对象的key属性
     * @return Map&lt;mapKey, Dict&gt; 对象
     */
    public Dict findMapByCode(String code, String mapKey) {
        List<Dict> list = getSqlSession().selectList("Dict.findByCode", code);
        for (Dict dict : list) {
            if (dict.getKey().equals(mapKey)) {
                return dict;
            }
        }
        return null;
    }

    public List<Dict> findByCode(String code) {
        return getSqlSession().selectList("Dict.findByCode", code);
    }

    public int insertBatch(Collection<Dict> dicts) {
        return getSqlSession().insert("Dict.insertBatch", dicts);
    }

    public int deleteByCode(String code) {
        return getSqlSession().delete("Dict.deleteByCode", code);
    }

    public int deleteByCodeLogical(String code) {
        return getSqlSession().delete("Dict.deleteByCodeLogical", code);
    }
}