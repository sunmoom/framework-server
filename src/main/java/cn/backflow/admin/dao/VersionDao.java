package cn.backflow.admin.dao;

import cn.backflow.admin.dao.base.BaseMyBatisDao;
import cn.backflow.admin.common.pagination.Page;
import cn.backflow.admin.common.pagination.PageRequest;
import cn.backflow.admin.entity.Version;
import cn.backflow.admin.entity.VersionChannel;
import cn.backflow.admin.entity.VersionUser;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class VersionDao extends BaseMyBatisDao<Version, Integer> {

    @Override
    @Resource(name = "sqlSessionFactoryServer")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        super.setSqlSessionFactory(sqlSessionFactory);
    }

    public Page<Version> findByPageRequest(PageRequest pageRequest) {
        return pageQuery("Version.paging", pageRequest);
    }

    public void saveVersionChannels(List<VersionChannel> vcs) {
        getSqlSession().insert("Version.insertVersionChannel", vcs);
    }

    public List<String> findCodesByVersionId(Object parameter) {
        return getSqlSession().selectList("Version.findCodesByVersionId", parameter);
    }

    public int deleteVersionChannel(Object parameter) {
        return getSqlSession().delete("Version.deleteVersionChannel", parameter);
    }

    public int deleteVersionUser(Object parameter) {
        return getSqlSession().delete("Version.deleteVersionUser", parameter);
    }

    public int saveVersionUsers(List<VersionUser> versionUsers) {
        return getSqlSession().insert("Version.insertVersionUser", versionUsers);
    }

    public List<Map<String, Object>> searchUsers(String keyword, Collection<Integer> userIds) {
        Map<String, Object> filter = new HashMap<>();
        filter.put("keyword", keyword);
        filter.put("userIds", userIds);
        return getSqlSession().selectList("Version.searchUsers", filter);
    }

    public Map<Integer, VersionUser> findVersionUserMap(Object parameter, String key) {
        return getSqlSession().selectMap("Version.findVersionUser", parameter, key);
    }

    public List<VersionUser> findVersionUsers(Object parameter) {
        return getSqlSession().selectList("Version.findVersionUser", parameter);
    }
}