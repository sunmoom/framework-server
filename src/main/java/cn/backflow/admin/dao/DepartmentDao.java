package cn.backflow.admin.dao;

import cn.backflow.admin.common.pagination.Page;
import cn.backflow.admin.common.pagination.PageRequest;
import cn.backflow.admin.entity.Department;
import cn.backflow.admin.dao.base.BaseMyBatisDao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DepartmentDao extends BaseMyBatisDao<Department, Integer> {


    @Override
    public int saveOrUpdate(Department department) {
        return department.getId() == null ? save(department) : update(department);
    }

    public Page<Department> findByPageRequest(PageRequest pageRequest) {
        return pageQuery("Department.paging", pageRequest);
    }

    public List<Department> findByUserId(Integer id) {
        return getSqlSession().selectList("Department.findByUserId", id);
    }
}