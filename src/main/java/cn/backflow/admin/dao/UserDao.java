package cn.backflow.admin.dao;

import cn.backflow.admin.common.pagination.Page;
import cn.backflow.admin.common.pagination.PageRequest;
import cn.backflow.admin.dao.base.BaseMyBatisDao;
import cn.backflow.admin.entity.User;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserDao extends BaseMyBatisDao<User, Integer> {

    @Override
    public int saveOrUpdate(User user) {
        return user.getId() == null ? save(user) : update(user);
    }

    public Page<User> findByPageRequest(PageRequest pageRequest) {
        return pageQuery("User.paging", pageRequest);
    }

    public User getByName(String v) {
        return getSqlSession().selectOne("User.getByName", v);
    }

    public User getByEmail(String v) {
        return getSqlSession().selectOne("User.getByEmail", v);
    }

    public User getByPhone(String mobile) {
        return getSqlSession().selectOne("User.getByPhone", mobile);
    }

    public int updateVisited(String username) {
        return getSqlSession().update("User.updateVisited", username);
    }

    public int updateAvatar(User user) {
        return getSqlSession().update("User.avatar", user);
    }

    public int updatePass(String name, String pass) {
        Map<String, String> param = new HashMap<>(2);
        param.put("name", name);
        param.put("pass", pass);
        return getSqlSession().update("User.updatePass", param);
    }

    public Long getTotalUserCount() {
        return getSqlSession().selectOne("User.getTotalUserCount");
    }

    public List<User> search(PageRequest pr) {
        pr.getFilters().put("_offset_", pr.getPageOffset());
        pr.getFilters().put("_limit_", pr.getPageSize());
        pr.getFilters().put("_sort_", pr.getSortColumns());
        return getSqlSession().selectList("User.search", pr.getFilters());
    }

    public int updateSelective(User user) {
        return getSqlSession().update("User.updateSelective", user);
    }


    public List<Map<String, Object>> findUserAsMap(Integer userId) {
        Map<String, Object> param = new HashMap<>(1);
        param.put("userId", userId);
        return getSqlSession().selectList("User.findUserAsMap", param);

    }

    public Page<User> query(PageRequest pr) {
        return pageQuery("User.query", pr);
    }

    public int updateRoleId(Integer id, Integer roleId) {
        Map<String, Integer> map = new HashMap<>();
        map.put("roleId", roleId);
        map.put("id", id);
        return getSqlSession().update("User.updateRoleId", map);
    }

    /**
     * 分离部门关联
     * @param id 部门ID
     */
    public int separateDepartment(Integer id) {
        return getSqlSession().update("User.separateDepartment", id);
    }
}