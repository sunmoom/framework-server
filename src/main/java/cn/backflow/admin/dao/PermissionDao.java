package cn.backflow.admin.dao;

import cn.backflow.admin.entity.Permission;
import cn.backflow.admin.dao.base.BaseMyBatisDao;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class PermissionDao extends BaseMyBatisDao<Permission, Integer> {


    @Override
    public int saveOrUpdate(Permission permission) {
        return permission.getId() == null ? save(permission) : update(permission);
    }

    public List<String> findCode(Object parameter) {
        return getSqlSession().selectList("Permission.findCode", parameter);
    }
}