package cn.backflow.admin.common.pagination;


import cn.backflow.admin.controller.base.BaseSpringController;
import cn.backflow.lib.util.StringUtil;

import java.io.Serializable;
import java.util.*;


/**
 * 分页请求信息
 */
public class PageRequest implements Serializable {
    private static final long serialVersionUID = 4047115735625926686L;

    private Map<String, Object> filters = new HashMap<>(8); // 查询过滤参数
    private Integer pageNumber; // 页号码,页码从1开始
    private Integer pageSize;   // 分页大小
    private String sortColumns; // 排序的多个列, 如: username desc
    private String requestUri = ""; // 内部请求地址
    private String query = "";

    public PageRequest() {
    }

    public PageRequest(int pageSize, int pageNumber, String sortColumns) {
        setSortColumns(sortColumns);
        setPageNumber(pageNumber);
        setPageSize(pageSize);
    }

    public Map<String, Object> getFilters() {
        return filters;
    }

    public PageRequest addFilter(String name, Object value) {
        this.filters.put(name, value);
        return this;
    }

    public PageRequest addInitialFilter(String name, Object value) {
        this.filters.put(name, value);
        if (!BaseSpringController.PARAM_PAGENUMBER.equals(name))
            query = addQueryParam(query, name, value);
        return this;
    }

    public PageRequest addInitialFilter(String name, Object... values) {
        this.filters.put(name, values);
        if (!BaseSpringController.PARAM_PAGENUMBER.equals(name))
            addQueryParam(query, name, values);
        return this;
    }

    public String replaceQueryParam(String name, String value) {
        if (StringUtil.isNotBlank(query)) {
            query = query.replaceAll(name + "=.*?($|&)", "").replaceFirst("&$", "");
        }
        return addQueryParam(name, value);
    }

    public String addQueryParam(String query, String name, Object... values) {
        query += query.isEmpty() ? "?" : "&";
        for (int i = 0, len = values.length; i < len; i++) {
            if (i > 0) query += "&";
            query += name + "=" + values[i];
        }
        return query;
    }

    public String addParamAndGetQuery(String name, Object value) {
        String query = this.query;
        return requestUri + addQueryParam(query, name, value);
    }

    public String getQuery() {
        return requestUri + query;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public PageRequest setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber < 1 ? 1 : pageNumber;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public PageRequest setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public int getPageOffset() {
        return (pageNumber - 1) * pageSize;
    }


    public PageRequest setRequestUri(String requestUri) {
        this.requestUri = requestUri;
        return this;
    }

    public String getSortColumns() {
        return sortColumns;
    }

    /**
     * 排序的列,可以同时多列,使用逗号分隔,如 username desc,age asc
     */
    public PageRequest setSortColumns(String sortColumns) {
        checkSortColumnsSqlInjection(sortColumns);
        if (sortColumns != null && sortColumns.length() > 50)
            throw new IllegalArgumentException("sortColumns.length() must be less than 50");
        this.sortColumns = sortColumns;
        return this;
    }

    /**
     * 将sortColumns进行解析以便返回SortInfo以便使用
     */
    public List<SortInfo> getSortInfos() {
        return Collections.unmodifiableList(SortInfo.parseSortColumns(sortColumns));
    }

    private void checkSortColumnsSqlInjection(String sortColumns) {
        if (sortColumns == null)
            return;
        if (sortColumns.contains("'") || sortColumns.contains("\\"))
            throw new IllegalArgumentException("sortColumns:" + sortColumns + " has SQL Injection risk");
    }


    /**
     * 排序的列
     */
    public static class SortInfo implements Serializable {
        private static final long serialVersionUID = 6959974032209696722L;

        private String column;
        private String order;

        public SortInfo() {
        }

        public SortInfo(String column, String order) {
            this.column = column;
            this.order = order;
        }

        public void setColumn(String column) {
            this.column = column;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public String getColumn() {
            return column;
        }

        public String getOrder() {
            return order;
        }

        public static List<SortInfo> parseSortColumns(String sortColumns) {
            if (sortColumns == null) {
                return Collections.emptyList();
            }

            List<SortInfo> results = new ArrayList<>();
            String[] segments = sortColumns.trim().split(",");
            for (String segment : segments) {
                String[] array = segment.split("\\s+");

                SortInfo info = new SortInfo();
                info.setColumn(array[0]);
                info.setOrder(array.length == 2 ? array[1] : null);
                results.add(info);
            }
            return results;
        }

        @Override
        public String toString() {
            return column + (order == null ? "" : " " + order);
        }
    }
}